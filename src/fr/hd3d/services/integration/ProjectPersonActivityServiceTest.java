package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.lightweight.impl.LSimpleActivity;
import fr.hd3d.model.lightweight.impl.LTaskActivity;


/**
 * Project / Persons / Activities web service tests : list (GET) and singleton (CRUD)
 * 
 * @author Guillaume Chatelet
 */
public class ProjectPersonActivityServiceTest extends AbstractServiceIntegrationTest<ILActivity>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    @Test
    public void testGetActivities() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final Collection<ILActivity> activities = doGetObject(projectUrl + "/persons/" + gch.getId() + "/activities");
        assertNotNull(gch);
        assertNotNull(activities);
        assertEquals(3, activities.size());
        int simpleActivityCount = 0;
        int taskActivityCount = 0;
        for (ILActivity activity : activities)
        {
            if (activity instanceof ILSimpleActivity)
            {
                ++simpleActivityCount;
                final ILSimpleActivity simpleActivity = (ILSimpleActivity) activity;
                assertNotNull(simpleActivity.getType());
                assertEquals(simpleActivity.getTaskName(), simpleActivity.getType());
            }
            else if (activity instanceof ILTaskActivity)
            {
                ++taskActivityCount;
                final ILTaskActivity taskActivity = (ILTaskActivity) activity;
                final Long taskID = taskActivity.getTaskID();
                assertNotNull(taskID);
                final ILTask task = doGetObject(projectUrl + "/persons/" + gch.getId() + "/tasks/" + taskID.toString());
                assertEquals(task.getName(), taskActivity.getTaskName());
            }
            else
                fail("Unimplemented object " + activity.getClass().getName());
            assertEquals(projectA.getId(), activity.getProjectID());
            assertEquals(projectA.getName(), activity.getProjectName());
            assertEquals(projectA.getColor(), activity.getProjectColor());
            assertEquals(gch.getId(), activity.getWorkerID());
            assertEquals(gch.getName(), activity.getWorkerName());
            assertNotNull(activity.getDuration());
            assertNotNull(activity.getDayID());
            assertNotNull(activity.getComment());
        }
        assertEquals(2, simpleActivityCount);
        assertEquals(1, taskActivityCount);
    }

    @Test
    public void testGetActivity() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String gchActivitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final List<ILActivity> activities = doGetObject(gchActivitiesUrl);
        assertEquals(3, activities.size());
        final ILActivity activity = doGetObject(projectUrl + "/persons/" + gch.getId() + "/activities/"
                + activities.get(2).getId().toString());
        assertNotNull(activity);
        assertEquals(projectA.getName(), activity.getProjectName());
        assertEquals(projectA.getId(), activity.getProjectID());
        assertEquals(projectA.getColor(), activity.getProjectColor());
        final String meetingString = ESimpleActivityType.MEETING.toString();
        assertEquals(meetingString, activity.getTaskName());
        assertEquals(gch.getName(), activity.getWorkerName());
        assertEquals(gch.getId(), activity.getWorkerID());
        assertEquals(gch.getLogin(), activity.getWorkerLogin());
        assertNotNull(activity.getDayID());
        assertNotNull(activity.getDuration());
    }

    @Test
    public void testPostSimpleActivity() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String activitiesUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/activities";
        Collection<ILActivity> activities = doGetObject(activitiesUrl);
        assertEquals(3, activities.size());
        final Date startDate = new Date();
        final Long duration = 10L;
        final String comment = Double.toString(Math.random());
        final ILPersonDay day = this.getDay(gch.getId());
        final ILActivity newActivity = LSimpleActivity.getNewInstance(ESimpleActivityType.MEETING, day.getId(),
                duration, comment, gch.getId(), projectA.getId(), gch.getId(), startDate);
        final WebResponse response = doPostAtURL(activitiesUrl, newActivity);
        assertEquals(CREATION_OK, response.getText());
        activities = doGetObject(activitiesUrl);
        assertEquals(4, activities.size());
        for (ILActivity activity : activities)
        {
            if (comment.equals(activity.getComment()))
            {
                if (activity instanceof ILSimpleActivity == false)
                    fail("The created activity is not a simple activity");
                final ILSimpleActivity simpleActivity = (ILSimpleActivity) activity;
                assertEquals(day.getId(), simpleActivity.getDayID());
                assertEquals(startDate.toString(), simpleActivity.getFilledDate().toString());
                assertEquals(duration, simpleActivity.getDuration());
                final String meetingString = ESimpleActivityType.MEETING.toString();
                assertEquals(meetingString, simpleActivity.getType());
                assertEquals(meetingString, simpleActivity.getTaskName());
                assertEquals(gch.getId(), simpleActivity.getWorkerID());
                assertEquals(projectA.getId(), simpleActivity.getProjectID());
                return;
            }
        }
        fail("The created activity has not been found ...");
    }

    @Test
    public void testPostTaskActivity() throws IOException
    {
        // getting a project
        final ILProject projectA = getProjectA();
        // getting a person
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String activitiesUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/activities";
        final String tasksUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/tasks/";
        final List<ILTask> tasks = doGetObject(tasksUrl);
        final ILTask task = tasks.get(0);
        Collection<ILActivity> activities = doGetObject(activitiesUrl);
        assertEquals(3, activities.size());
        final Date startDate = new Date();
        final Long duration = 10L;
        final String comment = Double.toString(Math.random());
        final ILPersonDay day = getDay(gch.getId());
        final ILActivity newActivity = LTaskActivity.getNewInstance(day.getId(), day.getDate(), duration, comment,
                task, gch.getId(), projectA.getId(), gch.getId(), startDate);
        final WebResponse response = doPostAtURL(activitiesUrl, newActivity);
        assertEquals(CREATION_OK, response.getText());
        activities = doGetObject(activitiesUrl);
        assertEquals(4, activities.size());
        for (ILActivity activity : activities)
        {
            if (comment.equals(activity.getComment()))
            {
                if (activity instanceof ILTaskActivity == false)
                    fail("The created activity is not a task activity");
                final ILTaskActivity taskActivity = (ILTaskActivity) activity;
                assertEquals(startDate.toString(), taskActivity.getFilledDate().toString());
                assertEquals(duration, taskActivity.getDuration());
                assertEquals(task.getName(), taskActivity.getTaskName());
                assertEquals(task.getId(), taskActivity.getTaskID());
                assertEquals(gch.getId(), taskActivity.getWorkerID());
                assertEquals(projectA.getId(), taskActivity.getProjectID());
                assertEquals(day.getId(), taskActivity.getDayID());
                return;
            }
        }
        fail("The created activity has not been found ...");
    }

    @Test
    public void testPutTaskActivity() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        String constraint = "?constraint=[{\"type\":\"eq\",\"column\":"
                + "\"comment\",\"value\":\"Did a few things\"}]";
        final Collection<ILTaskActivity> activities = doGetObject(projectUrl + "/persons/" + gch.getId()
                + "/activities/" + constraint);
        assertNotNull(activities);
        ILTaskActivity activity = activities.iterator().next();
        final Date filledDate = new Date(5000);
        activity.setFilledDate(filledDate);
        activity.setDuration(50L);
        activity.setComment("Fire in the place!!!");
        doPUTAtURL(getProjectUrl(projectA) + "/persons/" + gch.getId() + "/activities/" + activity.getId(), activity);
        ILTaskActivity createdActivity = doGetObject(getProjectUrl(projectA) + "/persons/" + gch.getId()
                + "/activities/" + activity.getId());
        assertEquals(filledDate.toString(), createdActivity.getFilledDate().toString());
        assertEquals(new Long(50), createdActivity.getDuration());
        assertEquals("Fire in the place!!!", createdActivity.getComment());
    }

    @Test
    public void testPutSimpleActivity() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String projectAUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectAUrl);

        final String gchActivitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final List<ILActivity> activities = doGetObject(gchActivitiesUrl);

        assertEquals(3, activities.size());
        final ILSimpleActivity activity = doGetObject(projectAUrl + "/persons/" + gch.getId() + "/activities/"
                + activities.get(2).getId().toString());
        assertNotNull(activity);
        activity.setType(ESimpleActivityType.TECHNICAL_INCIDENT.toString());
        final Date filledDate = new Date(5000);
        activity.setFilledDate(filledDate);
        activity.setDuration(50L);
        activity.setComment("Fire in the place!!!");
        doPUTAtURL(projectAUrl + "/persons/" + gch.getId() + "/activities/" + activity.getId(), activity);
        ILSimpleActivity createdActivity = doGetObject(projectAUrl + "/persons/" + gch.getId() + "/activities/"
                + activity.getId());
        assertNotNull(createdActivity);
        assertEquals(ESimpleActivityType.TECHNICAL_INCIDENT.toString(), createdActivity.getType());
        assertEquals(filledDate.toString(), createdActivity.getFilledDate().toString());
        assertEquals(new Long(50), createdActivity.getDuration());
        assertEquals("Fire in the place!!!", createdActivity.getComment());
    }

    /**
     * Test task activity update through web services (DELETE
     * /hd3dServices/project/{projectId}/persons/{personId}/activities).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskActivityDELETE() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        String constraint = "?constraint=[{\"type\":\"eq\",\"column\""
                + ":\"comment\",\"value\":\"Did a few things\"}]";
        final String url = projectUrl + "/persons/" + gch.getId() + "/activities/" + constraint;
        // Get the task activity to delete
        final Collection<ILTaskActivity> activities = doGetObject(url);
        assertNotNull(activities);
        ILTaskActivity activity = activities.iterator().next();
        // Delete group.
        doDELETEAtURL(this.getActivityUrl(activity));
        // Ensure that activity does not exist anymore.
        Collection<ILActivity> activitiesAll = doGetObject(SERVICE + ACTIVITY);
        assertEquals(6, activitiesAll.size());
    }

    @Test
    public void testActivityLike() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        String constraint = getLikeConstraint("comment", "%simple%");
        final Collection<ILActivity> activities = doGetObject(projectUrl + "/persons/" + gch.getId() + "/activities/"
                + constraint);
        assertEquals(1, activities.size());
        ILActivity activity = activities.iterator().next();
        assertEquals(projectA.getName(), activity.getProjectName());
        assertEquals(projectA.getId(), activity.getProjectID());
        assertEquals(projectA.getColor(), activity.getProjectColor());
        assertEquals("A simple task for gch", activity.getComment());
    }
}
