package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.SEQUENCES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.lightweight.impl.LSequence;
import fr.hd3d.model.lightweight.impl.LShot;


/**
 * Tests web service Shots in Project sequences: list (GET) and singleton (CRUD)
 * 
 * @author Try LAM
 */
public class ShotsServiceTest extends AbstractServiceIntegrationTest<ILShot>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=SequenceShots");
    }

    /**
     * Test shots retrieved through web services (GET /hd3dServices/project/{projectId}/sequences/{sequenceID}/shots ).
     * 
     * @throws IOException
     */
    @Test
    public void testShotsGET() throws IOException
    {
        String url = getProjectAPiloteSequenceUrl() + "/shots";
        Collection<ILShot> shots = doGetObject(url);
        assertEquals(2, shots.size());
    }

    /**
     * Test shot CREATION through web services (POST /hd3dServices/project/{projectId}/sequences/{sequenceID}/shots).
     * 
     * @throws IOException
     */
    @Test
    public void testShotsPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // get the category
        ILSequence sequence = getProjectAPiloteSequence();
        // create a new shot
        ILShot shot = LShot.getNewInstance(sequence.getId(), 0, "New shot Description 日本", 5, "Newshothook",
                "New shot Label日本", 5, true, true, false, false, false, false, false, false, false, 42, null, 0, 0);
        String url = getProjectUrl(projectA) + '/' + SEQUENCES + "/" + sequence.getId() + "/shots";
        WebResponse response = doPostAtURL(url, shot);
        assertEquals(CREATION_OK, response.getText());
        // get the new shot
        ILShot newShot = getProjectAPiloteShot("New shot Label日本");
        // compare
        assertEquals(shot.getNbFrame(), newShot.getNbFrame());
        assertEquals(shot.getSequence(), newShot.getSequence());
        assertEquals(shot.getDescription(), newShot.getDescription());
        assertEquals(shot.getCompletion(), newShot.getCompletion());
        assertEquals(shot.getDifficulty(), newShot.getDifficulty());
        assertEquals(shot.getHook(), newShot.getHook());
        assertEquals(shot.getTrust(), newShot.getTrust());
        assertEquals(shot.getLabel(), newShot.getLabel());
    }

    /**
     * Test shot MULITPLE CREATION through web services (POST
     * /hd3dServices/project/{projectId}/sequences/{sequenceID}/shots).
     * 
     * @throws IOException
     */
    @Test
    public void testBulkShotsPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // get the sequence
        ILSequence sequence = getProjectAPiloteSequence();
        // create new shots
        List<ILShot> shots = new ArrayList<ILShot>();
        final int NUMBERSHOTS = 3;
        for (int i = 0; i < NUMBERSHOTS; i++)
        {
            shots.add(LShot.getNewInstance(sequence.getId(), 0, "New shot Description 日本", 5, "Newshothook",
                    "New shot Label日本" + i, 5, false, false, false, false, false, false, false, false, false, 42, null,
                    0, 0));
        }
        String url = getProjectUrl(projectA) + '/' + SEQUENCES + "/" + sequence.getId() + "/shots";
        WebResponse response = doPostAtURL(url + "?mode=bulk", shots);
        assertEquals(CREATION_OK, response.getText());
        // get the new shots
        List<ILShot> newShots = doGetObject(url);
        int count = 0;
        for (ILShot s : newShots)
        {
            if (s.getLabel().startsWith("New shot Label日本"))
            {
                count++;
            }
        }
        assertEquals(NUMBERSHOTS, count);
    }

    // /**
    // * Test shot UPDATE through web services (PUT
    // * /hd3dServices/project/{projectId}/sequences/{sequenceID}/shots).
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testShotsPUT() throws IOException {
    // // get the project A
    // ILProject projectA = getProjectA();
    // // create a new main category (with "shot" as parent) and part of
    // // project A
    // ILCategory category = LCategory.getNewInstance(null, null, "nouvelle Categorie", null, projectA.getId());
    // WebResponse response = doPUTAtURL(getProjectUrl(projectA) + Hd3dApplication.CATEGORIES, category);
    // // MUST NOT BE ALLOWED
    // assertEquals("", response.getText());
    //
    // }

    //
    // /**
    // * Test Categories DELETE through web services (DELETE /hd3dServices/project/{projectId}/sequences).
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoriesDELETE() throws IOException {
    // // get the project A
    // ILProject projectA = getProjectA();
    // // create a new main category (with "shot" as parent) and part of
    // // project A
    // WebResponse response = doDELETEAtURL(getProjectUrl(projectA) + Hd3dApplication.CATEGORIES);
    // // MUST NOT BE ALLOWED
    // assertEquals("", response.getText());
    //
    // }
    //    
    // convenience method
    private String getProjectAPiloteSequenceUrl() throws IOException
    {
        String ret = null;

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<LSequence> sequences = doGetObject(url);
        // retrieve the main sequences (with "sequence" as parent) for the
        // project A
        assertEquals(2, sequences.size());
        for (LSequence sequence : sequences)
        {

            if ("Pilote".equals(sequence.getName()))
            {
                assertEquals(projectA.getId(), sequence.getProject());
                // get the subsequences
                url = url + "/" + sequence.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }

    // convenience method
    private ILSequence getProjectAPiloteSequence() throws IOException
    {
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<LSequence> sequences = doGetObject(url);
        // retrieve the main sequences (with "sequence" as parent) for the
        // project A
        assertEquals(2, sequences.size());
        for (LSequence sequence : sequences)
        {
            if ("Pilote".equals(sequence.getName()))
            {
                return sequence;
            }
        }
        return null;
    }

    // convenience method
    private ILShot getProjectAPiloteShot(String label) throws IOException
    {
        String url = getProjectAPiloteSequenceUrl() + "/shots";
        Collection<ILShot> shots = doGetObject(url);
        for (ILShot s : shots)
        {
            if (s.getLabel().equals(label))
            {
                return s;
            }
        }
        return null;
    }
}
