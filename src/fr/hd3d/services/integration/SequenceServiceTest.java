package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CHILDREN;
import static fr.hd3d.common.client.ServicesURI.SEQUENCES;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.impl.LSequence;


/**
 * @author Try LAM
 */
public class SequenceServiceTest extends AbstractServiceIntegrationTest<ILSequence>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=SequenceShots");
    }

    /**
     * Test Sequences retrieved through web services (GET /hd3dServices/project/{projectId}/sequences/{sequenceId}).
     * 
     * @throws IOException
     */
    @Test
    public void testSequenceGET() throws IOException
    {

        // test
        List<ILProject> projects = doGetObject(SERVICE + PROJECT);
        String version = doGetObject(SERVICE + "version");
        //
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<ILSequence> sequences = doGetObject(url);
        // retrieve the main subsequences (with "subsequence" as parent) for the
        // project A
        assertEquals(2, sequences.size());
        for (ILSequence sequence : sequences)
        {
            if ("Pilote".equals(sequence.getName()))
            {
                assertEquals(projectA.getId(), sequence.getProject());
                // get the subsequences
                // url = url + "/" + sequence.getId();
                url = url + "/" + sequence.getId() + "/children";
                // Pilote is supposed to have 2 subsequences
                Collection<ILSequence> subSequences = doGetObject(url);
                assertEquals(2, subSequences.size());
            }
            // TODO check other sequences
        }
    }

    /**
     * Test Sequence MODIFICATION through web services (PUT /hd3dServices/project/{projectId}/sequences/{sequenceId}).
     * 
     * @throws IOException
     */
    @Test
    public void testSequencePUT() throws IOException
    {

        ILProject projectA = getProjectA();
        ILSequence sequence = this.getSequenceInProject("Pilote", getProjectUrl(projectA));

        sequence.setName("UPDATED Pilote");
        String url = getProjectUrl(projectA) + '/' + SEQUENCES + "/" + sequence.getId();
        doPUTAtURL(url, sequence);
        ILSequence updatedSequence = this.getSequenceInProject("UPDATED Pilote", getProjectUrl(projectA));
        assertEquals("UPDATED Pilote", updatedSequence.getName());
    }

    /**
     * Test Sequence CREATION through web services (POST /hd3dServices/project/{projectId}/sequences/{sequenceId}).
     * 
     * @throws IOException
     */
    @Test
    public void testSequencePOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        // retrieve the main sequences (with "sequence" as parent) for the project A
        Collection<LSequence> sequences = doGetObject(url);
        assertEquals(2, sequences.size());
        for (LSequence sequence : sequences)
        {
            // retrieve the Pilote main sequence
            if ("Pilote".equals(sequence.getName()))
            {
                // create a new sub-sequence (with "Pilote" as parent)
                ILSequence subSequence = LSequence.getNewInstance("Sous Séquence", "Titre de sous-séquence", null,
                        projectA.getId(), null);
                String url1 = getProjectUrl(projectA) + '/' + SEQUENCES + "/" + sequence.getId() + '/' + CHILDREN;
                WebResponse response = doPostAtURL(url1, subSequence);
                assertEquals(CREATION_OK, response.getText());
                // retrieve the project A's newly created sequence and check
                sequences = doGetObject(url);
                for (LSequence newSequence : sequences)
                {
                    if ("Pilote".equals(newSequence.getName()))
                    {
                        String url2 = getProjectUrl(projectA) + '/' + SEQUENCES + "/" + newSequence.getId() + "/"
                                + CHILDREN;
                        Collection<LSequence> newSequences = doGetObject(url2);
                        // Pilote category must have one more new sub sequence
                        assertEquals(3, newSequences.size());
                    }
                }
            }
        }
    }

    /**
     * Test Sequence DELETION through web services (DELETE /hd3dServices/project/{projectId}/sequences/{sequenceId}).
     * 
     * @throws IOException
     */
    @Test
    public void testSequenceDELETE() throws IOException
    {

        // get the "AAACtion !" category of the project A
        ILSequence sequence = getSequenceInProject("AAACtion", getProjectUrl(getProjectA()));
        // delete it
        String url = getProjectUrl(getProjectA()) + "/" + SEQUENCES + "/" + sequence.getId();
        // + "/" + CHILDREN;
        doDELETEAtURL(url);
        // read it back again
        ILSequence sequence1 = getSequenceInProject("AAACtion", getProjectUrl(getProjectA()));
        assertNull(sequence1);
        // assertEquals(sequence, sequence1);
    }

    /**
     * Test like condition on Sequence name
     * 
     * @throws IOException
     */
    @Test
    public void testSequenceLike() throws IOException
    {

        String constraint = getLikeConstraint("name", "Pilo%");
        Collection<LSequence> sequences = doGetObject(getProjectUrl(getProjectA()) + '/' + SEQUENCES + constraint);
        assertEquals(1, sequences.size());
        assertTrue(sequences.iterator().next().getName().startsWith("Pilo"));
    }

}
