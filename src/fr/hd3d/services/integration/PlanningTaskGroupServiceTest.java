package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.PLANNINGS;
import static fr.hd3d.common.client.ServicesURI.TASK_GROUPS;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.lightweight.impl.LTaskGroup;


public class PlanningTaskGroupServiceTest extends AbstractServiceIntegrationTest<ILTaskGroup>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    private static final String TEST_TASK_GROUP_WITH_PARENT_NAME = "Setup";

    @Test
    public void testGetTaskGroups() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS;
        final Collection<ILTaskGroup> taskGroups = doGetObject(planningTaskGroupUrl);
        assertNotNull(taskGroups);
        assertEquals(3, taskGroups.size());
        for (ILTaskGroup taskGroup : taskGroups)
        {
            assertEquals(planning.getId(), taskGroup.getPlanning());
        }
    }

    @Test
    public void testGetTaskGroup() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS;
        final Collection<ILTaskGroup> taskGroups = doGetObject(planningTaskGroupUrl);
        assertNotNull(taskGroups);
        assertEquals(3, taskGroups.size());
        final ILTaskGroup taskGroup = doGetObject(planningTaskGroupUrl + "/"
                + ((ILTaskGroup) (taskGroups.iterator().next())).getId().toString());
        assertNotNull(taskGroup);
        assertEquals(taskGroup.getPlanning(), planning.getId());
    }

    @Test
    public void testPostTaskGroup() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS;
        Collection<ILTaskGroup> taskGroups = doGetObject(planningTaskGroupUrl);
        assertNotNull(taskGroups);
        assertEquals(3, taskGroups.size());
        // add new TaskGroup
        final String testTaskGroupName = "The Test Task Group";
        final ILTaskGroup newTaskGroup = LTaskGroup.getNewInstance(testTaskGroupName, "#ffffff", planning.getId(),
                null, null, null, null, null);
        final WebResponse response = doPostAtURL(planningTaskGroupUrl, newTaskGroup);
        assertEquals("Creation ok", response.getText());
        taskGroups = doGetObject(planningTaskGroupUrl);
        assertEquals(4, taskGroups.size());
        for (ILTaskGroup taskGroup : taskGroups)
        {
            if (taskGroup.getName().equals(testTaskGroupName))
            {
                assertEquals(planning.getId(), taskGroup.getPlanning());
            }
        }
    }

    @Test
    public void testUpdateTaskGroup() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS;
        Collection<ILTaskGroup> taskGroups = doGetObject(planningTaskGroupUrl);
        assertNotNull(taskGroups);
        assertEquals(3, taskGroups.size());
        // get the first task group
        final ILTaskGroup taskGroup = taskGroups.iterator().next();
        final String newTaskGroupName = taskGroup.getName() + " updated";
        taskGroup.setName(newTaskGroupName);
        // update task group
        final String taskGroupUrl = planningTaskGroupUrl + "/" + taskGroup.getId().toString();
        doPUTAtURL(taskGroupUrl, taskGroup);
        final ILTaskGroup newTaskGroup = doGetObject(taskGroupUrl);
        assertEquals(taskGroup.getPlanning(), newTaskGroup.getPlanning());
        assertEquals(taskGroup.getColor(), newTaskGroup.getColor());
        assertEquals(taskGroup.getName(), newTaskGroupName);
        assertEquals(taskGroup.getTaskGroup(), newTaskGroup.getTaskGroup());
    }

    @Test
    public void testDeleteDeletableTaskGroup() throws IOException
    {
        // before delete
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS;
        Collection<ILTaskGroup> taskGroups = doGetObject(planningTaskGroupUrl);
        assertNotNull(taskGroups);
        assertEquals(3, taskGroups.size());
        // do delete
        for (ILTaskGroup taskGroup : taskGroups)
        {
            if (TEST_TASK_GROUP_WITH_PARENT_NAME.equals(taskGroup.getName()))
            {
                doDELETEAtURL(planningTaskGroupUrl + "/" + taskGroup.getId());
                // doDELETEAtURL(planningTaskGroupUrl + "/" + taskGroups.iterator().next().getId());
            }
        }
        // after delete
        taskGroups = doGetObject(planningTaskGroupUrl);
        assertEquals(2, taskGroups.size());
    }

    // @Test
    // public void testDeleteParentTaskGroup() throws IOException
    // {
    //
    // }

    @Test
    public void testTaskGroupLike() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        String constraint = getLikeConstraint("name", "Mod%");
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS + constraint;
        final Collection<ILTaskGroup> taskGroupCollection = doGetObject(planningTaskGroupUrl);
        assertEquals(1, taskGroupCollection.size());
        ILTaskGroup taskGroup = taskGroupCollection.iterator().next();
        assertEquals(taskGroup.getPlanning(), planning.getId());
    }

}
