/**
 * 
 */
package fr.hd3d.services.integration;

import java.io.IOException;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPerson;


/**
 * @author Thomas ESKENAZI
 * 
 */
public class RootServiceTest extends AbstractServiceIntegrationTest<ILPerson>
{
    protected void initDB() throws IOException
    {}

    public void testServerRoot() throws IOException
    {
        WebResponse response = doHTMLGetAtURL(SERVICE);
        assertTrue(response.getText().length() != 0);
    }
}
