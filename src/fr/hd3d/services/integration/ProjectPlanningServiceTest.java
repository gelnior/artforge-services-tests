package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.PLANNINGS;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LPlanning;


public class ProjectPlanningServiceTest extends AbstractServiceIntegrationTest<ILPlanning>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    private static final String TEST_PLANNING_NAME = "Planning1A";

    @Test
    public void testGetPlannings() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final Collection<ILPlanning> plannings = doGetObject(projectUrl + '/' + PLANNINGS);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        for (ILPlanning planning : plannings)
        {
            assertEquals(projectA.getId(), planning.getProject());
        }
    }

    @Test
    public void testGetPlanning() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        String constraint = "?orderBy=[\"id\"]";
        final Collection<ILPlanning> plannings = doGetObject(projectUrl + '/' + PLANNINGS + constraint);
        assertEquals(2, plannings.size());
        final ILPlanning planning = doGetObject(projectUrl + '/' + PLANNINGS + "/"
                + ((ILPlanning) (plannings.iterator().next())).getId());
        assertEquals(projectA.getId(), planning.getProject());
        assertEquals(TEST_PLANNING_NAME, planning.getName());
    }

    @Test
    public void testPostPlanning() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        Collection<ILPlanning> plannings = doGetObject(projectUrl + '/' + PLANNINGS);
        assertEquals(2, plannings.size());
        final String testPlanningName = "The test Planning";
        Calendar now = Calendar.getInstance();
        Date startDate = now.getTime();
        now.add(Calendar.YEAR, 1);
        Date endDate = now.getTime();
        final ILPlanning newPlanning = LPlanning.getNewInstance(projectA.getId(), testPlanningName, true, startDate,
                endDate);
        final WebResponse response = doPostAtURL(projectUrl + '/' + PLANNINGS, newPlanning);
        assertEquals("Creation ok", response.getText());
        plannings = doGetObject(projectUrl + '/' + PLANNINGS);
        assertEquals(3, plannings.size());
        for (ILPlanning planning : plannings)
        {
            if (testPlanningName.equals(planning.getName()))
            {
                assertEquals(projectA.getId(), planning.getProject());
            }
        }
    }

    @Test
    public void testUpdatePlanning() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String planningsUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        Collection<ILPlanning> plannings = doGetObject(planningsUrl);
        assertEquals(2, plannings.size());
        // get the first planning and update it
        final ILPlanning planning = plannings.iterator().next();
        final String newPlanningName = planning.getName() + " updated";
        Calendar cal = Calendar.getInstance();
        cal.setTime(planning.getStartDate());
        cal.add(Calendar.YEAR, 1);
        planning.setStartDate(cal.getTime());
        cal.setTime(planning.getEndDate());
        cal.add(Calendar.YEAR, 1);
        planning.setEndDate(cal.getTime());
        planning.setName(newPlanningName);
        // submit the changes
        final String planningUrl = planningsUrl + "/" + planning.getId().toString();
        doPUTAtURL(planningUrl, planning);
        ILPlanning newPlanning = doGetObject(planningUrl);
        if (newPlanning == null)
            newPlanning = doGetObject(planningUrl);// try again

        assertNotNull(newPlanning);
        assertEquals(planning.getProject(), newPlanning.getProject());
        assertEquals(planning.getName(), newPlanning.getName());
        assertEquals(planning.getStartDate(), newPlanning.getStartDate());
        assertEquals(planning.getEndDate(), newPlanning.getEndDate());
    }

    @Test
    public void testDeleteDeletablePlanning() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String planningsUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        Collection<ILPlanning> plannings = doGetObject(planningsUrl);
        assertEquals(2, plannings.size());
        for (ILPlanning planning : plannings)
        {
            if (TEST_PLANNING_NAME.equals(planning.getName()))
            {
                doDELETEAtURL(planningsUrl + "/" + planning.getId().toString());
            }
        }
        plannings = doGetObject(planningsUrl);
        assertEquals(1, plannings.size());
    }

    @Test
    public void testPlanningLike() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String planningsUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        String constraint = getLikeConstraint("name", "Pl%");
        final Collection<ILPlanning> plannings = doGetObject(planningsUrl + constraint);
        assertEquals(2, plannings.size());
        ILPlanning planning = plannings.iterator().next();
        assertEquals(planning.getProject(), projectA.getId());
    }

}
