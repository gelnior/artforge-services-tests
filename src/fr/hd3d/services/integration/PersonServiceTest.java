package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.impl.LPerson;


/**
 * Tests web service Person : list (GET) and singleton (CRUD)
 * 
 * @author Guillaume Chatelet
 */
public class PersonServiceTest extends AbstractServiceIntegrationTest<ILPerson>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    /**
     * Test Persons retrieve through web services (GET /hd3dServices/persons/).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonsGET() throws IOException
    {
        // Get Persons List
        Collection<LPerson> persons = doGetObject(SERVICE + "/" + PERSON);
        assertEquals(4, persons.size());
        // Verify 3 of them
        for (LPerson person : persons)
        {
            if ("gch".equals(person.getLogin()))
            {
                assertEquals("Guillaume", person.getFirstName());
                assertEquals("Chatelet", person.getLastName());
            }
            else if ("eth".equals(person.getLogin()))
            {
                assertEquals("Thomas", person.getFirstName());
                assertEquals("Eskénazi", person.getLastName());
            }
            else if ("acs".equals(person.getLogin()))
            {
                assertEquals("Arnaud", person.getFirstName());
                assertEquals("Chassagne", person.getLastName());
            }
            else if ("root".equals(person.getLogin()))
            {
                assertEquals("Default", person.getFirstName());
                assertEquals("User", person.getLastName());
            }
            else
            {
                fail("unexpected person :" + person.getName());
            }
        }
    }

    /**
     * Test Person retrieve through web services (GET /hd3dServices/persons/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonGET() throws IOException
    {
        // Get Persons List
        Collection<LPerson> persons = doGetObject(SERVICE + "/" + PERSON);
        assertEquals(4, persons.size());
        // Retrieve person informations via specific URL and test it.
        for (LPerson person : persons)
        {
            if ("gch".equals(person.getLogin()))
            {
                LPerson personWithId = doGetObject(SERVICE + "/" + PERSON + person.getId());
                assertEquals("gch", personWithId.getLogin());
                assertEquals("Guillaume", personWithId.getFirstName());
                assertEquals("Chatelet", personWithId.getLastName());
            }
            else if ("eth".equals(person.getLogin()))
            {
                LPerson personWithId = doGetObject(SERVICE + "/" + PERSON + person.getId());
                assertEquals("eth", personWithId.getLogin());
                assertEquals("Thomas", personWithId.getFirstName());
                assertEquals("Eskénazi", personWithId.getLastName());
            }
            else if ("acs".equals(person.getLogin()))
            {
                LPerson personWithId = doGetObject(SERVICE + "/" + PERSON + person.getId());
                assertEquals("acs", personWithId.getLogin());
                assertEquals("Arnaud", personWithId.getFirstName());
                assertEquals("Chassagne", personWithId.getLastName());
            }
        }
    }

    /**
     * Test Person creation through web services (POST /hd3dServices/persons/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonsPOST() throws IOException
    {
        ILPerson person = LPerson.getNewInstance("jdoe", "Jane", "Doe", null, null, null, 0L);

        WebResponse response = doPostAtURL(SERVICE + "/" + PERSON, person);
        assertEquals(CREATION_OK, response.getText());
        ILPerson createdPerson = this.getPerson("jdoe");
        assertEquals(person.getLogin(), createdPerson.getLogin());
        assertEquals(person.getFirstName(), createdPerson.getFirstName());
        assertEquals(person.getLastName(), createdPerson.getLastName());
    }

    /**
     * Test Person deletion through web services (DELETE /hd3dServices/persons/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonDELETE() throws IOException
    {
        ILPerson person = getPerson("eth");
        WebResponse response = doDELETEAtURL(SERVICE + "/" + PERSON + person.getId());
        assertTrue(response.getResponseCode() == 200);

        ILPerson deletedPerson = getPerson("eth");
        assertNull(deletedPerson);

        /* Due to a tomcat bug (?), accents i response messages are not properly displayed */
        // /!\ DO NOT DESACTIVATE THIS TEST /!\
        // assertTrue(response.getResponseMessage().contentEquals("Méthode Non Autorisée")
        // || response.getResponseMessage().contentEquals("Method Not Allowed"));
    }

    /**
     * It must be not possible to delete the root user
     * 
     * @throws IOException
     */
    @Test
    public void testRootDELETE() throws IOException
    {
        ILPerson person = getPerson("root");
        WebResponse response = doDELETEAtURL(SERVICE + "/" + PERSON + person.getId());
        assertTrue(response.getResponseCode() == 400);

        ILPerson deletedPerson = getPerson("root");
        assertNotNull(deletedPerson);
    }

    /**
     * Test Person modification through web services (PUT /hd3dServices/person/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonPUT() throws IOException
    {
        ILPerson person = getPerson("eth");
        person.setFirstName("Bob");
        person.setLastName("Maurane");
        doPUTAtURL(SERVICE + "/" + PERSON + person.getId(), person);
        ILPerson createdPerson = getPerson("eth");
        assertEquals("Bob", createdPerson.getFirstName());
        assertEquals("Maurane", createdPerson.getLastName());
    }

    /**
     * Test like condition on login, FirstName and LastName attributes.
     * 
     * @throws IOException
     */
    @Test
    public void testPersonLike() throws IOException
    {
        String constraint = getLikeConstraint("login", "%h");
        Collection<LPerson> persons = doGetObject(SERVICE + "/" + PERSON + constraint);
        assertEquals(2, persons.size());
        for (LPerson person : persons)
        {
            if ("gch".equals(person.getLogin()))
            {
                assertEquals("Guillaume", person.getFirstName());
                assertEquals("Chatelet", person.getLastName());
            }
            else if ("eth".equals(person.getLogin()))
            {
                assertEquals("Thomas", person.getFirstName());
                assertEquals("Eskénazi", person.getLastName());
            }
            else
            {
                fail("unexpected person :" + person.getName());
            }
        }
        // Test lastName column
        constraint = getLikeConstraint("lastName", "%hatelet");
        persons = doGetObject(SERVICE + "/" + PERSON + constraint);
        assertEquals(1, persons.size());
        ILPerson person = persons.iterator().next();
        assertEquals("gch", person.getLogin());
        assertEquals("Guillaume", person.getFirstName());
        assertEquals("Chatelet", person.getLastName());
        // Test firstName column
        constraint = getLikeConstraint("firstName", "%d");
        persons = doGetObject(SERVICE + "/" + PERSON + constraint);
        assertEquals(1, persons.size());
        person = persons.iterator().next();
        assertEquals("acs", person.getLogin());
        assertEquals("Arnaud", person.getFirstName());
        assertEquals("Chassagne", person.getLastName());
    }
}
