package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LProject;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PersonProjectServiceTest extends AbstractServiceIntegrationTest<ILPerson>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    /**
     * Test Persons retrieved through web services (GET /hd3dServices/groups/{groupId}/persons/).
     * 
     * @throws IOException
     */
    @Test
    public void testProjectPersonsGET() throws Exception
    {

        ILPerson gch = this.getPerson("gch");
        final String url = this.getPersonUrl(gch) + "/projects/";

        Collection<LProject> projects = doGetObject(url);

        assertEquals(2, projects.size());
        for (LProject project : projects)
        {
            if ("ProjectA".equals(project.getName()))
            {
                assertEquals("#FF0000", project.getColor());
            }
            else if ("ProjectB".equals(project.getName()))
            {
                assertEquals("#0000FF", project.getColor());
            }
            else if (!project.getName().startsWith("Project"))
            {
                fail("unexpected project :" + project.getName());
            }
        }
    }

    /**
     * Test Persons retrieved through web services (GET /hd3dServices/groups/{groupId}/persons/).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonProjectGET() throws Exception
    {

        // Get project URL
        ILProject projectA = this.getProjectA();
        ILPerson eth = this.getPerson("eth");
        final String url = this.getPersonUrl(eth) + "/projects/" + projectA.getId();

        // Get and validate project
        final ILProject project = this.doGetObject(url);
        assertNotNull(project);
        assertEquals(projectA.getId(), project.getId());
        assertEquals(projectA.getName(), project.getName());

        // Validate that nothing is returned when the project ID is wrong.
        final ILProject projectC = this.getProjectC();
        final String urlNull = this.getPersonUrl(eth) + "/projects/" + projectC.getId();
        final ILProject projectNull = this.doGetObject(urlNull);
        assertNull(projectNull);
    }
}
