package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.COMPOSITIONS;
import static fr.hd3d.common.client.ServicesURI.SEQUENCES;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILComposition;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LComposition;
import fr.hd3d.model.lightweight.impl.LSequence;


/**
 * Tests web service Constituents in Project categories: list (GET) and singleton (CRUD)
 * 
 * @author Try LAM
 */
public class CompositionsServiceTest extends AbstractServiceIntegrationTest<ILComposition>
{

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=SequenceShots");
    }

    @Test
    public void testCompositionPOST() throws IOException
    {
        // create a Composition
        ILConstituent constituent = getProjectADecorConstituent("const 1 label");
        ILComposition compo = LComposition.getNewInstance(null, null, null, "myCompo", "descrition", constituent
                .getId(), null, null, null, null, null);
        String url = SERVICE + '/' + COMPOSITIONS;
        WebResponse response = doPostAtURL(url, compo);
        assertEquals(CREATION_OK, response.getText());

        // create it again, it must not be possible (same label)
        response = doPostAtURL(url, compo);
        assertEquals(400, response.getResponseCode());// error is expected

        // update another field (not subject to any constraint) but keep the same label
        // it must not be possible
        LSequence seq = getProjectASequence();
        ILComposition compo_1 = LComposition.getNewInstance(null, null, null, "myCompo", "toto", null, null, null, seq
                .getId(), null, null);
        response = doPostAtURL(url, compo_1);
        assertEquals(400, response.getResponseCode());// error is expected

    }

    /**
     * Test constituent MULITPLE CREATION through web services (POST
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents).
     * 
     * @throws IOException
     */
    // @Test
    // public void testBulkConstituentsPOST() throws IOException
    // {
    // // get the project A
    // ILProject projectA = getProjectA();
    // // get the category
    // ILCategory category = getProjectADecorCategory();
    // // create new constituents
    // List<ILConstituent> constituents = new ArrayList<ILConstituent>();
    // final int NUMBERCONSTITUENTS = 3;
    // for (int i = 0; i < NUMBERCONSTITUENTS; i++)
    // {
    // constituents.add(LConstituent.getNewInstance(category.getId(), category.getName(), 0,
    // "New constituent Description 日本", 5, "New constituent hook", "New constituent Label日本" + i, 5, 0,
    // 0, 0, 0, 0, 0));
    // }
    // String url = getProjectUrl(projectA) + CATEGORIES + "/" + category.getId() + "/constituents";
    // WebResponse response = doPostAtURL(url + "?mode=bulk", constituents);
    // assertEquals(CREATION_OK, response.getText());
    // // get the new constituents
    // List<ILConstituent> newConstituents = doGetObject(url);
    // int count = 0;
    // for (ILConstituent c : newConstituents)
    // {
    // if (c.getLabel().startsWith("New constituent Label日本"))
    // {
    // count++;
    // }
    // }
    // assertEquals(NUMBERCONSTITUENTS, count);
    // }
    /**
     * Test constituent UPDATE through web services (PUT
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents/{constituentId}).
     * 
     * @throws IOException
     */
    // @Test
    // public void testConstituentsPUT() throws IOException
    // {
    // final String updated = "const 1 label日本";
    // ILConstituent const1 = getProjectADecorConstituent("const 1 label");
    // const1.setLabel(updated);
    // String url = getProjectADecorCategoryUrl() + "/constituents/" + const1.getId();
    // doPUTAtURL(url, const1);
    // ILConstituent const1Updated = doGetObject(url);
    // // read back
    // assertEquals(updated, const1Updated.getLabel());
    // }
    /**
     * Test constituent BULK UPDATE through web services (PUT
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents/).
     * 
     * @throws IOException
     */
    // @Test
    // public void testConstituentsPUTBulk() throws IOException
    // {
    //
    // ILConstituent const1 = getProjectADecorConstituent("const 1 label");
    // const1.setLabel("const 1 label UPDATED");
    // ILConstituent const2 = getProjectADecorConstituent("const 2 label");
    // const2.setLabel("const 2 label UPDATED");
    // List<ILConstituent> constituents = new ArrayList<ILConstituent>();
    // constituents.add(const1);
    // constituents.add(const2);
    // String url = getProjectADecorCategoryUrl() + "/constituents/";
    // doPUTAtURL(url, constituents);
    // // read back
    // ILConstituent const1Updated = doGetObject(url + const1.getId());
    // assertEquals("const 1 label UPDATED", const1Updated.getLabel());
    // ILConstituent const2Updated = doGetObject(url + const2.getId());
    // assertEquals("const 2 label UPDATED", const2Updated.getLabel());
    // }
    // convenience method
    private String getProjectADecorCategoryUrl() throws IOException
    {
        String ret = null;

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {
            if ("Decor".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
                // get the subcategories
                url = url + "/" + category.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }

    // convenience method
    private ILCategory getProjectADecorCategory() throws IOException
    {
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {

            if ("Decor".equals(category.getName()))
            {
                return category;
            }
        }
        return null;
    }

    // convenience method
    private ILConstituent getProjectADecorConstituent(String label) throws IOException
    {
        String url = getProjectADecorCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        for (ILConstituent c : constituents)
        {
            if (c.getLabel().equals(label))
            {
                return c;// actually, it may have several ones, but just return the first here
            }
        }
        return null;
    }

    // convenience method
    private LSequence getProjectASequence() throws IOException
    {

        ILProject projectA = getProjectA();
        final String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<LSequence> sequences = doGetObject(url);
        // retrieve the main sequences (with "sequence" as parent) for the
        // project A
        assertEquals(2, sequences.size());
        for (LSequence sequence : sequences)
        {
            if ("Pilote".equals(sequence.getName()))
            {
                assertEquals(projectA.getId(), sequence.getProject());
                return sequence;
            }
            // TODO check other categories
        }
        return null;
    }
}
