package fr.hd3d.services.integration;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILRole;
import fr.hd3d.model.lightweight.impl.LRole;


public class RoleServiceTest extends AbstractServiceIntegrationTest<ILRole>
{

    public static String ROLE = "roles/";

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    /**
     * Test Roles retrieved through web services (GET /hd3dServices/roles/).
     * 
     * @throws IOException
     */
    @Test
    public void testGetRoles() throws Exception
    {

        final Collection<ILRole> roles = this.doGetObject(SERVICE + ROLE);
        assertNotNull(roles);

        // Look for Test_group_00 and Test_group_01
        for (ILRole role : roles)
        {
            if ("Test_role_00".equals(role.getName()))
            {
                assertEquals(1, role.getResourceGroupNames().size());
                assertEquals(2, role.getPermissions().size());
            }
            else if ("Test_role_01".equals(role.getName()))
            {
                assertEquals(0, role.getResourceGroupNames().size());
                assertEquals(0, role.getPermissions().size());
            }
        }
    }

    /**
     * Test Role retrieved through web services (GET /hd3dServices/roles/).
     * 
     * @throws IOException
     */
    @Test
    public void testRoleGET() throws IOException
    {

        // Get role
        final ILRole role = this.getRole("Test_role_00");
        final ILRole roleUrl = this.doGetObject(this.getRoleUrl(role));

        // Validate role
        assertEquals(1, roleUrl.getResourceGroupNames().size());
        assertEquals("Test_role_00", roleUrl.getName());

        int okGuys = 0;
        for (String perm : roleUrl.getPermissions())
        {
            if (perm.equals("v1:persons:*:read") || perm.equals("v1:projects:*:read"))
            {
                okGuys++;
            }
        }
        assertEquals(2, okGuys);

        // Validate that nothing is returned when the group ID is wrong.
        final ILRole roleNull = this.doGetObject(SERVICE + ROLE + "666666/");
        assertNull(roleNull);
    }

    /**
     * Test Role creation through web services (POST /hd3dServices/project/roles/).
     * 
     * @throws IOException
     */
    @Test
    public void testRolePOST() throws IOException
    {

        final LRole role = new LRole();

        role.setName("Test_role_POST");
        WebResponse response = doPostAtURL(SERVICE + ROLE, role);
        assertEquals(CREATION_OK, response.getText());
    }

    /**
     * Test Role deletion through web services (DELETE /hd3dServices/roles).
     * 
     * @throws IOException
     */
    @Test
    public void testRoleDELETE() throws IOException
    {

        final ILRole role = this.getRole("Test_role_02");
        assertNotNull(role);

        // Delete role.
        doDELETEAtURL(this.getRoleUrl(role));

        // Ensure that role does not exist anymore.
        String constraint = "?constraint=[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + role.getName() + "\"}]";
        Collection<ILProject> roles = doGetObject(SERVICE + ROLE + constraint);
        assertEquals(0, roles.size());
    }

    /**
     * Test role update through web services (PUT /hd3dServices/roles).
     * 
     * @throws IOException
     */
    @Test
    public void testRolePUT() throws IOException
    {
        ILRole role = this.getRole("Test_role_00");
        String url = this.getRoleUrl(role);

        role.setName("Test_role_UPDATE");

        doPUTAtURL(url, role);

        ILRole upRole = this.doGetObject(url);

        if (upRole == null)
            upRole = this.doGetObject(url);// try again

        assertNotNull(upRole);
        assertEquals("Test_role_UPDATE", upRole.getName());
    }

    /**
     * Test Like constraint through web services (GET /hd3dServices/roles).
     * 
     * @throws IOException
     */
    @Test
    public void testRoleLike() throws IOException
    {

        // %25 means % (URL compliance)
        String constraint = getLikeConstraint("name", "Test_role_%");

        Collection<ILRole> roles = this.doGetObject(SERVICE + ROLE + constraint);
        assertEquals(100, roles.size());
    }

    /**
     * Retrieve Role from its name via web service with EQUAL constraint.
     * 
     * @param name
     *            Role name
     * @return The group whose name is <i>name</i>
     * @throws IOException
     */
    public static ILRole getRole(String name) throws IOException
    {
        try
        {
            String constraint = "?constraint="
                    + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + name + "\"}]", "UTF-8");

            final Collection<ILRole> roles = doGetObject(SERVICE + ROLE + constraint);

            assertNotNull(roles);
            // assertTrue(groups.size() == 1);
            return roles.iterator().next();
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static String getRoleUrl(ILRole role)
    {

        assertNotNull(role);
        return SERVICE + ROLE + role.getId().toString();
    }

}
