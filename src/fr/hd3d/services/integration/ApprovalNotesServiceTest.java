package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.lightweight.impl.LApprovalNote;
import fr.hd3d.model.lightweight.impl.LApprovalNoteType;
import fr.hd3d.model.lightweight.impl.LTask;
import fr.hd3d.model.lightweight.impl.LTaskType;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ApprovalNotesServiceTest extends AbstractServiceIntegrationTest<ILTask>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
    }

    public static ILTaskType createTaskType() throws Exception
    {
        /* create */
        final ILProject projectA = getProjectA();

        ILTaskType taskType = new LTaskType();
        taskType.setProjectID(projectA.getId());
        taskType.setColor("#000000");
        taskType.setName("MyTaskType");

        final WebResponse response = doPostAtURL(SERVICE + ServicesURI.TASK_TYPES, taskType);
        assertEquals(CREATION_OK, response.getText());
        /* read back */
        final String createdTaskTypeUrl = response.getHeaderField("CONTENT-LOCATION");
        ILTaskType createdTaskType = doGetObject(createdTaskTypeUrl);
        assertNotNull(createdTaskType);
        return createdTaskType;
    }

    public static ILTask createTask() throws Exception
    {
        /* create */
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        ILConstituent constituent = ConstituentsServiceTest.getProjectAPersonnageConstituent("const 1 label");
        ILTaskType taskType = createTaskType();
        ILTask newTask = LTask.getNewInstance(taskType.getProjectID(), "myTask", ETaskStatus.defaultStatus(), null,
                null, constituent.getId(), "Constituent", taskType.getId());
        final WebResponse response = doPostAtURL(projectUrl + "/tasks", newTask);
        assertEquals(CREATION_OK, response.getText());
        /* read back */
        final String createdTaskUrl = response.getHeaderField("CONTENT-LOCATION");
        ILTask createdTask = doGetObject(createdTaskUrl);
        assertNotNull(createdTask);
        return createdTask;
    }

    public static ILApprovalNoteType createApprovalNoteType(Long taskType, Long projectId) throws Exception
    {
        /* create */
        ILApprovalNoteType approvalNoteType = LApprovalNoteType.getNewInstance("myApprovalNoteType", taskType,
                projectId);
        final WebResponse response = doPostAtURL(SERVICE + ServicesURI.APPROVALNOTETYPES, approvalNoteType);
        assertEquals(CREATION_OK, response.getText());

        /* read back */
        final String createdApprovalNoteTypeUrl = response.getHeaderField("CONTENT-LOCATION");
        ILApprovalNoteType createdApprovalNoteType = doGetObject(createdApprovalNoteTypeUrl);
        assertNotNull(createdApprovalNoteType);
        return createdApprovalNoteType;
    }

    @Test
    public void testPostApprovalNote() throws Exception
    {
        /* create approvalNote */
        ILTask task = createTask();
        ILApprovalNoteType approvalNoteType = createApprovalNoteType(task.getTaskTypeID(), task.getProjectID());
        ILApprovalNote approvalNote = LApprovalNote.getNewInstance(new Date(), EApprovalNoteStatus.defaultStatus()
                .toString(), "myApprovalNote", null, task.getWorkObjectId(), task.getWorkObjectName(), null,
                approvalNoteType.getId(), null);
        final WebResponse response = doPostAtURL(SERVICE + ServicesURI.APPROVALNOTES, approvalNote);
        assertEquals(CREATION_OK, response.getText());
        /* read back */
        final String createdApprovalNoteUrl = response.getHeaderField("CONTENT-LOCATION");
        ILApprovalNote createdApprovalNote = doGetObject(createdApprovalNoteUrl);
        assertNotNull(createdApprovalNote);

    }

    // @Test
    // public void testGetTask() throws Exception
    // {
    // final ILProject projectA = getProjectA();
    // final String projectUrl = getProjectUrl(projectA);
    // String constraint = "?orderBy=[\"id\"]}]";
    // final Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks" + constraint);
    // assertEquals(4, tasks.size());
    // final ILTask task = doGetObject(projectUrl + "/tasks/" + ((ILTask) (tasks.iterator().next())).getId());
    // assertEquals(projectA.getName(), task.getProjectName());
    // assertEquals(projectA.getId(), task.getProjectID());
    // assertEquals(projectA.getColor(), task.getProjectColor());
    // assertEquals("Test_Task_00", task.getName());
    // assertEquals((Byte) (byte) 0, task.getCompletion());
    // assertNull(task.getCompletionDate());
    // assertEquals(ETaskStatus.OK.toString(), task.getStatus());
    // assertNotNull(task);
    // }
    //
    // @Test
    // public void testPostTask() throws IOException
    // {
    // final ILProject projectA = getProjectA();
    // final String projectUrl = getProjectUrl(projectA);
    // ILPerson gch = this.getPerson("gch");
    // Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks");
    // assertEquals(4, tasks.size());
    // final String TestTask = "The test task";
    // final ILTask newTask = LTask.getNewInstance(projectA.getId(), TestTask, ETaskStatus.WORK_IN_PROGRESS, gch
    // .getId(), gch.getId(), projectA.getId(), "Project", null);
    // final WebResponse response = doPostAtURL(projectUrl + "/tasks", newTask);
    // assertEquals(CREATION_OK, response.getText());
    // tasks = doGetObject(projectUrl + "/tasks");
    // assertEquals(5, tasks.size());
    // for (ILTask task : tasks)
    // {
    // if (TestTask.equals(task.getName()))
    // {
    // assertEquals(gch.getName(), task.getCreatorName());
    // assertEquals(gch.getId(), task.getCreatorID());
    // assertEquals(gch.getName(), task.getWorkerName());
    // assertEquals(gch.getId(), task.getWorkerID());
    // assertEquals(projectA.getName(), task.getProjectName());
    // assertEquals(projectA.getId(), task.getProjectID());
    // assertEquals(projectA.getColor(), task.getProjectColor());
    // assertEquals(ETaskStatus.WORK_IN_PROGRESS.toString(), task.getStatus());
    // }
    // }
    // }
    //
    // @Test
    // public void testUpdateTask() throws IOException
    // {
    // // get all the projectA tasks
    // final ILProject projectA = getProjectA();
    // final String tasksUrl = getProjectUrl(projectA) + "/tasks";
    // Collection<ILTask> tasks = doGetObject(tasksUrl);
    // assertEquals(4, tasks.size());
    // // get the first task
    // final ILTask task = tasks.iterator().next();
    // final java.util.Date date = new java.util.Date();
    // final String newTaskName = "New name";
    // task.setCompletion((Byte) (byte) 100);
    // task.setCompletionDate(date);
    // task.setName(newTaskName);
    // // submit the changes
    // final String taskUrl = tasksUrl + '/' + task.getId().toString();
    // doPUTAtURL(taskUrl, task);
    // // read it back
    // final ILTask newTask = doGetObject(taskUrl);
    // assertEquals(task.getCompletion(), newTask.getCompletion());
    // assertEqualsDate(task.getCompletionDate(), newTask.getCompletionDate());
    // assertEquals(task.getName(), newTask.getName());
    // }
    //
    // @Test
    // public void testDeleteDeletableTask() throws IOException
    // {
    // final ILProject projectA = getProjectA();
    // final String tasksUrl = getProjectUrl(projectA) + "/tasks";
    // Collection<ILTask> tasks = doGetObject(tasksUrl);
    // assertEquals(4, tasks.size());
    // for (ILTask task : tasks)
    // if (TEST_TASK.equals(task.getName()))
    // {
    // doDELETEAtURL(tasksUrl + '/' + task.getId().toString());
    // }
    // tasks = doGetObject(tasksUrl);
    // assertEquals(3, tasks.size());
    // }
    //
    // @Test
    // /*
    // * ONE_SUPER_TASK has an activity it upon and so cannot be removed without removing the underlying activity
    // */
    // public void testDeleteUndeletableTask() throws IOException
    // {
    // final ILProject projectA = getProjectA();
    // final String tasksUrl = getProjectUrl(projectA) + "/tasks";
    // Collection<ILTask> tasks = doGetObject(tasksUrl);
    // assertEquals(4, tasks.size());
    // for (ILTask task : tasks)
    // if (ONE_SUPER_TASK.equals(task.getName()))
    // {
    // doDELETEAtURL(tasksUrl + '/' + task.getId().toString());
    // }
    // tasks = doGetObject(tasksUrl);
    // assertEquals(4, tasks.size());
    // }
    //
    // @Test
    // public void testTaskLike() throws Exception
    // {
    // final ILProject projectA = getProjectA();
    // final String projectUrl = getProjectUrl(projectA);
    // String constraint = getLikeConstraint("name", "One%");
    // final Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks/" + constraint);
    // assertEquals(1, tasks.size());
    // ILTask task = tasks.iterator().next();
    // assertEquals(projectA.getName(), task.getProjectName());
    // assertEquals(projectA.getId(), task.getProjectID());
    // assertEquals(projectA.getColor(), task.getProjectColor());
    // assertEquals(ONE_SUPER_TASK, task.getName());
    // assertEquals((Byte) (byte) 0, task.getCompletion());
    // assertNull(task.getCompletionDate());
    // assertEquals(ETaskStatus.OK.toString(), task.getStatus());
    // }
    /**
     * TODO Write a test to check the PUT of an invalid task bad workerID or projectID regarding the url.
     */
}
