package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSimpleActivity;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskActivity;
import fr.hd3d.model.lightweight.impl.LSimpleActivity;
import fr.hd3d.model.lightweight.impl.LTaskActivity;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class PersonActivityServiceTest extends AbstractServiceIntegrationTest<ILActivity>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    /**
     * Test Activities retrieved through web services (GET /hd3dServices/activities/).
     * 
     * @throws IOException
     */
    @Test
    public void testGetActivities() throws Exception
    {
        int simpleActivityCount = 0;
        int taskActivityCount = 0;
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final Collection<ILActivity> activities = doGetObject(activitiesUrl);
        assertNotNull(activities);
        assertEquals(3, activities.size());
        for (ILActivity activity : activities)
        {
            if (activity instanceof ILSimpleActivity)
            {
                // Simple activities test
                ++simpleActivityCount;
                final ILSimpleActivity simpleActivity = (ILSimpleActivity) activity;
                assertNotNull(simpleActivity.getType());
                assertEquals(simpleActivity.getTaskName(), simpleActivity.getType());
            }
            else if (activity instanceof ILTaskActivity)
            {
                // Task activities test
                ++taskActivityCount;
                final ILTaskActivity taskActivity = (ILTaskActivity) activity;
                final Long taskID = taskActivity.getTaskID();
                assertNotNull(taskID);
            }
            else
            {
                fail("Unimplemented object " + activity.getClass().getName());
            }
            // Check Person attributes
            assertEquals(gch.getId(), activity.getWorkerID());
            assertEquals(gch.getName(), activity.getWorkerName());
            // Check some attributes
            assertNotNull(activity.getDuration());
            assertNotNull(activity.getDayID());
            assertNotNull(activity.getComment());
        }
        // Check activity number
        assertEquals(2, simpleActivityCount);
        assertEquals(1, taskActivityCount);
    }

    /**
     * Test simple activity retrieved through web services (GET /hd3dServices/activities/).
     * 
     * @throws IOException
     */
    @Test
    public void testSimpleActivityGET() throws IOException
    {
        // Get project and person associated to the test activity.
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY + "?orderBy=[\"id\"]";
        final List<ILActivity> activities = this.doGetObject(activitiesUrl);
        assertEquals(3, activities.size());
        // Get the test activity.
        final ILActivity activity = activities.get(2);
        assertNotNull(activity);
        // Validate project data in the test activity.
        assertEquals(projectA.getName(), activity.getProjectName());
        assertEquals(projectA.getId(), activity.getProjectID());
        assertEquals(projectA.getColor(), activity.getProjectColor());
        // Validate person data in the test activity.
        assertEquals(gch.getName(), activity.getWorkerName());
        assertEquals(gch.getId(), activity.getWorkerID());
        assertEquals(gch.getLogin(), activity.getWorkerLogin());
        // Validate other data in the test activity.
        final String meetingString = ESimpleActivityType.MEETING.toString();
        assertEquals(meetingString, activity.getTaskName());
        assertNotNull(activity.getDayID());
        assertNotNull(activity.getDuration());
        assertNotNull(activity.getComment());
    }

    /**
     * Test task activity retrieved through web services (GET /hd3dServices/activities/).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskActivityGET() throws IOException
    {
        // Get project and person associated to the test activity.
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final List<ILActivity> activities = this.doGetObject(activitiesUrl);
        assertEquals(3, activities.size());
        // Get the test activity.
        ILActivity activity = new LSimpleActivity();
        Iterator<ILActivity> activityIterator = activities.iterator();
        while (activity instanceof ILSimpleActivity && activityIterator.hasNext())
        {
            activity = doGetObject(activitiesUrl + activityIterator.next().getId().toString());
            assertNotNull(activity);
        }
        // Validate project data in the test activity.
        assertEquals(projectA.getName(), activity.getProjectName());
        assertEquals(projectA.getId(), activity.getProjectID());
        assertEquals(projectA.getColor(), activity.getProjectColor());
        // Validate person data in the test activity.
        assertEquals(gch.getName(), activity.getWorkerName());
        assertEquals(gch.getId(), activity.getWorkerID());
        assertEquals(gch.getLogin(), activity.getWorkerLogin());
        // Validate other data in the test activity.
        final String taskString = "One super task";
        assertEquals(taskString, activity.getTaskName());
        assertNotNull(activity.getDayID());
        assertNotNull(activity.getDuration());
        assertNotNull(activity.getComment());
    }

    /**
     * Test simple activity creation through web services (POST /hd3dServices/project/activities/).
     * 
     * @throws IOException
     */
    @Test
    public void testSimpleActivityPOST() throws IOException
    {
        // Retrieve project and person data.
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final List<ILActivity> activities = this.doGetObject(activitiesUrl);
        assertEquals(3, activities.size());
        // Create new activity.
        final Date startDate = new Date();
        final Long duration = 10L;
        final String comment = Double.toString(Math.random());
        final ILPersonDay day = this.getDay(gch.getId());
        final ILActivity newActivity = LSimpleActivity.getNewInstance(ESimpleActivityType.MEETING, day.getId(),
                duration, comment, gch.getId(), projectA.getId(), gch.getId(), startDate);
        // Check the server response.
        final WebResponse response = doPostAtURL(activitiesUrl, newActivity);
        assertEquals(CREATION_OK, response.getText());
        // Refresh activities data
        Collection<ILActivity> activities2 = doGetObject(activitiesUrl);
        assertEquals(4, activities2.size());
        // Look for the newly created activity.
        for (ILActivity activity : activities2)
        {
            if (comment.equals(activity.getComment()))
            {
                // Check the activity type
                if (activity instanceof ILSimpleActivity == false)
                {
                    fail("The created activity is not a simple activity");
                }
                // Verify date and duration data.
                final ILSimpleActivity simpleActivity = (ILSimpleActivity) activity;
                assertEquals(day.getId(), simpleActivity.getDayID());
                assertEquals(startDate.toString(), simpleActivity.getFilledDate().toString());
                assertEquals(duration, simpleActivity.getDuration());
                // Verify type and name
                final String meetingString = ESimpleActivityType.MEETING.toString();
                assertEquals(meetingString, simpleActivity.getType());
                assertEquals(meetingString, simpleActivity.getTaskName());
                // Verify project and person
                assertEquals(gch.getId(), simpleActivity.getWorkerID());
                assertEquals(projectA.getId(), simpleActivity.getProjectID());
                return;
            }
        }
        fail("The created activity has not been found ...");
    }

    /**
     * Test task activity creation through web services (POST /hd3dServices/project/activities/).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskActivityPOST() throws IOException
    {
        // Retrieve project and person data.
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final String tasksUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/tasks/";
        final List<ILActivity> activities = doGetObject(activitiesUrl);
        final List<ILTask> tasks = doGetObject(tasksUrl);
        final ILTask task = tasks.get(0);
        assertEquals(3, activities.size());
        // Create new activity.
        final Date startDate = new Date();
        final Long duration = 10L;
        final String comment = Double.toString(Math.random());
        final ILPersonDay day = this.getDay(gch.getId());
        final ILActivity newActivity = LTaskActivity.getNewInstance(day.getId(), day.getDate(), duration, comment,
                task, gch.getId(), projectA.getId(), gch.getId(), startDate);
        // Check the server response.
        final WebResponse response = doPostAtURL(activitiesUrl, newActivity);
        assertEquals(CREATION_OK, response.getText());
        // Refresh activities data
        Collection<ILActivity> activities2 = doGetObject(activitiesUrl);
        assertEquals(4, activities2.size());
        // Look for the newly created activity.
        for (ILActivity activity : activities2)
        {
            if (comment.equals(activity.getComment()))
            {
                // Check the activity type
                if (activity instanceof ILTaskActivity == false)
                {
                    fail("The created activity is not a task activity");
                }
                final ILTaskActivity taskActivity = (ILTaskActivity) activity;
                // Verify date an duration data.
                assertEquals(startDate.toString(), taskActivity.getFilledDate().toString());
                assertEquals(duration, taskActivity.getDuration());
                assertEquals(task.getName(), taskActivity.getTaskName());
                assertEquals(task.getId(), taskActivity.getTaskID());
                // Verify name
                assertEquals(task.getName(), taskActivity.getTaskName());
                // Verify project and person
                assertEquals(gch.getId(), taskActivity.getWorkerID());
                assertEquals(projectA.getId(), taskActivity.getProjectID());
                assertEquals(day.getId(), taskActivity.getDayID());
                return;
            }
        }
        fail("The created activity has not been found ...");
    }

    /**
     * Test simple activity update through web services (PUT /hd3dServices/activities).
     * 
     * @throws IOException
     */
    @Test
    public void testSimpleActivityPUT() throws IOException
    {
        // Get person and project data.
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        final List<ILActivity> activities = this.doGetObject(activitiesUrl);
        // Get the test activity.
        assertEquals(3, activities.size());
        final ILSimpleActivity activity = this.doGetObject(SERVICE + ACTIVITY + activities.get(2).getId().toString());
        assertNotNull(activity);
        // Update the activity
        activity.setType(ESimpleActivityType.TECHNICAL_INCIDENT.toString());
        final Date filledDate = new Date(5000);
        activity.setFilledDate(filledDate);
        activity.setDuration(50L);
        activity.setComment("Fire in the place!!!");
        doPUTAtURL(activitiesUrl + activity.getId(), activity);
        // Check if the update worked correctly.
        ILSimpleActivity updatedActivity = doGetObject(activitiesUrl + activity.getId());
        assertEquals(ESimpleActivityType.TECHNICAL_INCIDENT.toString(), updatedActivity.getType());
        assertEquals(filledDate.toString(), updatedActivity.getFilledDate().toString());
        assertEquals(new Long(50), updatedActivity.getDuration());
        assertEquals("Fire in the place!!!", updatedActivity.getComment());
        assertEquals(gch.getId(), updatedActivity.getWorkerID());
    }

    /**
     * Test task activity update through web services (PUT /hd3dServices/activities).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskActivityPUT() throws IOException
    {
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        // Get the task activity to update
        String constraint = "?constraint=[{\"type\":\"eq\",\"column\":\"comment\",\"value\":\"Did a few things\"}]";
        final Collection<ILTaskActivity> activities = doGetObject(activitiesUrl + constraint);
        assertNotNull(activities);
        ILTaskActivity activity = activities.iterator().next();
        // Update the task activity
        final Date filledDate = new Date(5000);
        String newComment = "Fire in the place!!!";
        activity.setFilledDate(filledDate);
        activity.setDuration(50L);
        activity.setComment(newComment);
        doPUTAtURL(SERVICE + ACTIVITY + activity.getId(), activity);
        // Check if the update worked correctly.
        ILTaskActivity updatedActivity = doGetObject(SERVICE + ACTIVITY + activity.getId());
        assertEquals(filledDate.toString(), updatedActivity.getFilledDate().toString());
        assertEquals(new Long(50), updatedActivity.getDuration());
        assertEquals(newComment, updatedActivity.getComment());
        assertEquals(gch.getId(), updatedActivity.getWorkerID());
    }

    /**
     * Test task activity update through web services (DELETE /hd3dServices/activities/).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskActivityDELETE() throws IOException
    {
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        // Get the task activity to delete
        String constraint = "?constraint=[{\"type\":\"eq\",\"column\":\"comment\",\"value\":\"Did a few things\"}]";
        final Collection<ILTaskActivity> activities = doGetObject(activitiesUrl + constraint);
        assertNotNull(activities);
        ILTaskActivity activity = activities.iterator().next();
        // Delete activity.
        doDELETEAtURL(this.getActivityUrl(activity));
        // Ensure that activity does not exist anymore.
        Collection<ILActivity> activitiesAll = doGetObject(SERVICE + ACTIVITY);
        assertEquals(6, activitiesAll.size());
        final Collection<ILActivity> gchActivities = this.doGetObject(activitiesUrl);
        assertEquals(2, gchActivities.size());
    }

    /**
     * Test Like constraint through web services (GET /hd3dServices/activities).
     * 
     * @throws IOException
     */
    @Test
    public void testActivityLike() throws Exception
    {
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String activitiesUrl = this.getPersonUrl(gch) + "/" + ACTIVITY;
        String constraint = getLikeConstraint("comment", "A simple task for %");
        final Collection<ILActivity> activities = doGetObject(activitiesUrl + constraint);
        assertEquals(1, activities.size());
        ILActivity activity = activities.iterator().next();
        // Validate project data in the test activity.
        assertEquals(projectA.getName(), activity.getProjectName());
        assertEquals(projectA.getId(), activity.getProjectID());
        assertEquals(projectA.getColor(), activity.getProjectColor());
        // Validate person data in the test activity.
        assertEquals(gch.getName(), activity.getWorkerName());
        assertEquals(gch.getId(), activity.getWorkerID());
        assertEquals(gch.getLogin(), activity.getWorkerLogin());
        // Validate other data in the test activity.
        assertNotNull(activity.getDayID());
        assertNotNull(activity.getDuration());
        assertEquals("A simple task for gch", activity.getComment());
    }
}
