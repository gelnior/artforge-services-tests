package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.PLANNINGS;
import static fr.hd3d.common.client.ServicesURI.TASK_CHANGES;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskChanges;
import fr.hd3d.model.lightweight.impl.LTaskChanges;


public class PlanningTaskChangesTest extends AbstractServiceIntegrationTest<ILTaskChanges>
{

    private static final int ONE_WORKING_DAY = 8 * 60 * 60;// in seconds
    private static final int ONE_DAY = 24 * 60 * 60;// in seconds
    private static final String TEST_TASK = "Test_Task_00";

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    @Test
    public void testGetTaskChanges() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskChangesUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_CHANGES;
        final Collection<ILTaskChanges> taskChangesCollection = doGetObject(planningTaskChangesUrl);
        assertNotNull(taskChangesCollection);
        assertEquals(3, taskChangesCollection.size());
        for (ILTaskChanges taskChanges : taskChangesCollection)
        {
            assertEquals(planning.getId(), taskChanges.getPlanningID());
            assertNotNull(taskChanges.getTaskID());
            assertNotNull(taskChanges.getWorkerID());
        }
    }

    @Test
    public void testPostTaskChanges() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPerson("gch");
        final ILTask testTask = this.getTaskInProject(TEST_TASK, projectUrl);
        final String planningUrl = projectUrl + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskChangesUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_CHANGES;
        Date startDate = new Date();
        Date endDate = new Date(startDate.getTime() + 3 * ONE_DAY * 1000L);
        Long duration = new Long(3 * ONE_WORKING_DAY);
        ILTaskChanges taskChanges = LTaskChanges.getNewInstance(startDate, endDate, duration, gch.getId(), planning
                .getId(), testTask);
        final WebResponse response = doPostAtURL(planningTaskChangesUrl, taskChanges);
        assertEquals(CREATION_OK, response.getText());
        Collection<ILTaskChanges> taskChangesCollection = doGetObject(planningTaskChangesUrl);
        assertEquals(4, taskChangesCollection.size());
    }

    @Test
    public void testPutTaskChanges() throws Exception
    {
        // Get project data
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final String planningUrl = projectUrl + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        // Get planning data
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        assertNotNull(plannings);
        assertEquals(2, plannings.size());
        final ILPlanning planning = plannings.iterator().next();
        // Get task changes data
        final String planningTaskChangesUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_CHANGES;
        final List<ILTaskChanges> taskChangesList = doGetObject(planningTaskChangesUrl);
        assertEquals(3, taskChangesList.size());
        // Update the task changes
        final ILTaskChanges taskChanges = taskChangesList.iterator().next();
        assertNotNull(taskChanges);
        Date newEndDate = new Date(taskChanges.getEndDate().getTime() + 5 * ONE_DAY * 1000L);// end date is postponed
        taskChanges.setEndDate(newEndDate);
        doPUTAtURL(planningTaskChangesUrl + "/" + taskChanges.getId(), taskChanges);
        // Check if the update worked correctly.
        ILTaskChanges updatedTaskChanges = doGetObject(planningTaskChangesUrl + "/" + taskChanges.getId());
        assertEquals(newEndDate, updatedTaskChanges.getEndDate());
        assertEquals(taskChanges.getWorkerID(), updatedTaskChanges.getWorkerID());
        assertEquals(taskChanges.getPlanningID(), updatedTaskChanges.getPlanningID());
        assertEquals(taskChanges.getTaskID(), updatedTaskChanges.getTaskID());
    }
}
