package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.impl.LPersonDay;


/**
 * 
 * @author David Gauthier
 * 
 */
public class PersonDayTest extends AbstractServiceIntegrationTest<ILPersonDay>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    @Test
    public void testGETPersonDays() throws IOException
    {
        ILPerson gch = this.getPerson("gch");
        Collection<LPersonDay> days = doGetObject(SERVICE + PERSON + gch.getId() + "/" + DAYS_AND_ACTIVITIES);
        assertEquals(1, days.size());
    }

    @Test
    public void testPOSTPersonDays() throws IOException
    {
        Date date = new Date();
        final ILPerson gch = this.getPerson("gch");
        final ILPersonDay newDay = LPersonDay.getNewInstance(date, gch.getId());
        WebResponse response = doPostAtURL(SERVICE + DAYS_AND_ACTIVITIES, newDay);
        assertEquals(CREATION_OK, response.getText());

        // read back
        Collection<ILPersonDay> days = doGetObject(SERVICE + DAYS_AND_ACTIVITIES);
        assertEquals(3, days.size());
        for (ILPersonDay day : days)
        {
            if (day.equals(newDay))
            {
                assertEquals(newDay.getDate(), day.getDate());
                assertEquals(gch.getId(), day.getPersonID());
                assertEquals(null, day.getActivitiesApprovedByID());
                assertEquals(null, day.getActivitiesApprovedDate());
            }
        }
    }

    @Test
    public void testPUTPersonDays() throws IOException
    {
        final ILPerson gch = this.getPerson("gch");
        final ILPerson eth = this.getPerson("eth");
        Collection<ILPersonDay> days = doGetObject(SERVICE + PERSON + gch.getId() + "/" + DAYS_AND_ACTIVITIES);
        assertEquals(days.size(), 1);

        ILPersonDay day = days.iterator().next();
        Date originalDate = day.getDate();
        Long originalPersonID = day.getPersonID();

        // update one user's personday with the values of the second one
        Date approvedDate = new Date();
        day.setActivitiesApprovedByID(eth.getId());
        day.setActivitiesApprovedDate(approvedDate);
        day.setDate(approvedDate);
        day.setPersonID(eth.getId());
        String url = SERVICE + PERSON + gch.getId() + "/" + DAYS_AND_ACTIVITIES + day.getId();
        doPUTAtURL(url, day);

        // read back
        LPersonDay updatedPersonDay = doGetObject(url);
        assertEquals(eth.getId(), updatedPersonDay.getActivitiesApprovedByID());
        assertEquals(approvedDate.toString(), updatedPersonDay.getActivitiesApprovedDate().toString());
        // On ne change pas la personne proprietaire d'une journée
        assertEquals(originalPersonID, updatedPersonDay.getPersonID());
        // On ne change pas la date d'une journée
        assertEquals(originalDate.toString(), updatedPersonDay.getDate().toString());
    }

    @Test
    public void testDELETEPersonDays() throws IOException
    {
        // get the user and his days&activities
        final ILPerson gch = this.getPerson("gch");
        Collection<ILPersonDay> days = doGetObject(SERVICE + PERSON + gch.getId() + "/" + DAYS_AND_ACTIVITIES);
        assertEquals(1, days.size());

        // try to disable it
        ILPersonDay day = days.iterator().next();
        String url = SERVICE + PERSON + gch.getId() + "/" + DAYS_AND_ACTIVITIES + day.getId();
        WebResponse response = doDELETEAtURL(url);
        assertEquals(200, response.getResponseCode());
    }
}
