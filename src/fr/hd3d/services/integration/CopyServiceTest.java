/**
 * 
 */
package fr.hd3d.services.integration;

import java.io.IOException;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EFrameRate;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.model.lightweight.ILPlayListRevision;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LPlayListRevision;
import fr.hd3d.model.lightweight.impl.LProject;


/**
 * @author Thomas ESKENAZI
 */
public class CopyServiceTest extends AbstractServiceIntegrationTest<ILPlayListRevision>
{
    protected void initDB() throws IOException
    {}

    LProject project;
    LProject updatedProject;
    ILPlayListRevision playListRevision1;

    public ILProject getProjectInstance1()
    {
        project = new LProject();
        project.setColor("#123456");
        project.setName("ProjTest");
        project.setStatus(EProjectStatus.CLOSED.toString());
        project.setHook("ProjTesthook");
        return project;
    }

    public ILProject getProjectInstance2()
    {
        updatedProject = new LProject();
        updatedProject.setColor("#654321");
        updatedProject.setName("ProjTest2");
        updatedProject.setStatus(EProjectStatus.OPEN.toString());
        updatedProject.setHook("ProjTest2hook");
        return updatedProject;
    }

    public ILPlayListRevision getPlayListRevisionInstance1(Long projectId)
    {
        playListRevision1 = new LPlayListRevision();
        playListRevision1.setProject(projectId);
        playListRevision1.setFrameRate(EFrameRate.FILM_24.toString());
        playListRevision1.setName("My PlayListRevision");
        playListRevision1.setRevision(1);
        playListRevision1.setComment("first comment");
        return playListRevision1;
    }

    public WebResponse createProject(ILProject project) throws IOException
    {
        return doPostAtURL(SERVICE + PROJECT, project);
    }

    public WebResponse createPlayListRevision(ILPlayListRevision playListRevision) throws IOException
    {
        return doPostAtURL(SERVICE + ServicesURI.PLAYLISTREVISIONS, playListRevision);
    }

    public WebResponse copyPlayListRevision(ILPlayListRevision playListRevision) throws IOException
    {
        return doPostAtURL(SERVICE + ServicesURI.PLAYLISTREVISIONS + '/' + playListRevision.getId() + "/copy",
                playListRevision);
    }

    public WebResponse deleteProject(ILProject project) throws IOException
    {
        return doDELETEAtURL(getProjectUrl(project));
    }

    @Test
    public void testPlayListRevisionPOSTandCOPY() throws IOException
    {
        /* create the project */
        ILProject project = getProjectInstance2();
        WebResponse response = createProject(project);
        assertEquals(CREATION_OK, response.getText());
        ILProject createdProjectUrl = doGetObject(response.getHeaderField("CONTENT-LOCATION"));
        /* create the playlistrevision */
        ILPlayListRevision playListRevision = getPlayListRevisionInstance1(createdProjectUrl.getId());
        response = createPlayListRevision(playListRevision);
        assertEquals(CREATION_OK, response.getText());
        /* copy */
        String createdPlayListRevisionUrl = response.getHeaderField("CONTENT-LOCATION");
        ILPlayListRevision createdPlayListRevision = doGetObject(createdPlayListRevisionUrl);
        response = copyPlayListRevision(createdPlayListRevision);
        ILPlayListRevision copyPlayListRevision = doGetObject(response.getHeaderField("CONTENT-LOCATION"));
        assertEquals(CREATION_OK, response.getText());
        assertNotSame(createdPlayListRevision.getId(), copyPlayListRevision.getId());
        assertEquals((int) (createdPlayListRevision.getRevision() + 1), (int) copyPlayListRevision.getRevision());
        assertNull(copyPlayListRevision.getComment());
        assertEquals(createdPlayListRevision.getFrameRate(), copyPlayListRevision.getFrameRate());
        assertEquals(createdPlayListRevision.getProject(), copyPlayListRevision.getProject());
    }
}
