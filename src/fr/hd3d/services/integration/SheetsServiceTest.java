package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CLASSDYNMETADATATYPES;
import static fr.hd3d.common.client.ServicesURI.ITEMGROUPS;
import static fr.hd3d.common.client.ServicesURI.ITEMS;
import static fr.hd3d.common.client.ServicesURI.SHEETS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSheet;
import fr.hd3d.model.lightweight.impl.LClassDynMetaDataType;
import fr.hd3d.model.lightweight.impl.LDynMetaDataValue;
import fr.hd3d.model.lightweight.impl.LItem;
import fr.hd3d.model.lightweight.impl.LItemGroup;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.lightweight.impl.LSheet;
import fr.hd3d.services.resources.ItemGroupResource;
import fr.hd3d.utils.CollectUtils;
import fr.hd3d.utils.Const;


/**
 * @author Try LAM
 */
public class SheetsServiceTest extends AbstractServiceIntegrationTest<ILSheet>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=DynMetaData");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=Sheets");
    }

    /**
     * Test Sheets retrieved through web services (GET /hd3dServices/project/{projectId}/sheets).
     * 
     * @throws IOException
     */
    @Test
    public void testSheetsGET() throws IOException
    {

        String url = getProjectUrl(getProjectA()) + '/' + SHEETS;
        Collection<LSheet> sheets = doGetObject(url);
        assertEquals(3, sheets.size());
    }

    /**
     * Test Sheets CREATION through web services (POST /hd3dServices/project/{projectId}/sheets).
     * 
     * @throws IOException
     */
    @Test
    public void testSheetsPOST() throws IOException
    {
        String sheetsUrl = getProjectUrl(getProjectA()) + '/' + SHEETS;

        ILProject projectA = getProjectA();

        /* create a valid sheet */
        ILSheet sheet = LSheet.getNewInstance("My new sheet", "one description blabla...", projectA.getId(), "Project",
                null);
        WebResponse response = doPostAtURL(sheetsUrl, sheet);
        assertEquals(CREATION_OK, response.getText());
        ILSheet updatedSheet = null;
        List<ILSheet> sheets = doGetObject(sheetsUrl);
        for (ILSheet s : sheets)
        {
            if (s != null && s.getName().equals("My new sheet"))
            {
                updatedSheet = s;
                break;
            }
        }

        /* create itemGroups */
        LItemGroup itemGroup_1 = LItemGroup.getNewInstance("itemGroup Test 1", null, updatedSheet.getId(),
                "ItemGrouptest1hook");
        LItemGroup itemGroup_2 = LItemGroup.getNewInstance("itemGroup Test 2", null, updatedSheet.getId(),
                "ItemGrouptest2hook");
        LItemGroup itemGroup_3 = LItemGroup.getNewInstance("itemGroup Test 3", null, updatedSheet.getId(),
                "ItemGrouptest3hook");
        List<LItemGroup> groups = new ArrayList<LItemGroup>();
        groups.add(itemGroup_1);// notice the order
        groups.add(itemGroup_3);
        groups.add(itemGroup_2);
        String itemGroupsUrl = SERVICE + SHEETS + "/" + updatedSheet.getId() + '/' + ITEMGROUPS;
        response = doPostAtURL(itemGroupsUrl + "?mode=bulk", groups);
        assertEquals(CREATION_OK, response.getText());

        /* read them back; the order of the itemgroups must be the same */
        List<LItemGroup> createdGroups = doGetObject(itemGroupsUrl);
        assertEquals(3, createdGroups.size());
        assertTrue("itemGroup Test 1".equals(createdGroups.get(0).getName()));
        assertTrue("itemGroup Test 3".equals(createdGroups.get(1).getName()));
        assertTrue("itemGroup Test 2".equals(createdGroups.get(2).getName()));

        /* create dynMetadataValues for test */
        ILDynMetaDataType stringType = ClassDynMetaDataTypesServiceTest.getDynMetaDataTypeByName("string");

        /* create a ClassDynMetaDataType bound to the previous DynMetaDataType */
        ILClassDynMetaDataType classDyn = LClassDynMetaDataType.getNewInstance("classDynTest", stringType.getId(), "*",
                "Project", null);
        String url = SERVICE + '/' + CLASSDYNMETADATATYPES;
        response = doPostAtURL(url, classDyn);
        assertEquals(CREATION_OK, response.getText());

        classDyn = ClassDynMetaDataTypesServiceTest.getClassDynMetaDataTypeByName("classDynTest");

        /* create items */
        LItem item_1 = LItem.getNewInstance("item_1", Sheet.ITEMTYPE_ATTRIBUTE, "name", "control content",
                createdGroups.get(0).getId(), "java.lang.String",
                "fr.hd3d.services.resources.transformer.TestTransformer", null);
        LItem item_2 = LItem.getNewInstance("item_2", Sheet.ITEMTYPE_ATTRIBUTE, "name", "control content",
                createdGroups.get(0).getId(), "java.lang.String",
                "fr.hd3d.services.resources.transformer.TestTransformer", null);
        LItem item_3 = LItem.getNewInstance("item_3", Sheet.ITEMTYPE_ATTRIBUTE, "name", "control content",
                createdGroups.get(0).getId(), "java.lang.String",
                "fr.hd3d.services.resources.transformer.TestTransformer", null);
        LItem item_4 = LItem.getNewInstance("item_4", Sheet.ITEMTYPE_METADATA, String.valueOf(classDyn.getId()),
                "control content", createdGroups.get(0).getId(), "java.lang.String", null, null);
        List<LItem> items = new ArrayList<LItem>(4);
        items.add(item_4);
        items.add(item_3);
        items.add(item_2);
        items.add(item_1);
        String itemsUrl = itemGroupsUrl + "/" + createdGroups.get(0).getId() + '/' + ITEMS;
        response = doPostAtURL(itemsUrl + "?mode=bulk", items);
        assertEquals(CREATION_OK, response.getText());
        List<LItem> createdItems = doGetObject(itemsUrl);
        assertEquals(4, createdItems.size());
        assertTrue("item_4".equals(createdItems.get(0).getName()));
        assertTrue("item_3".equals(createdItems.get(1).getName()));
        assertTrue("item_2".equals(createdItems.get(2).getName()));
        assertTrue("item_1".equals(createdItems.get(3).getName()));

        /* change the order of itemgroups in sheet */
        List<Long> newOrderedItemGroupIds = new ArrayList<Long>(3);
        newOrderedItemGroupIds.add(createdGroups.get(2).getId());// "itemGroup Test 2"
        newOrderedItemGroupIds.add(createdGroups.get(0).getId());// "itemGroup Test 1"
        newOrderedItemGroupIds.add(createdGroups.get(1).getId());// "itemGroup Test 3"
        updatedSheet.setItemGroups(newOrderedItemGroupIds);
        String sheetUrl = SERVICE + StringUtils.stripStart(SHEETS, "/") + '/' + updatedSheet.getId();
        doPUTAtURL(sheetUrl, updatedSheet);

        /* read them back, the new order is expected */
        ILSheet updatedAgainSheet = doGetObject(sheetUrl);
        if (updatedAgainSheet == null)
            updatedAgainSheet = doGetObject(sheetUrl);
        assertEquals(newOrderedItemGroupIds, updatedAgainSheet.getItemGroups());
        assertNotSame(CollectUtils.getLightIds(groups), updatedAgainSheet.getItemGroups());

        /****************************************/
        /* read the sheet and check its content */
        /****************************************/
        /* read projects first to create the dynMetaDataValues */
        Collection<LProject> projects = doGetObject(SERVICE + PROJECT + "?dyn=true");
        /* update projects dynMetaDataValues bound to classDyn to change their values */
        List<ILDynMetaDataValue> updatedDynValues = new ArrayList<ILDynMetaDataValue>();
        final String METADATAVALUE = "My MetaDataaaaa!!!!";
        for (LProject proj : projects)
        {
            for (ILDynMetaDataValue dynValue : proj.getDynMetaDataValues())
            {
                if (dynValue.getClassDynMetaDataType().equals(classDyn.getId()))
                {
                    dynValue.setValue(METADATAVALUE);
                    updatedDynValues.add(dynValue);
                }
            }
        }
        response = doPUTAtURL(SERVICE + ServicesURI.DYNMETADATAVALUES, updatedDynValues);

        /* read the sheet */
        Collection<Map<String, Object>> sheetData = doGetObject(sheetUrl + "/data");
        for (Map<String, Object> map : sheetData)
        {
            // String projectId = (String) map.get(Const.ID);
            List<Map<String, Object>> itemGroupsMap = (List<Map<String, Object>>) map.get(Const.GROUPS);
            if (itemGroupsMap != null)
            {
                for (Map<String, Object> itemGroupMap : itemGroupsMap)
                {
                    Long itemGroupId = (Long) itemGroupMap.get(Const.ID);
                    for (Map<String, Object> itemsMap : (List<Map<String, Object>>) itemGroupMap.get(Const.ITEMS))
                    {
                        Long itemId = (Long) itemsMap.get(ItemGroupResource.ID);
                        ILItem item = doGetObject(sheetUrl + '/' + ServicesURI.ITEMGROUPS + '/' + itemGroupId
                                + "/items/" + itemId);

                        if (item != null && item.getQuery() != null
                                && item.getQuery().equals(String.valueOf(classDyn.getId())))
                        {
                            /* check metaDataValues */
                            Object itemValue = itemsMap.get(ItemGroupResource.VALUE);
                            assertEquals(METADATAVALUE, itemValue);
                        }
                        // String itemName = (String) itemsMap.get(ItemGroupResource.NAME);
                        // Object itemValue = itemsMap.get(ItemGroupResource.VALUE);
                    }
                }
            }
        }

        /* remove the created dynMetaDataValues, create some new bound to classDyn and check if ok */
        for (ILDynMetaDataValue v : updatedDynValues)
        {
            response = doDELETEAtURL(SERVICE + ServicesURI.DYNMETADATAVALUES + '/' + v.getId());
        }
        List<ILDynMetaDataValue> newDynValues = new ArrayList<ILDynMetaDataValue>();
        final String NEW_METADATAVALUE = "Coucou!";
        ILDynMetaDataValue d1 = LDynMetaDataValue.getNewInstance(classDyn.getId(), projectA.getId(), "Project",
                NEW_METADATAVALUE, null);
        newDynValues.add(d1);
        response = doPostAtURL(SERVICE + ServicesURI.DYNMETADATAVALUES + "?mode=bulk", newDynValues);

        /* read back the sheet */
        Collection<Map<String, Object>> updatedSheetData = doGetObject(sheetUrl + "/data");
        for (Map<String, Object> map : updatedSheetData)
        {
            Long projectId = (Long) map.get(Const.ID);
            List<Map<String, Object>> itemGroupsMap = (List<Map<String, Object>>) map.get(Const.GROUPS);
            if (itemGroupsMap != null)
            {
                for (Map<String, Object> itemGroupMap : itemGroupsMap)
                {
                    Long itemGroupId = (Long) itemGroupMap.get(Const.ID);
                    for (Map<String, Object> itemsMap : (List<Map<String, Object>>) itemGroupMap.get(Const.ITEMS))
                    {
                        Long itemId = (Long) itemsMap.get(ItemGroupResource.ID);
                        ILItem item = doGetObject(sheetUrl + '/' + ServicesURI.ITEMGROUPS + '/' + itemGroupId
                                + "/items/" + itemId);

                        if (projectId.equals(projectA.getId()) && item != null && item.getQuery() != null
                                && item.getQuery().equals(String.valueOf(classDyn.getId())))
                        {
                            /* check metaDataValues */
                            Object itemValue = itemsMap.get(ItemGroupResource.VALUE);
                            assertEquals(NEW_METADATAVALUE, itemValue);
                        }
                        // String itemName = (String) itemsMap.get(ItemGroupResource.NAME);
                        // Object itemValue = itemsMap.get(ItemGroupResource.VALUE);
                    }
                }
            }
        }

        /* create an invalid sheet: invalid bound class name */
        sheet = LSheet.getNewInstance("My new sheet", "one description blabla...", getProjectA().getId(), "TOTO", null);
        response = doPostAtURL(sheetsUrl, sheet);
        assertEquals("", response.getText());
    }

    /**
     * Test Sheets CREATION through web services (POST /hd3dServices/sheets).
     * 
     * @throws IOException
     */
    @Test
    public void testSheetPostWithoutProject() throws IOException
    {
        String url = SERVICE + '/' + SHEETS;
        // valid sheet
        ILSheet sheet = LSheet.getNewInstance("My new sheet", "one description blabla...", null, "Constituent", null);
        WebResponse response = doPostAtURL(url, sheet);
        assertEquals(CREATION_OK, response.getText());

        // invalid sheet: invalid bound class name
        sheet = LSheet.getNewInstance("My new sheet", "one description blabla...", null, "TOTO", null);
        response = doPostAtURL(url, sheet);
        assertEquals("", response.getText());
    }
}
