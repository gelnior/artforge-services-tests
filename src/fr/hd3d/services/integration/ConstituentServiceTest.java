package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.TAGS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LTag;


/**
 * @author Try LAM
 */
public class ConstituentServiceTest extends AbstractServiceIntegrationTest<ILConstituent>
{

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
    }

    /**
     * Test Constituent retrieved through web services (GET
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents/{constituentId} ).
     * 
     * @throws IOException
     */
    @Test
    public void testConstituentGET() throws IOException
    {
        // get all the constituents of Personnage category of project A
        String url = getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        assertEquals(2, constituents.size());
        // check them
        for (ILConstituent c : constituents)
        {
            if (c.getLabel().equals("const 1 label"))
            {
                url += "/" + c.getId();
                ILConstituent c1 = doGetObject(url);
                assertEquals(c.getLabel(), c1.getLabel());
            }
        }
    }

    /**
     * Test Constituent MODIFICATION through web services (PUT
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents/{constituentId} ).
     * 
     * @throws IOException
     */
    @Test
    public void testConstituentPUT() throws IOException
    {
        // init
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=Tags");

        // get all the constituents of Personnage category of project A
        String url = getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url + "?tags=true");
        assertEquals(2, constituents.size());
        // updated one of them
        for (ILConstituent c : constituents)
        {
            if (c.getLabel().equals("const 1 label"))
            {
                // update label
                c.setLabel("const 1 label UPDATED");
                // update description
                c.setDescription("UPDATED");
                // update tags, attach some other tags to the constituent
                List<Long> ltags = new ArrayList<Long>();
                Collection<LTag> tags = doGetObject(SERVICE + '/' + TAGS);
                for (LTag t : tags)
                {
                    String name = t.getName();
                    if ("Generic Project tag".equals(name) || "Project A tag".equals(name))
                    {
                        ltags.add(t.getId());
                    }
                }
                c.setTags(ltags);
                url += "/" + c.getId();
                doPUTAtURL(url + "?tags=true", c);
                // read it back
                ILConstituent updated = doGetObject(url + "?tags=true");
                assertEquals("const 1 label UPDATED", updated.getLabel());
                assertEquals("UPDATED", updated.getDescription());
                int count = 0;
                for (Long tagId : updated.getTags())
                {
                    LTag tag = doGetObject(SERVICE + '/' + TAGS + "/" + tagId);
                    if (tag.getName().equals("Generic Project tag") || "Project A tag".equals(tag.getName()))
                    {
                        count++;
                    }
                }
                assertEquals(2, count);
                assertEquals(2, updated.getTags().size());// the newly attached tags, the old ones have been detached.
            }
        }
    }

    /**
     * Test Category CREATION through web services (POST /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    // @Test
    // public void testCategoryPOST() throws IOException {
    // // get the project A
    // ILProject projectA = getProjectA();
    // String url = getProjectUrl(projectA) + CATEGORIES;
    // // retrieve the main categories (with "constituent" as parent) for the project A
    // Collection<LCategory> categories = doGetObject(url);
    // assertEquals(2, categories.size());
    // for (LCategory category : categories) {
    // // retrieve the Personnage main category
    // if ("Personnage".equals(category.getName())) {
    // // create a new sub-category (with "Personnage" as parent)
    // ILCategory subCategory = LCategory.getNewInstance(null, null, "Sous Categorie", null, projectA.getId());
    // String url1 = getProjectUrl(projectA) + CATEGORIES + "/" + category.getId();
    // WebResponse response = doPostAtURL(url1, subCategory);
    // assertEquals("Creation ok", response.getText());
    // // retrieve the project A's newly created category and check
    // categories = doGetObject(url);
    // for (LCategory newCategorie : categories) {
    // if ("Personnage".equals(newCategorie.getName())) {
    // String url2 = getProjectUrl(projectA) + CATEGORIES + "/" + newCategorie.getId();
    // Collection<LCategory> newCategories = doGetObject(url2);
    // // Personnage category must have one more new sub category
    // assertEquals(3, newCategories.size());
    // }
    // }
    // }
    // }
    // }
    //
    // /**
    // * Test Category DELETION through web services (DELETE /hd3dServices/project/{projectId}/categories/{categoryId}).
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoryDELETE() throws IOException {
    //
    // // get the "Decor" category of the project A
    // ILCategory category = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
    // // delete it
    // String url = getProjectUrl(getProjectA()) + "/" + CATEGORIES + "/" + category.getId();
    // doDELETEAtURL(url);
    // // read it back again
    // ILCategory category1 = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
    //
    // assertEquals(category, category1);
    // }
    //
    // /**
    // * Test like condition on Category name
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoryLike() throws IOException {
    //
    // String constraint = getLikeConstraint("name", "Perso%");
    // Collection<LPerson> categories = doGetObject(getProjectUrl(getProjectA()) + CATEGORIES
    // + constraint);
    // assertEquals(1, categories.size());
    // }
    // convenience method
    public static String getProjectAPersonnageCategoryUrl() throws IOException
    {
        String ret = null;

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {

            if ("Personnage".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
                // get the subcategories
                url = url + "/" + category.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }
}
