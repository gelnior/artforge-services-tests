/**
 * 
 */
package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.lightweight.impl.LTaskType;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;


/**
 * @author thomas-eskenazi
 * 
 */
public class TaskTypeServiceTest extends AbstractServiceIntegrationTest<ILTaskType>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    public WebResponse createTaskType(ILTaskType taskType) throws IOException
    {
        return doPostAtURL(SERVICE + TASK_TYPE, taskType);
    }

    public WebResponse deleteTaskType(ILTaskType taskType) throws IOException
    {
        return doDELETEAtURL(getTaskTypeUrl(taskType));
    }

    public ILTaskType updateTaskType(ILTaskType taskType, ILTaskType update) throws IOException
    {
        taskType.setColor(update.getColor());
        taskType.setName(update.getName());
        taskType.setDescription(update.getDescription());
        taskType.setProjectID(update.getProjectID());
        WebResponse resp = doPUTAtURL(getTaskTypeUrl(taskType), taskType);
        try
        {
            return (ILTaskType) Hd3dJsonSerializer.unserialize(resp.getText());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    protected String getTaskTypeUrl(ILTaskType taskType)
    {
        assertNotNull(taskType);
        return SERVICE + TASK_TYPE + taskType.getId().toString();
    }

    @Test
    public void testTaskTypeGET()
    {
        Collection<ILTaskType> taskTypes = doGetObject(SERVICE + TASK_TYPE);
        assertEquals(17, taskTypes.size());
        for (ILTaskType taskType : taskTypes)
        {
            if ("Animation".equals(taskType.getName()))
            {
                assertEquals("#B27D00", taskType.getColor());
                assertEquals("The Animation Task Type", taskType.getDescription());
                assertNull(taskType.getProjectID());
            }
            else if ("Layout".equals(taskType.getName()))
            {
                assertEquals("#FECC80", taskType.getColor());
                assertEquals("The Layout Task Type", taskType.getDescription());
                assertNull(taskType.getProjectID());
            }
        }
    }

    @Test
    public void testTaskTypePOST() throws IOException
    {
        // get the project A
        ILTaskType taskType = new LTaskType();
        ILProject projectB = getProjectB();
        taskType.setColor("testColor");
        taskType.setDescription("testDesc");
        taskType.setName("testName");
        taskType.setProjectID(projectB.getId());
        WebResponse response = doPostAtURL(SERVICE + TASK_TYPE, taskType);
        assertEquals(CREATION_OK, response.getText());
        Collection<ILTaskType> taskTypes = doGetObject(SERVICE + TASK_TYPE + getLikeConstraint("name", "testName"));
        assertEquals(taskTypes.size(), 1);
        ILTaskType createdTaskType = taskTypes.iterator().next();
        assertEquals(createdTaskType.getColor(), taskType.getColor());
        assertEquals(createdTaskType.getDescription(), taskType.getDescription());
        assertEquals(createdTaskType.getName(), taskType.getName());
        assertEquals(createdTaskType.getProjectID(), taskType.getProjectID());
    }

}
