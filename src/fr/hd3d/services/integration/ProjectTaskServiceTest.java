package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.model.lightweight.ILApprovalNote;
import fr.hd3d.model.lightweight.ILApprovalNoteType;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.lightweight.impl.LTask;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class ProjectTaskServiceTest extends AbstractServiceIntegrationTest<ILTask>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    // private static final String ANOTHER_SUPER_TASK = "Another super task";
    private static final String ONE_SUPER_TASK = "One super task";
    private static final String TEST_TASK = "Test_Task_00";

    @Test
    public void testGetTasks() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks");
        assertNotNull(tasks);
        assertEquals(4, tasks.size());
        for (ILTask task : tasks)
        {
            assertEquals(projectA.getName(), task.getProjectName());
            assertEquals(projectA.getId(), task.getProjectID());
            assertEquals(projectA.getColor(), task.getProjectColor());
            assertEquals((Byte) (byte) 0, task.getCompletion());
            assertNull(task.getCompletionDate());
            assertEquals(ETaskStatus.OK.toString(), task.getStatus());
        }
    }

    @Test
    public void testGetTask() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        // TODO log
        String constraint = "?orderBy=[\"id\"]}]";
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks" + constraint);
        assertEquals(4, tasks.size());
        final ILTask task = doGetObject(projectUrl + "/tasks/" + ((ILTask) (tasks.iterator().next())).getId());
        assertEquals(projectA.getName(), task.getProjectName());
        assertEquals(projectA.getId(), task.getProjectID());
        assertEquals(projectA.getColor(), task.getProjectColor());
        assertEquals("Test_Task_00", task.getName());
        assertEquals((Byte) (byte) 0, task.getCompletion());
        assertNull(task.getCompletionDate());
        assertEquals(ETaskStatus.OK.toString(), task.getStatus());
        assertNotNull(task);
    }

    @Test
    public void testPostTask() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        ILPerson gch = this.getPerson("gch");
        Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks");
        assertEquals(4, tasks.size());
        final String TestTask = "The test task";

        final ILTaskType taskType = ApprovalNotesServiceTest.createTaskType();
        assertNotNull(taskType);

        /*
         * the approvalnotetype must be created before the task creation, since this latter triggers approval note
         * creation
         */
        final ILApprovalNoteType approvalNoteType = ApprovalNotesServiceTest.createApprovalNoteType(taskType.getId(),
                taskType.getProjectID());
        assertNotNull(approvalNoteType);

        final ILTask newTask = LTask.getNewInstance(projectA.getId(), TestTask, ETaskStatus.WORK_IN_PROGRESS, gch
                .getId(), gch.getId(), projectA.getId(), "Project", taskType.getId());
        final WebResponse response = doPostAtURL(projectUrl + "/tasks", newTask);
        assertEquals(CREATION_OK, response.getText());
        final String createdTaskUrl = response.getHeaderField("CONTENT-LOCATION");
        ILTask createdTask = doGetObject(createdTaskUrl);
        assertNotNull(createdTask);

        assertEquals(gch.getName(), createdTask.getCreatorName());
        assertEquals(gch.getId(), createdTask.getCreatorID());
        assertEquals(gch.getName(), createdTask.getWorkerName());
        assertEquals(gch.getId(), createdTask.getWorkerID());
        assertEquals(projectA.getName(), createdTask.getProjectName());
        assertEquals(projectA.getId(), createdTask.getProjectID());
        assertEquals(projectA.getColor(), createdTask.getProjectColor());
        assertEquals(ETaskStatus.WORK_IN_PROGRESS.toString(), createdTask.getStatus());
        assertEquals(taskType.getId(), createdTask.getTaskType().getId());

        /* after task creation, there must be one related approvalnote */
        checkApprovalNotes(createdTask);

        /* update the task status, there must be one approvalnote created */
        createdTask.setStatus(ETaskStatus.STAND_BY.toString());
        createdTask.setCommentForApprovalNote("Comment for approval note");
        List<ILTask> updatedTasks = new ArrayList<ILTask>();
        updatedTasks.add(createdTask);
        doPUTAtURL(SERVICE + "/tasks", updatedTasks);
        ILTask updatedTask = doGetObject(createdTaskUrl);
        assertNotNull(updatedTask);
        assertEquals(createdTask.getStatus(), updatedTask.getStatus());
        checkApprovalNotes(updatedTask);
    }

    private void checkApprovalNotes(ILTask task)
    {
        final Collection<ILApprovalNote> approvalNotes = doGetObject(SERVICE + ServicesURI.APPROVALNOTES);
        List<ILApprovalNote> createdApprovalNotes = new ArrayList<ILApprovalNote>();
        for (ILApprovalNote approvalNote : approvalNotes)
        {
            boolean sameBoundEntityName = approvalNote.getBoundEntityName() != null
                    && task.getBoundEntityName() != null
                    && approvalNote.getBoundEntityName().equals(task.getBoundEntityName());

            boolean sameBoundEntity = approvalNote.getBoundEntity() != null && task.getWorkObjectId() != null
                    && approvalNote.getBoundEntity().equals(task.getWorkObjectId());

            boolean sameStatus = approvalNote.getStatus() != null && task.getStatus() != null
                    && approvalNote.getStatus().equals(task.getStatus());

            boolean sameType = approvalNote.getTaskTypeId() != null && task.getTaskTypeID() != null
                    && approvalNote.getTaskTypeId().equals(task.getTaskTypeID());

            if (sameBoundEntityName && sameBoundEntity && sameStatus && sameType)
            {
                createdApprovalNotes.add(approvalNote);
            }
        }
        assertEquals(1, createdApprovalNotes.size());
    }

    @Test
    public void testUpdateTask() throws IOException
    {
        // get all the projectA tasks
        final ILProject projectA = getProjectA();
        final String tasksUrl = getProjectUrl(projectA) + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(4, tasks.size());
        // get the first task
        final ILTask task = tasks.iterator().next();
        final java.util.Date date = new java.util.Date();
        final String newTaskName = "New name";
        task.setCompletion((Byte) (byte) 100);
        task.setCompletionDate(date);
        task.setName(newTaskName);
        // submit the changes
        final String taskUrl = tasksUrl + '/' + task.getId().toString();
        doPUTAtURL(taskUrl, task);
        // read it back
        final ILTask newTask = doGetObject(taskUrl);
        assertEquals(task.getCompletion(), newTask.getCompletion());
        assertEqualsDate(task.getCompletionDate(), newTask.getCompletionDate());
        assertEquals(task.getName(), newTask.getName());
    }

    @Test
    public void testDeleteDeletableTask() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String tasksUrl = getProjectUrl(projectA) + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(4, tasks.size());
        for (ILTask task : tasks)
            if (TEST_TASK.equals(task.getName()))
            {
                doDELETEAtURL(tasksUrl + '/' + task.getId().toString());
            }
        tasks = doGetObject(tasksUrl);
        assertEquals(3, tasks.size());
    }

    @Test
    /*
     * ONE_SUPER_TASK has an activity it upon and so cannot be removed without removing the underlying activity
     */
    public void testDeleteUndeletableTask() throws IOException
    {
        final ILProject projectA = getProjectA();
        final String tasksUrl = getProjectUrl(projectA) + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(4, tasks.size());
        for (ILTask task : tasks)
            if (ONE_SUPER_TASK.equals(task.getName()))
            {
                doDELETEAtURL(tasksUrl + '/' + task.getId().toString());
            }
        tasks = doGetObject(tasksUrl);
        assertEquals(4, tasks.size());
    }

    @Test
    public void testTaskLike() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        String constraint = getLikeConstraint("name", "One%");
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks/" + constraint);
        assertEquals(1, tasks.size());
        ILTask task = tasks.iterator().next();
        assertEquals(projectA.getName(), task.getProjectName());
        assertEquals(projectA.getId(), task.getProjectID());
        assertEquals(projectA.getColor(), task.getProjectColor());
        assertEquals(ONE_SUPER_TASK, task.getName());
        assertEquals((Byte) (byte) 0, task.getCompletion());
        assertNull(task.getCompletionDate());
        assertEquals(ETaskStatus.OK.toString(), task.getStatus());
    }
    /**
     * TODO Write a test to check the PUT of an invalid task bad workerID or projectID regarding the url.
     */
}
