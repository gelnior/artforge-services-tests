package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LConstituent;


/**
 * Tests web service Constituents in Project categories: list (GET) and singleton (CRUD)
 * 
 * @author Try LAM
 */
public class ConstituentsServiceTest extends AbstractServiceIntegrationTest<ILConstituent>
{

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ScriptExec");
    }

    /**
     * Test constituents retrieved through web services (GET
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents ).
     * 
     * @throws IOException
     */
    @Test
    public void testConstituentsGET() throws IOException
    {
        String url = getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        assertEquals(2, constituents.size());
    }

    /**
     * Test constituent CREATION through web services (POST
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents).
     * 
     * @throws IOException
     */
    @Test
    public void testConstituentsPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // get the category
        ILCategory category = getProjectAPersonnageCategory();
        // create a new constituent
        ILConstituent constituent = LConstituent.getNewInstance(category.getId(), category.getName(), 0,
                "New constituent Description 日本", 5, "Newconstituenthook", "New constituent Label日本", 5, true, true,
                true, false, false, false, null);
        String url = getProjectUrl(projectA) + '/' + CATEGORIES + "/" + category.getId() + "/constituents";
        WebResponse response = doPostAtURL(url, constituent);
        assertEquals(CREATION_OK, response.getText());
        // get the new constituent
        ILConstituent newConstituent = getProjectAPersonnageConstituent("New constituent Label日本");
        // compare
        assertEquals(constituent.getCategory(), newConstituent.getCategory());
        assertEquals(constituent.getDescription(), newConstituent.getDescription());
        assertEquals(constituent.getCompletion(), newConstituent.getCompletion());
        assertEquals(constituent.getDifficulty(), newConstituent.getDifficulty());
        assertEquals(constituent.getHook(), newConstituent.getHook());
        assertEquals(constituent.getLabel(), newConstituent.getLabel());
        assertEquals(constituent.getTrust(), newConstituent.getTrust());

        /* hook and label uniqueness test */
        response = doPostAtURL(url, constituent);
        assertFalse(CREATION_OK.equals(response.getText()));
        /* same than constituent but with a different name */
        ILConstituent constituent_1 = LConstituent.getNewInstance(category.getId(), category.getName(), 0,
                "New constituent Description 日本", 5, "DifferentNewconstituenthook", "New constituent 1 Label日本", 5,
                true, true, true, false, false, false, null);
        response = doPostAtURL(url, constituent_1);
        assertTrue(CREATION_OK.equals(response.getText()));
        /* same constituent than constituent but with a different label */
        ILConstituent constituent_2 = LConstituent.getNewInstance(category.getId(), category.getName(), 0,
                "New constituent Description 日本", 5, "Newconstituenthook", "New constituent 1 Label日本", 5, true, true,
                true, false, false, false, null);
        response = doPostAtURL(url, constituent_2);
        assertFalse(CREATION_OK.equals(response.getText()));
    }

    /**
     * Test constituent MULITPLE CREATION through web services (POST
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents).
     * 
     * @throws IOException
     */
    @Test
    public void testBulkConstituentsPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // get the category
        ILCategory category = getProjectAPersonnageCategory();
        // create new constituents
        List<ILConstituent> constituents = new ArrayList<ILConstituent>();
        final int NUMBERCONSTITUENTS = 3;
        for (int i = 0; i < NUMBERCONSTITUENTS; i++)
        {
            constituents.add(LConstituent.getNewInstance(category.getId(), category.getName(), 0,
                    "New constituent Description 日本", 5, "Newconstituenthook", "New constituent Label日本" + i, 5, true,
                    true, true, false, false, false, null));
        }
        String url = getProjectUrl(projectA) + '/' + CATEGORIES + "/" + category.getId() + "/constituents";
        WebResponse response = doPostAtURL(url + "?mode=bulk", constituents);
        assertEquals(CREATION_OK, response.getText());
        // get the new constituents
        List<ILConstituent> newConstituents = doGetObject(url);
        int count = 0;
        for (ILConstituent c : newConstituents)
        {
            if (c.getLabel().startsWith("New constituent Label日本"))
            {
                count++;
            }
        }
        assertEquals(NUMBERCONSTITUENTS, count);
    }

    /**
     * Test constituent UPDATE through web services (PUT
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents/{constituentId}).
     * 
     * @throws IOException
     */
    @Test
    public void testConstituentsPUT() throws IOException
    {
        // TODO due to httpUnit bug (?) put method, foreign characters seem to be properly conveyed
        // final String updated = "const 1 label日本";
        final String updated = "const 1 label updated";
        final Integer updatedCompletion = 1000;
        ILConstituent const1 = getProjectAPersonnageConstituent("const 1 label");
        const1.setLabel(updated);
        const1.setCompletion(updatedCompletion);
        String url = getProjectAPersonnageCategoryUrl() + "/constituents/" + const1.getId();
        doPUTAtURL(url, const1);
        ILConstituent const1Updated = doGetObject(url);
        // read back
        assertEquals(updated, const1Updated.getLabel());
        assertEquals(updatedCompletion, const1Updated.getCompletion());
    }

    /**
     * Test constituent BULK UPDATE through web services (PUT
     * /hd3dServices/project/{projectId}/categories/{categoryID}/constituents/).
     * 
     * @throws IOException
     */
    @Test
    public void testConstituentsPUTBulk() throws IOException
    {

        ILConstituent const1 = getProjectAPersonnageConstituent("const 1 label");
        const1.setLabel("const 1 label UPDATED");
        ILConstituent const2 = getProjectAPersonnageConstituent("const 2 label");
        const2.setLabel("const 2 label UPDATED");
        List<ILConstituent> constituents = new ArrayList<ILConstituent>();
        constituents.add(const1);
        constituents.add(const2);
        String url = getProjectAPersonnageCategoryUrl() + "/constituents/";
        doPUTAtURL(url, constituents);
        // read back
        ILConstituent const1Updated = doGetObject(url + const1.getId());
        assertEquals("const 1 label UPDATED", const1Updated.getLabel());
        ILConstituent const2Updated = doGetObject(url + const2.getId());
        assertEquals("const 2 label UPDATED", const2Updated.getLabel());
    }

    //
    // /**
    // * Test Categories DELETE through web services (DELETE /hd3dServices/project/{projectId}/categories).
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoriesDELETE() throws IOException {
    // // get the project A
    // ILProject projectA = getProjectA();
    // // create a new main category (with "constituent" as parent) and part of
    // // project A
    // WebResponse response = doDELETEAtURL(getProjectUrl(projectA) + CATEGORIES);
    // // MUST NOT BE ALLOWED
    // assertEquals("", response.getText());
    //
    // }
    //    
    // convenience method
    private static String getProjectAPersonnageCategoryUrl() throws IOException
    {
        String ret = null;

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {
            if ("Personnage".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
                // get the subcategories
                url = url + "/" + category.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }

    // convenience method
    private ILCategory getProjectAPersonnageCategory() throws IOException
    {
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {

            if ("Personnage".equals(category.getName()))
            {
                return category;
            }
        }
        return null;
    }

    // convenience method
    public static ILConstituent getProjectAPersonnageConstituent(String label) throws IOException
    {
        String url = getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        for (ILConstituent c : constituents)
        {
            if (c.getLabel().equals(label))
            {
                return c;// actually, it may have several ones, but just return the first here
            }
        }
        return null;
    }
}
