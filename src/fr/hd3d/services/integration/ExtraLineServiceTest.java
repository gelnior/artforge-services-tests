/**
 * 
 */
package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.EXTRA_LINES;
import static fr.hd3d.common.client.ServicesURI.PLANNINGS;
import static fr.hd3d.common.client.ServicesURI.TASK_GROUPS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILExtraLine;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskGroup;
import fr.hd3d.model.lightweight.impl.LExtraLine;


/**
 * @author thomas-eskenazi
 * 
 */
public class ExtraLineServiceTest extends AbstractServiceIntegrationTest<ILExtraLine>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    public WebResponse createTaskType(ILExtraLine extraline) throws IOException
    {
        return doPostAtURL(SERVICE + TASK_TYPE, extraline);
    }

    // public WebResponse deleteTaskType(ILExtraLine taskType) throws IOException
    // {
    // return doDELETEAtURL(getTaskTypeUrl(taskType));
    // }
    //
    // public ILTaskType updateTaskType(ILExtraLine taskType, ILExtraLine update) throws IOException
    // {
    // taskType.setColor(update.getColor());
    // taskType.setName(update.getName());
    // taskType.setDescription(update.getDescription());
    // taskType.setProjectID(update.getProjectID());
    // WebResponse resp = doPUTAtURL(getTaskTypeUrl(taskType), taskType);
    // try
    // {
    // return (ILTaskType) Hd3dJsonSerializer.unserialize(resp.getText());
    // }
    // catch (Exception e)
    // {
    // e.printStackTrace();
    // return null;
    // }
    // }

    // protected String getTaskTypeUrl(ILTaskType taskType)
    // {
    // assertNotNull(taskType);
    // return SERVICE + TASK_TYPE + taskType.getId().toString();
    // }
    //
    // @Test
    // public void testTaskTypeGET()
    // {
    // Collection<ILTaskType> taskTypes = doGetObject(SERVICE + TASK_TYPE);
    // assertEquals(15, taskTypes.size());
    // for (ILTaskType taskType : taskTypes)
    // {
    // if ("Animation".equals(taskType.getName()))
    // {
    // assertEquals("#B27D00", taskType.getColor());
    // assertEquals("The Animation Task Type", taskType.getDescription());
    // assertNull(taskType.getProjectID());
    // }
    // else if ("Layout".equals(taskType.getName()))
    // {
    // assertEquals("#FECC80", taskType.getColor());
    // assertEquals("The Layout Task Type", taskType.getDescription());
    // assertNull(taskType.getProjectID());
    // }
    // }
    // }
    //
    @Test
    public void testExtraLinePOST() throws Exception
    {
        try
        {
            ILExtraLine extraLine = new LExtraLine();
            ILTaskGroup taskGroup = getTaskGroup();
            extraLine.setTaskGroupID(taskGroup.getId());
            extraLine.setName("TOTO");
            extraLine.setTaskBaseIDs(testGetTasks());
            WebResponse response = doPostAtURL(SERVICE + taskGroup.getDefaultPath() + '/' + EXTRA_LINES, extraLine);
            assertEquals(CREATION_OK, response.getText());
            Collection<ILExtraLine> extraLines = doGetObject(SERVICE + taskGroup.getDefaultPath() + '/' + EXTRA_LINES);
            ILExtraLine extraLine2 = extraLines.iterator().next();
            assertEquals(extraLine.getName(), extraLine2.getName());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    public void testExtraLinePOSTwithHiddenPerson() throws Exception
    {
        try
        {
            final ILPerson ethPerson = getPerson("eth");
            ILExtraLine extraLine = new LExtraLine();
            ILTaskGroup taskGroup = getTaskGroup();
            extraLine.setTaskGroupID(taskGroup.getId());
            extraLine.setName("TOTO");
            extraLine.setTaskBaseIDs(testGetTasks());
            extraLine.setHiddenPersonID(ethPerson.getId());
            WebResponse response = doPostAtURL(SERVICE + taskGroup.getDefaultPath() + '/' + EXTRA_LINES, extraLine);
            assertEquals(CREATION_OK, response.getText());
            Collection<ILExtraLine> extraLines = doGetObject(SERVICE + taskGroup.getDefaultPath() + '/' + EXTRA_LINES);
            ILExtraLine extraLine2 = extraLines.iterator().next();
            assertEquals(extraLine.getName(), extraLine2.getName());
            assertEquals(extraLine.getHiddenPersonID(), extraLine2.getHiddenPersonID());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }

        // ILProject projectB = getProjectB();
        // extraLine.setColor("testColor");
        // extraLine.setDescription("testDesc");
        // extraLine.setName("testName");
        // extraLine.setProjectID(projectB.getId());
        // WebResponse response = doPostAtURL(SERVICE + TASK_TYPE, extraLine);
        // assertEquals(CREATION_OK, response.getText());
        // Collection<ILTaskType> taskTypes = doGetObject(SERVICE + TASK_TYPE + getLikeConstraint("name", "testName"));
        // assertEquals(taskTypes.size(), 1);
        // ILTaskType createdTaskType = taskTypes.iterator().next();
        // assertEquals(createdTaskType.getColor(), extraLine.getColor());
        // assertEquals(createdTaskType.getDescription(), extraLine.getDescription());
        // assertEquals(createdTaskType.getName(), extraLine.getName());
        // assertEquals(createdTaskType.getProjectID(), extraLine.getProjectID());
    }

    private ILTaskGroup getTaskGroup() throws Exception
    {
        final ILProject projectA = getProjectA();
        final String planningUrl = getProjectUrl(projectA) + '/' + PLANNINGS;
        final String contraint = "?orderBy=[\"name\"]";
        final Collection<ILPlanning> plannings = doGetObject(planningUrl + contraint);
        final ILPlanning planning = plannings.iterator().next();
        final String planningTaskGroupUrl = getPlanningUrlForTaskGroup(planning) + '/' + TASK_GROUPS;
        final Collection<ILTaskGroup> taskGroups = doGetObject(planningTaskGroupUrl);
        final ILTaskGroup taskGroup = doGetObject(planningTaskGroupUrl + "/"
                + ((ILTaskGroup) (taskGroups.iterator().next())).getId().toString());
        return taskGroup;
    }

    private List<Long> testGetTasks() throws Exception
    {
        List<Long> taskIds = new ArrayList<Long>();
        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/tasks");
        for (Iterator<ILTask> iterator = tasks.iterator(); iterator.hasNext();)
        {
            ILTask task = (ILTask) iterator.next();
            taskIds.add(task.getId());
        }
        return taskIds;
    }

}
