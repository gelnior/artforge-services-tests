package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;


/**
 * Tests web service Category in Project categories: list (GET) and singleton (CRUD)
 * 
 * @author Try LAM
 */
@SuppressWarnings("unchecked")
public class ProjectCategoriesServiceTest extends AbstractServiceIntegrationTest
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
    }

    /**
     * Test Categories retrieved through web services (GET /hd3dServices/project/{projectId}/categories/).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoriesGET() throws IOException
    {

        ILProject projectA = getProjectA();
        final String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {

            if ("Personnage".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
            }
            // TODO check other categories
        }
    }

    /**
     * Test Categories CREATION through web services (POST /hd3dServices/project/{projectId}/categories).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoriesPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // create a new main category (with "constituent" as parent) and part of
        // project A
        ILCategory category = LCategory.getNewInstance("nouvelle Catégorie中国门", null, projectA.getId());
        WebResponse response = doPostAtURL(getProjectUrl(projectA) + "/" + CATEGORIES, category);
        assertEquals(CREATION_OK, response.getText());

        // retrieve the project A's newly created category and check
        String resp = this.getProjectUrl(getProjectA());
        ILCategory createdCategory = this.getCategoryInProject("nouvelle Catégorie中国门", resp);

        assertEquals(category.getName(), createdCategory.getName());
        // assertEquals(category.getParent(), createdCategory.getParent());
        assertEquals(category.getProject(), createdCategory.getProject());
    }

    /**
     * MULTIPLE creation
     * 
     * @throws IOException
     */
    @Test
    public void testBulkCategoriesPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // create a new main category (with "constituent" as parent) and part of project A
        List<ILCategory> categories = new ArrayList<ILCategory>();
        int NUMBERCATEGORIES = 3;
        for (int i = 0; i < NUMBERCATEGORIES; i++)
        {
            categories.add(LCategory.getNewInstance("nouvelle Catégorie中国门 " + i, null, projectA.getId()));
        }
        String url = getProjectUrl(projectA) + "/" + CATEGORIES;
        WebResponse response = doPostAtURL(url + "?mode=bulk", categories);
        assertEquals(CREATION_OK, response.getText());

        // get the new constituents
        List<ILCategory> newCategories = doGetObject(url);
        int count = 0;
        for (ILCategory c : newCategories)
        {
            if (c.getName().startsWith("nouvelle Catégorie中国门"))
            {
                count++;
            }
        }
        assertEquals(NUMBERCATEGORIES, count);
    }

    /**
     * Test Categories UPDATE through web services (PUT /hd3dServices/project/{projectId}/categories).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoriesPUT() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        final String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        List<LCategory> updated = new ArrayList<LCategory>();
        for (LCategory category : categories)
        {
            // update the categories
            category.setName(category.getName() + " UPDATED");
            updated.add(category);
        }
        WebResponse response = doPUTAtURL(getProjectUrl(projectA) + '/' + CATEGORIES, updated);
        // check
        assertEquals(200, response.getResponseCode());
        categories.clear();
        categories = doGetObject(url);
        for (LCategory category : categories)
        {
            assertTrue(category.getName().endsWith(" UPDATED"));
        }
    }

    /**
     * Test Categories DELETE through web services (DELETE /hd3dServices/project/{projectId}/categories).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoriesDELETE() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // create a new main category (with "constituent" as parent) and part of
        // project A
        WebResponse response = doDELETEAtURL(getProjectUrl(projectA) + '/' + CATEGORIES);
        // MUST NOT BE ALLOWED
        assertEquals("", response.getText());

    }

}
