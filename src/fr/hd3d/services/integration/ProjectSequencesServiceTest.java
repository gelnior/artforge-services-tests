package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.SEQUENCES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.impl.LSequence;


/**
 * Tests web service Sequence in Project Sequences: list (GET) and singleton (CRUD)
 * 
 * @author Try LAM
 */
public class ProjectSequencesServiceTest extends AbstractServiceIntegrationTest<ILSequence>
{

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=SequenceShots");
    }

    /**
     * Test Sequences retrieved through web services (GET /hd3dServices/project/{projectId}/sequences/).
     * 
     * @throws IOException
     */
    @Test
    public void testSequencesGET() throws IOException
    {

        ILProject projectA = getProjectA();
        final String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<LSequence> sequences = doGetObject(url);
        // retrieve the main sequences (with "sequence" as parent) for the
        // project A
        assertEquals(2, sequences.size());
        for (LSequence sequence : sequences)
        {

            if ("Pilote".equals(sequence.getName()))
            {
                assertEquals(projectA.getId(), sequence.getProject());
            }
            // TODO check other categories
        }
    }

    /**
     * Test Sequences CREATION through web services (POST /hd3dServices/project/{projectId}/sequences).
     * 
     * @throws IOException
     */
    @Test
    public void testSequencesPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // create a new main sequence (with "sequence" as parent) and part of
        // project A
        ILSequence sequence = LSequence.getNewInstance("nouvelle Séquence中国门", "Sequence title", null,
                projectA.getId(), null);
        WebResponse response = doPostAtURL(getProjectUrl(projectA) + "/" + SEQUENCES, sequence);
        assertEquals(CREATION_OK, response.getText());

        // retrieve the project A's newly created category and check
        String resp = this.getProjectUrl(getProjectA());
        ILSequence createdSequence = this.getSequenceInProject("nouvelle Séquence中国门", resp);

        assertEquals(sequence.getName(), createdSequence.getName());
        assertEquals(sequence.getProject(), createdSequence.getProject());
    }

    /**
     * MULTIPLE creation
     * 
     * @throws IOException
     */
    @Test
    public void testBulkSequencesPOST() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // create a new main sequence (with "sequence" as parent) and part of project A
        List<ILSequence> sequences = new ArrayList<ILSequence>();
        int NUMBERSEQUENCES = 3;
        for (int i = 0; i < NUMBERSEQUENCES; i++)
        {
            sequences.add(LSequence.getNewInstance("nouvelle Séquence中国门 " + i, "Séquence Titre", null, projectA
                    .getId(), null));
        }
        String url = getProjectUrl(projectA) + "/" + SEQUENCES;
        WebResponse response = doPostAtURL(url + "?mode=bulk", sequences);
        assertEquals(CREATION_OK, response.getText());

        // get the new constituents
        List<ILSequence> newSequences = doGetObject(url);
        int count = 0;
        for (ILSequence s : newSequences)
        {
            if (s.getName().startsWith("nouvelle Séquence中国门"))
            {
                count++;
            }
        }
        assertEquals(NUMBERSEQUENCES, count);
    }

    /**
     * Test Sequences UPDATE through web services (PUT /hd3dServices/project/{projectId}/sequences).
     * 
     * @throws IOException
     */
    @Test
    public void testSequencesPUT() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        final String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<ILSequence> sequences = doGetObject(url);
        List<ILSequence> updated = new ArrayList<ILSequence>();
        for (ILSequence sequence : sequences)
        {
            // update the categories
            sequence.setName(sequence.getName() + " UPDATED");
            updated.add(sequence);
        }
        WebResponse response = doPUTAtURL(getProjectUrl(projectA) + '/' + SEQUENCES, updated);
        // check
        assertEquals(200, response.getResponseCode());
        sequences.clear();
        sequences = doGetObject(url);
        for (ILSequence sequence : sequences)
        {
            assertTrue(sequence.getName().endsWith(" UPDATED"));
        }
    }

    /**
     * Test Sequences DELETE through web services (DELETE /hd3dServices/project/{projectId}/sequences).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoriesDELETE() throws IOException
    {
        // get the project A
        ILProject projectA = getProjectA();
        // create a new main category (with "constituent" as parent) and part of
        // project A
        WebResponse response = doDELETEAtURL(getProjectUrl(projectA) + '/' + SEQUENCES);
        // MUST NOT BE ALLOWED
        assertEquals("", response.getText());

    }

}
