/**
 * 
 */
package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILStep;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.ILTaskType;
import fr.hd3d.model.lightweight.impl.LStep;
import fr.hd3d.utils.Const;


/**
 * @author Try LAM
 * 
 */
public class StepsServiceTest extends AbstractServiceIntegrationTest<ILStep>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
    }

    protected String getTaskTypeUrl(ILTaskType taskType)
    {
        assertNotNull(taskType);
        return SERVICE + TASK_TYPE + taskType.getId().toString();
    }

    /**
     * create ONE step
     */
    @Test
    public void testStepPOST() throws IOException
    {
        /* retrieve a tasktype */
        Collection<ILTaskType> taskTypes = doGetObject(SERVICE + TASK_TYPE);
        final ILTaskType taskType = taskTypes.iterator().next();

        ILProject projectA = getProjectA();

        // get all the constituents of Personnage category of project A
        ConstituentServiceTest test = new ConstituentServiceTest();
        String url = test.getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        final ILConstituent constituent = constituents.iterator().next();

        /* create step */
        ILStep step = LStep.getNewInstance(constituent.getId(), Const.ENTITYNAME_CONSTITUENT, taskType.getId(),
                taskType.getName(), Boolean.FALSE, 10);// duration is > 0, a task must be created

        WebResponse response = doPostAtURL(SERVICE + ServicesURI.STEPS, step);
        assertEquals(CREATION_OK, response.getText());

        /* check that a new task has been created */
        final Collection<ILTask> tasks = doGetObject(getProjectUrl(projectA) + "/tasks");
        boolean ok = false;
        for (ILTask task : tasks)
        {
            if (task.getName().equals(taskType.getName() + ' ' + constituent.getHook()))
            {
                ok = true;
                break;
            }
        }
        assertTrue(ok);
    }

    /**
     * create MANY steps
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testStepsPOST() throws IOException
    {
        final Map<?, ?> result = createSteps();

        WebResponse response = doPostAtURL(SERVICE + ServicesURI.STEPS + "?mode=bulk", (List<ILStep>) result
                .get("steps"));
        assertEquals(CREATION_OK, response.getText());

        /* check that a new task has been created */
        Collection<ILTask> tasks = doGetObject(getProjectUrl((ILProject) result.get("project")) + "/tasks");
        int nb = 0;
        for (ILTask task : tasks)
        {
            if (matchTask(task, (List<ILTaskType>) result.get("taskTypes"), (List<ILConstituent>) result
                    .get("constituentsList")))
            {
                nb++;
            }
        }
        assertEquals(result.get("nbOfTasks"), nb);

        /* recreate the same */
        // response = doPostAtURL(SERVICE + ServicesURI.STEPS + "?mode=bulk", (List<ILStep>) result.get("steps"));
        // assertEquals(CREATION_OK, response.getText());
        // tasks = doGetObject(getProjectUrl((ILProject) result.get("project")) + "/tasks");
        // nb = 0;
        // for (ILTask task : tasks)
        // {
        // if (matchTask(task, (List<ILTaskType>) result.get("taskTypes"), (List<ILConstituent>) result
        // .get("constituentsList")))
        // {
        // nb++;
        // }
        // }
        // assertEquals(result.get("nbOfTasks"), nb);// the number remains the same
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testStepsPUT() throws IOException
    {
        /* create steps */
        final Map<?, ?> result = createSteps();

        WebResponse response = doPostAtURL(SERVICE + ServicesURI.STEPS + "?mode=bulk", (List<ILStep>) result
                .get("steps"));
        assertEquals(CREATION_OK, response.getText());

        /* check that a new task has been created */
        Collection<ILTask> tasks = doGetObject(getProjectUrl((ILProject) result.get("project")) + "/tasks");
        int nb = 0;
        for (ILTask task : tasks)
        {
            if (matchTask(task, (List<ILTaskType>) result.get("taskTypes"), (List<ILConstituent>) result
                    .get("constituentsList")))
            {
                nb++;
            }
        }
        assertEquals(result.get("nbOfTasks"), nb);

        /* update the steps's duration, attached tasks' duration must be updated too */
        List<ILStep> createdSteps = doGetObject(SERVICE + ServicesURI.STEPS);

        createdSteps.get(0).setEstimatedDuration(0);// the task attached must be deleted
        createdSteps.get(1).setEstimatedDuration(5);// the task attached must be updated
        createdSteps.get(2).setEstimatedDuration(10);// the task attached must be created

        response = doPUTAtURL(SERVICE + ServicesURI.STEPS + "?mode=bulk", createdSteps);
        assertEquals(200, response.getResponseCode());
        tasks = doGetObject(getProjectUrl((ILProject) result.get("project")) + "/tasks");
        nb = 0;
        for (ILTask task : tasks)
        {
            if (matchTask(task, (List<ILTaskType>) result.get("taskTypes"), (List<ILConstituent>) result
                    .get("constituentsList")))
            {
                nb++;
            }
        }
        // assertEquals(result.get("nbOfTasks"), nb);// the number remains the same
    }

    private boolean matchTask(ILTask taskToMatch, List<ILTaskType> taskTypes, List<ILConstituent> constituents)
    {
        boolean ok = false;
        for (int i = 0; i < taskTypes.size(); i++)
        {
            ok = taskToMatch.getName().equals(taskTypes.get(i).getName() + ' ' + constituents.get(i).getHook());
            if (ok)
                break;
        }
        return ok;
    }

    @SuppressWarnings("unchecked")
    private Map<?, ?> createSteps() throws IOException
    {
        /* retrieve a tasktype */
        Collection<ILTaskType> taskTypes = doGetObject(SERVICE + TASK_TYPE);
        final Iterator<ILTaskType> it = taskTypes.iterator();
        final ILTaskType taskType1 = it.next();
        final ILTaskType taskType2 = it.next();
        final ILTaskType taskType3 = it.next();

        ILProject projectA = getProjectA();

        // get all the constituents of Personnage category of project A
        ConstituentServiceTest test = new ConstituentServiceTest();
        String url = test.getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        final ILConstituent constituent1 = constituents.iterator().next();
        final ILConstituent constituent2 = constituents.iterator().next();
        final ILConstituent constituent3 = constituents.iterator().next();

        /* create step */
        List<ILConstituent> constituentsList = new ArrayList<ILConstituent>();
        List<String> entityNamesList = new ArrayList<String>();
        List<ILTaskType> taskTypesList = new ArrayList<ILTaskType>();
        List<Boolean> createTasknAssetsList = new ArrayList<Boolean>();
        List<Integer> estimatedDurationsList = new ArrayList<Integer>();

        constituentsList.add(constituent1);
        constituentsList.add(constituent2);
        constituentsList.add(constituent3);

        entityNamesList.add(Const.ENTITYNAME_CONSTITUENT);
        entityNamesList.add(Const.ENTITYNAME_CONSTITUENT);
        entityNamesList.add(Const.ENTITYNAME_CONSTITUENT);

        taskTypesList.add(taskType1);
        taskTypesList.add(taskType2);
        taskTypesList.add(taskType3);

        createTasknAssetsList.add(Boolean.FALSE);
        createTasknAssetsList.add(Boolean.FALSE);
        createTasknAssetsList.add(Boolean.FALSE);

        estimatedDurationsList.add(10);// duration is > 0, a task must be created
        estimatedDurationsList.add(5);// duration is > 0, a task must be created
        estimatedDurationsList.add(0);// duration = 0, no task is created

        int nbOfTasks = 0;
        for (Integer d : estimatedDurationsList)
        {
            if (d > 0)
                nbOfTasks++;
        }

        Map map = new HashMap();
        map.put("steps", createSteps(constituentsList, entityNamesList, taskTypesList, createTasknAssetsList,
                estimatedDurationsList));
        map.put("project", projectA);
        map.put("constituentsList", constituentsList);
        map.put("entityNamesList", entityNamesList);
        map.put("taskTypes", taskTypesList);
        map.put("createTasknAssetsList", createTasknAssetsList);
        map.put("estimatedDurationsList", estimatedDurationsList);
        map.put("nbOfTasks", nbOfTasks);

        return map;
    }

    private List<ILStep> createSteps(List<ILConstituent> constituentsList, List<String> entityNamesList,
            List<ILTaskType> taskTypesList, List<Boolean> createTasknAssetsList, List<Integer> estimatedDurationsList)
    {
        List<ILStep> steps = new ArrayList<ILStep>();
        for (int i = 0; i < constituentsList.size(); i++)
        {
            steps.add(LStep.getNewInstance(constituentsList.get(i).getId(), entityNamesList.get(i), taskTypesList
                    .get(i).getId(), taskTypesList.get(i).getName(), createTasknAssetsList.get(i),
                    estimatedDurationsList.get(i)));
        }
        return steps;
    }
}
