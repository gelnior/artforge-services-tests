package fr.hd3d.services.integration;

import java.io.IOException;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.model.lightweight.ILPerson;


/**
 * Tests web service Person : list (GET) and singleton (CRUD)
 * 
 * @author Guillaume Chatelet
 */
public class UserAccountServiceTest extends AbstractServiceIntegrationTest<ILPerson>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    /**
     * It must be not possible to delete the root user
     * 
     * @throws IOException
     */
    @Test
    public void testRootDELETE() throws IOException
    {
        WebResponse response = doDELETEAtURL(SERVICE + ServicesURI.USERACCOUNTS + "/root");
        assertTrue(response.getResponseCode() == 422);
    }

}
