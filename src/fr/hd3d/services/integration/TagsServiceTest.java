package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.TAGS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.model.lightweight.impl.LTag.LTagParentIdType;


/**
 * Tests web service Tag : list (GET) and singleton (CRUD)
 * 
 * @author Try LAM
 */
public class TagsServiceTest extends AbstractServiceIntegrationTest<ILTag>
{

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=Tags");
    }

    /**
     * adds a tag called tagName to object, if tagName does not exist create it. className must be provided (business
     * entity class name) since it cannot be guessed out from lightweight classes
     * 
     * @param <T>
     * @param object
     * @param tagName
     * @throws IOException
     */
    public <T extends ILBase> void addTag(T object, String className, String tagName) throws IOException
    {
        List<LTagParentIdType> tagparents = new ArrayList<LTagParentIdType>();
        tagparents.add(new LTagParentIdType(object.getId(), className));
        ILTag tag = getOrCreateTag(tagName);
        object.addTag(tag.getId());
    }

    final static String TYPE_PROJECT = "Project";

    /**
     * Test Tags retrieve through web services (GET /hd3dServices/tags/).
     * 
     * @throws IOException
     * @throws Hd3dPersistenceException
     */
    @Test
    public void testTagsGET() throws IOException, Hd3dPersistenceException
    {
        // Get Tags List
        Collection<LTag> tags = doGetObject(SERVICE + '/' + TAGS);
        assertEquals(5, tags.size());
    }

    /**
     * Test Tag retrieve through web services (GET /hd3dServices/tags/{tagId}).
     * 
     * @throws IOException
     */
    @Test
    public void testTagGET() throws IOException
    {
        // Get Tags List
        Collection<LTag> tags = doGetObject(SERVICE + '/' + TAGS);
        assertEquals(5, tags.size());
        int count = 0;
        for (LTag t : tags)
        {
            String name = t.getName();
            if ("Generic Project tag".equals(name) || "Project A tag".equals(name)
                    || "Constituent tag Name0".equals(name) || "Constituent tag Name1".equals(name))
            {
                count++;
            }
        }
        assertEquals(4, count);
    }

    /**
     * Test Tag creation through web services (POST /hd3dServices/tags/).
     * 
     * @throws IOException
     */
    @Test
    public void testTagsPOST() throws IOException
    {
        // create the project
        ProjectServiceTest projectServiceTest = new ProjectServiceTest();
        WebResponse response = projectServiceTest.createProject(projectServiceTest.getTestInstance1());
        assertEquals(CREATION_OK, response.getText());

        // read it back
        ILProject project = getProject(projectServiceTest.getTestInstance1().getName());

        ILTag tag = getOrCreateTag("NEW TAG");
        tag.addParent(project.getId(), TYPE_PROJECT);
        response = doPostAtURL(SERVICE + '/' + TAGS, tag);
        assertEquals(CREATION_OK, response.getText());

        // read it back
        Long createdTagId = 0L;

        Collection<LTag> tags = doGetObject(SERVICE + '/' + TAGS);
        boolean ok = false;
        for (LTag t : tags)
        {
            String name = t.getName();
            if ("NEW TAG".equals(name))
            {
                /* for later use */
                createdTagId = t.getId();
                /**/
                Collection<LTagParentIdType> parentsIdType = t.getParents();
                for (LTagParentIdType tgp : parentsIdType)
                {
                    if (tgp.getParentId().equals(project.getId()) && tgp.getParentType().equals(TYPE_PROJECT))
                    {
                        ok = true;
                    }
                }
            }
        }
        assertEquals(true, ok);

        /* delete the tag, the attached entity must not be attached anymore */
        doDELETEAtURL(SERVICE + '/' + TAGS + '/' + createdTagId);
        ILProject updatedProject = getProject(project.getName());

        List<Long> projectTags = updatedProject.getTags();
        assertTrue(projectTags == null || projectTags.isEmpty() || !projectTags.contains(createdTagId));

        /* remove all tags from an entity and update it */
        ILProject projectA = getProjectA();
        assertTrue(!projectA.getTags().isEmpty());

        projectA.setTags(new ArrayList());
        doPUTAtURL(getProjectUrl(projectA), projectA);
        projectA = getProjectA();
        assertTrue(projectA.getTags().isEmpty());

    }

    /**
     * Test Tag modification through web services (PUT /hd3dServices/tags/{tagId}).
     * 
     * @throws IOException
     */
    @Test
    public void testTagPUT() throws IOException
    {
        // Get Tags List
        Collection<LTag> tags = doGetObject(SERVICE + '/' + TAGS);
        ILProject projectA = getProjectA();
        final int projectAtagsNbs = projectA.getTags().size();
        for (LTag t : tags)
        {
            String name = t.getName();
            if ("Generic Project tag".equals(name))
            {
                // --------
                // update
                // --------
                t.setName("MODIFIED");// update the name
                ILProject projectB = getProjectB();
                final int projectBtagsNb = projectB.getTags().size();
                ILProject projectC = getProjectC();
                int projectCtagsNb = 0;
                if (projectC.getTags() != null)
                    projectCtagsNb = projectC.getTags().size();
                // remove the parent project A
                t.removeParent(projectA.getId(), TYPE_PROJECT);
                // add the parent project B & C
                t.addParent(projectB.getId(), TYPE_PROJECT);// NOTE:project B is already attached to this tag
                t.addParent(projectC.getId(), TYPE_PROJECT);
                doPUTAtURL(SERVICE + "/" + TAGS + "/" + t.getId(), t);

                // ------------
                // read it back
                // ------------
                LTag updated = doGetObject(SERVICE + '/' + TAGS + "/" + t.getId());
                assertEquals("MODIFIED", updated.getName());// name updated ?

                boolean projectApresent = false;
                int count = 0;
                Collection<LTagParentIdType> ltagParentsIdType = updated.getParents();
                for (Iterator<LTagParentIdType> it = ltagParentsIdType.iterator(); it.hasNext();)
                {
                    LTagParentIdType ltgp = it.next();
                    if (ltgp.getParentId().equals(projectA.getId()))
                    {
                        projectApresent = true;
                    }
                    if (ltgp.getParentId().equals(projectB.getId()) || ltgp.getParentId().equals(projectC.getId()))
                    {
                        count++;
                    }
                }
                assertEquals(false, projectApresent);// project A must not be a parent anymore
                assertEquals(2, count);// project B and C must be parents
                // refresh projects
                projectA = getProjectA();
                projectB = getProjectB();
                projectC = getProjectC();
                assertEquals(projectAtagsNbs - 1, projectA.getTags().size());// project A has one tag less attached
                assertEquals(projectBtagsNb, projectB.getTags().size());// no change for project B (already attached)
                assertEquals(projectCtagsNb + 1, projectC.getTags().size());// one more tag for project C
                break;
            }
        }
    }

    /**
     * Test Tag deletion through web services (DELETE /hd3dServices/tags/{tagId}).
     * 
     * @throws IOException
     */
    @Test
    public void testTagDELETE() throws IOException
    {
        Collection<LTag> tags = doGetObject(SERVICE + '/' + TAGS);
        assertEquals(5, tags.size());
        for (LTag t : tags)
        {
            String name = t.getName();
            // delete all tags with name="Generic Project tag"
            if ("Generic Project tag".equals(name))
            {
                doDELETEAtURL(SERVICE + '/' + TAGS + "/" + t.getId());
            }
        }
        tags = doGetObject(SERVICE + '/' + TAGS);
        assertEquals(4, tags.size());
    }

}
