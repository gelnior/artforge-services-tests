package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.impl.LTask;


/**
 * Tests web service Person's tasks in a project : list (GET) and singleton (CRUD)
 * 
 * @author Guillaume Chatelet
 */
public class ProjectPersonTaskServiceTest extends AbstractServiceIntegrationTest<ILTask>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    private static final String ANOTHER_SUPER_TASK = "Another super task";
    private static final String ONE_SUPER_TASK = "One super task";

    /**
     * Test Tasks retrieved through web services (GET /hd3dServices/project/{projectId}/persons/{personId}/tasks/).
     * 
     * @throws IOException
     */
    @Test
    public void testGetTasks() throws Exception
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final ILPerson acs = this.getPersonInProject("acs", projectUrl);
        final ILPerson eth = this.getPersonInProject("eth", projectUrl);
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/persons/" + gch.getId() + "/tasks");
        assertNotNull(gch);
        assertNotNull(acs);
        assertNotNull(eth);
        assertNotNull(tasks);

        assertEquals(1, tasks.size());
        for (ILTask task : tasks)
        {
            assertEquals(projectA.getName(), task.getProjectName());
            assertEquals(projectA.getId(), task.getProjectID());
            assertEquals(projectA.getColor(), task.getProjectColor());
            if (ONE_SUPER_TASK.equals(task.getName()))
            {
                assertEquals(acs.getName(), task.getCreatorName());
                assertEquals(acs.getId(), task.getCreatorID());
            }
            else if (ANOTHER_SUPER_TASK.equals(task.getName()))
            {
                assertEquals(eth.getName(), task.getCreatorName());
                assertEquals(eth.getId(), task.getCreatorID());
            }
            assertEquals(gch.getName(), task.getWorkerName());
            assertEquals(gch.getId(), task.getWorkerID());
            assertEquals((Byte) (byte) 0, task.getCompletion());
            assertNull(task.getCompletionDate());
            assertEquals(ETaskStatus.OK.toString(), task.getStatus());
        }
    }

    /**
     * Test Task retrieved through web services (GET /hd3dServices/project/{projectId}/persons/{personId}/tasks/).
     * 
     * @throws IOException
     */
    @Test
    public void testGetTask() throws Exception
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final ILPerson acs = this.getPersonInProject("acs", projectUrl);
        // TODO log
        String constraint = "?orderBy=[\"id\"]}]";
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/persons/" + gch.getId() + "/tasks" + constraint);
        assertEquals(1, tasks.size());
        final ILTask task = doGetObject(projectUrl + "/persons/" + gch.getId() + "/tasks/"
                + tasks.iterator().next().getId().toString());
        assertEquals(projectA.getName(), task.getProjectName());
        assertEquals(projectA.getId(), task.getProjectID());
        assertEquals(projectA.getColor(), task.getProjectColor());
        assertEquals(ONE_SUPER_TASK, task.getName());
        assertEquals(acs.getName(), task.getCreatorName());
        assertEquals(acs.getId(), task.getCreatorID());
        assertEquals(gch.getName(), task.getWorkerName());
        assertEquals(gch.getId(), task.getWorkerID());
        assertEquals((Byte) (byte) 0, task.getCompletion());
        assertNull(task.getCompletionDate());
        assertEquals(ETaskStatus.OK.toString(), task.getStatus());
        assertNotNull(task);
    }

    /**
     * Test Person creation through web services (POST /hd3dServices/project/{projectId}/persons/{personId}/tasks/).
     * 
     * @throws IOException
     */
    @Test
    public void testPostTask() throws IOException
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String tasksUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(1, tasks.size());
        final String TestTask = "The test task";
        final ILTask newTask = LTask.getNewInstance(projectA.getId(), TestTask, ETaskStatus.WORK_IN_PROGRESS, gch
                .getId(), gch.getId(), gch.getId(), "Person", null);
        final WebResponse response = doPostAtURL(tasksUrl, newTask);
        assertEquals(CREATION_OK, response.getText());
        tasks = doGetObject(tasksUrl);
        assertEquals(2, tasks.size());
        for (ILTask task : tasks)
        {
            if (TestTask.equals(task.getName()))
            {
                assertEquals(gch.getName(), task.getCreatorName());
                assertEquals(gch.getId(), task.getCreatorID());
                assertEquals(gch.getName(), task.getWorkerName());
                assertEquals(gch.getId(), task.getWorkerID());
                assertEquals(projectA.getName(), task.getProjectName());
                assertEquals(projectA.getId(), task.getProjectID());
                assertEquals(projectA.getColor(), task.getProjectColor());
                assertEquals(ETaskStatus.WORK_IN_PROGRESS.toString(), task.getStatus());
            }
        }
    }

    /**
     * Test Person modification through web services (PUT /hd3dServices/project/{projectId}/persons/{personId}/tasks/).
     * 
     * @throws IOException
     */
    @Test
    public void testUpdateTask() throws IOException
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String tasksUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(1, tasks.size());
        final ILTask task = tasks.iterator().next();
        final Date date = new Date();
        final String newTaskName = "New name";
        task.setCompletion((byte) 100);
        task.setCompletionDate(date);
        task.setName(newTaskName);
        final String taskUrl = tasksUrl + '/' + task.getId().toString();
        doPUTAtURL(taskUrl, task);
        final ILTask newTask = doGetObject(taskUrl);
        assertEquals(task.getCompletion(), newTask.getCompletion());
        assertEqualsDate(task.getCompletionDate(), newTask.getCompletionDate());
        assertEquals(task.getName(), newTask.getName());
    }

    /**
     * Test Person tasks deletion through web services (DELETE
     * /hd3dServices/project/{projectId}/persons/{personId}/tasks/).
     * 
     * @throws IOException
     */
    @Test
    public void testDeleteDeletableTask() throws IOException
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String tasksUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(1, tasks.size());
        for (ILTask task : tasks)
            if (ANOTHER_SUPER_TASK.equals(task.getName()))
                doDELETEAtURL(tasksUrl + '/' + task.getId().toString());
        tasks = doGetObject(tasksUrl);
        assertEquals(1, tasks.size());
    }

    /**
     * Test like condition on login, FirstName and LastName attributes.
     * 
     * @throws IOException
     */
    @Test
    /*
     * ONE_SUPER_TASK has an activity it upon and so cannot be removed without removing the underlying activity
     */
    public void testDeleteUndeletableTask() throws IOException
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        final String tasksUrl = getProjectUrl(projectA) + "/persons/" + gch.getId() + "/tasks";
        Collection<ILTask> tasks = doGetObject(tasksUrl);
        assertEquals(1, tasks.size());
        for (ILTask task : tasks)
            if (ONE_SUPER_TASK.equals(task.getName()))
                doDELETEAtURL(tasksUrl + '/' + task.getId().toString());
        tasks = doGetObject(tasksUrl);
        assertEquals(1, tasks.size());
    }

    @Test
    public void testTaskLike() throws Exception
    {

        final ILProject projectA = getProjectA();
        final String projectUrl = getProjectUrl(projectA);
        final ILPerson gch = this.getPersonInProject("gch", projectUrl);
        String constraint = getLikeConstraint("name", "One%");
        final Collection<ILTask> tasks = doGetObject(projectUrl + "/persons/" + gch.getId() + "/tasks/" + constraint);
        assertEquals(1, tasks.size());
        ILTask task = tasks.iterator().next();
        assertEquals(projectA.getName(), task.getProjectName());
        assertEquals(projectA.getId(), task.getProjectID());
        assertEquals(projectA.getColor(), task.getProjectColor());
        assertEquals(ONE_SUPER_TASK, task.getName());
        assertEquals((Byte) (byte) 0, task.getCompletion());
        assertNull(task.getCompletionDate());
        assertEquals(ETaskStatus.OK.toString(), task.getStatus());
    }

    /**
     * TODO Write a test to check the PUT of an invalid task bad workerID or projectID regarding the url.
     */
}
