package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import fr.hd3d.model.lightweight.ILActivity;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class QueryConstraintTest extends AbstractServiceIntegrationTest<ILActivity>
{

    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    private final String BASE_URL = SERVICE + ACTIVITY;

    /**
     * Test expression constraints through web services (GET /hd3dservices/activities).
     */
    @Test
    public void testExpression()
    {
        // Filter : <
        String constraint = "?constraint=[{\"type\":\"lt\",\"column\":\"duration\",\"value\":5}]";
        String url = BASE_URL + constraint;
        Collection<ILActivity> activities = this.doGetObject(url);
        assertEquals(1, activities.size());
        // Filter : >
        constraint = "?constraint=[{\"type\":\"gt\",\"column\":\"duration\",\"value\":2}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(6, activities.size());
        // Filter : <=
        constraint = "?constraint=[{\"type\":\"leq\",\"column\":\"duration\",\"value\":5}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(3, activities.size());
        // Filter : >=
        constraint = "?constraint=[{\"type\":\"geq\",\"column\":\"duration\",\"value\":2}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(7, activities.size());
        // Filter : ==
        constraint = "?constraint=[{\"type\":\"eq\",\"column\":\"duration\",\"value\":2}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(1, activities.size());
        // Filter : !=
        constraint = "?constraint=[{\"type\":\"neq\",\"column\":\"duration\",\"value\":2}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(6, activities.size());
        // Filter : between value 1 (start) and value 2 (end)
        constraint = "?constraint=[{\"type\":\"btw\",\"column\":\"duration\",\"value\":{\"start\":4,\"end\":6}}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(2, activities.size());
        // Filter : <= in (value 1, value 2, ..., value n)
        constraint = "?constraint=[{\"type\":\"in\",\"column\":\"duration\",\"value\":[2,10]}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(2, activities.size());
        // Filter : <= not in (value 1, value 2, ..., value n)
        constraint = "?constraint=[{\"type\":\"notin\",\"column\":\"duration\",\"value\":[2,10]}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(5, activities.size());
        // Filter : LIKE value_1%
        constraint = "?constraint=[{\"type\":\"like\",\"column\":\"comment\",\"value\":\"Ano%25\"}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(1, activities.size());
        // Filter : IS NULL
        constraint = "?constraint=[{\"type\":\"isnull\",\"column\":\"day\"}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(0, activities.size());
        // Filter : IS NOT NULL
        constraint = "?constraint=[{\"type\":\"isnotnull\",\"column\":\"day\"}]";
        url = BASE_URL + constraint;
        activities = this.doGetObject(url);
        assertEquals(7, activities.size());
    }
}
