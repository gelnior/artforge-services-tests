package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public class QueryFilterFactoryTest extends AbstractServiceIntegrationTest<ILResourceGroup>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    private final String BASE_URL = SERVICE + RESOURCE_GROUP;

    /**
     * Test pagination constraint through web services (GET /hd3dservices/groups).
     */
    @Test
    public void testPagination() throws IOException
    {
        int nbRecords = 0;

        // /groups?pagination={"first":0,"quantity":1}
        String constraint = "?orderBy=[\"id\"]&pagination={\"first\":0,\"quantity\":1}";
        String url = BASE_URL + constraint;
        Collection<ILResourceGroup> groups = this.doGetObject(url);
        assertEquals(1, groups.size());
        assertEquals("Test_group_00", getGroupNameFromCollection(groups, 0));
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);

        // /groups?pagination={"first":0,"quantity":20}
        constraint = "?orderBy=[\"id\"]&pagination={\"first\":0,\"quantity\":20}";
        url = BASE_URL + constraint;
        groups = this.doGetObject(url);
        assertEquals(20, groups.size());
        assertEquals("Test_group_00", getGroupNameFromCollection(groups, 0));
        assertEquals("Test_group_19", getGroupNameFromCollection(groups, 19));
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);

        // /groups?pagination={"first":20,"quantity":20}
        constraint = "?orderBy=[\"id\"]&pagination={\"first\":20,\"quantity\":20}";
        url = BASE_URL + constraint;
        groups = this.doGetObject(url);
        assertEquals(20, groups.size());
        assertEquals("Test_group_20", getGroupNameFromCollection(groups, 0));
        assertEquals("Test_group_39", getGroupNameFromCollection(groups, 19));
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);

        // /groups?pagination={"first":90,"quantity":20}
        constraint = "?orderBy=[\"id\"]&pagination={\"first\":90,\"quantity\":20}";
        url = BASE_URL + constraint;
        groups = this.doGetObject(url);
        assertEquals(10, groups.size());
        assertEquals("Test_group_90", getGroupNameFromCollection(groups, 0));
        assertEquals("Test_group_99", getGroupNameFromCollection(groups, 9));
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);

        // /groups?pagination={"first":110,"quantity":20}
        constraint = "?orderBy=[\"id\"]&pagination={\"first\":110,\"quantity\":20}";
        url = BASE_URL + constraint;
        groups = this.doGetObject(url);
        assertEquals(0, groups.size());
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);

        groups = this.doGetObject(BASE_URL + constraint);
        assertEquals(0, groups.size());

        // /groups?pagination={"first":0,"quantity":-1}
        constraint = "?orderBy=[\"id\"]&pagination={\"first\":0,\"quantity\":-1}";
        groups = this.doGetObject(BASE_URL + constraint);
        assertEquals(0, groups.size());
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);

        // /groups?pagination={"first":-10,"quantity":1}
        constraint = "?orderBy=[\"id\"]&pagination={\"first\":-10,\"quantity\":1}";
        groups = this.doGetObject(BASE_URL + constraint);
        assertEquals(1, groups.size());
        assertEquals("Test_group_00", getGroupNameFromCollection(groups, 0));
        // Verify number of records
        nbRecords = Hd3dJsonSerializer.unserializeNbRecords(doJsonGetAtURL(url).getText());
        // actually, it is the number of ALL records and not just the one displayed
        assertEquals(100, nbRecords);
    }

    private String getGroupNameFromCollection(Collection<ILResourceGroup> groups, int i)
    {
        return ((ILResourceGroup) groups.toArray()[i]).getName();
    }

    /**
     * Test order by constraint through web services (GET /hd3dservices/groups).
     */
    @Test
    public void testGroupsOrderBy()
    {

        // ASCENDANT : /groups?orderBy=["id"]
        String constraint = "?orderBy=[\"id\"]";
        boolean test = false;

        // Get groups with constraint
        Collection<ILResourceGroup> groups = this.doGetObject(BASE_URL + constraint);
        assertNotNull(groups);
        assertEquals(100, groups.size());

        // Test the order
        test = ((ILResourceGroup) groups.toArray()[0]).getId() < ((ILResourceGroup) groups.toArray()[1]).getId();
        assertEquals(true, test);

        // DESCENDANT : /groups?orderBy=["-id"]
        constraint = "?orderBy=[\"-id\"]";

        // Get groups with constraint
        groups = this.doGetObject(BASE_URL + constraint);
        assertNotNull(groups);
        assertEquals(100, groups.size());

        // Test the order
        test = ((ILResourceGroup) groups.toArray()[0]).getId() < ((ILResourceGroup) groups.toArray()[1]).getId();
        assertEquals(false, test);
    }
}
