package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.PLANNINGS;
import static fr.hd3d.common.client.ServicesURI.PROJECTS;
import static fr.hd3d.common.client.ServicesURI.SEQUENCES;
import static fr.hd3d.common.client.ServicesURI.SHOTS;
import static fr.hd3d.common.client.ServicesURI.TAGS;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.meterware.httpunit.ClientProperties;
import com.meterware.httpunit.HeadMethodWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.PutMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.DateFormat;
import fr.hd3d.model.lightweight.ILActivity;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPlanning;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.lightweight.ILTag;
import fr.hd3d.model.lightweight.impl.LTag;
import fr.hd3d.services.AbstractServiceTest;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Thomas ESKENAZI
 * @param <E>
 * 
 */
public abstract class AbstractServiceIntegrationTest<E extends Object> extends AbstractServiceTest
{
    protected static final String CREATION_OK = fr.hd3d.common.client.ResourceConst.CREATION_OK;
    protected static final String CONTENT_TYPE_HEADER = "Content-Type";
    protected static final String HTML_HEADER_VALUE = "text/html";
    protected static final String PERSON = "persons/";
    static
    {
        HttpUnitOptions.setDefaultCharacterSet("UTF-8");
        HttpUnitOptions.setDefaultContentType(JSON_HEADER_VALUE);
        ClientProperties.getDefaultProperties().setAcceptGzip(true);// NOTE: EOFException still remains in spite of this
    }

    protected WebResponse doHTMLGetAtURL(String uRL) throws IOException
    {
        return doGetAtURL(uRL, HTML_HEADER_VALUE);
    }

    protected static WebResponse doPostAtURL(String uRL, Object objectToSerialize) throws IOException
    {
        WebConversation conversation = new WebConversation();

        addAuthentication(conversation);
        String serializedObj = (String) Hd3dJsonSerializer.serialize(objectToSerialize);
        InputStream is = new ByteArrayInputStream(serializedObj.getBytes("UTF-8"));

        WebRequest request = new PostMethodWebRequest(uRL, is, JSON_HEADER_VALUE);

        // request.setParameter(JSonFormSerializer.OBJECT, serializedObj);

        // request.setParameter(null, serializedObj);
        // request.setHeaderField("Content-Type", JSON_HEADER_VALUE);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    protected WebResponse doPostAtURL(String uRL, List objectsToSerialize) throws IOException
    {
        WebConversation conversation = new WebConversation();

        addAuthentication(conversation);

        String serializedObj = (String) Hd3dJsonSerializer.serialize(objectsToSerialize);
        InputStream is = new ByteArrayInputStream(serializedObj.getBytes("UTF-8"));

        WebRequest request = new PostMethodWebRequest(uRL, is, JSON_HEADER_VALUE);

        // request.setParameter(JSonFormSerializer.OBJECT, serializedObj);
        // request.setParameter(null, serializedObj);
        // request.setHeaderField("Content-Type", JSON_HEADER_VALUE);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    protected WebResponse doPUTAtURL(String uRL, ILBase objectToSerialize) throws IOException
    {
        WebConversation conversation = new WebConversation();
        addAuthentication(conversation);
        String serializedObj = (String) Hd3dJsonSerializer.serialize(objectToSerialize);
        // System.out.println("JSON=" + serializedObj);
        // yeah, I know, StringBufferInputStream is @deprecated, but I had no
        // other choice here...
        // InputStream is = new StringBufferInputStream(JSonFormSerializer.OBJECT + '=' + serializedObj);
        // InputStream is = new ByteArrayInputStream((JSonFormSerializer.OBJECT + '=' +
        // serializedObj).getBytes("UTF-8"));
        InputStream is = new ByteArrayInputStream(serializedObj.getBytes("UTF-8"));
        WebRequest request = new PutMethodWebRequest(uRL, is, JSON_HEADER_VALUE);
        request.setParameter(JSonFormSerializer.OBJECT, serializedObj);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    // quick and dirty convenience method
    protected WebResponse doPUTConstituentAtURL(String uRL, ILConstituent objectToSerialize) throws IOException
    {
        WebConversation conversation = new WebConversation();
        addAuthentication(conversation);
        String serializedObj = (String) Hd3dJsonSerializer.serialize(objectToSerialize);
        // System.out.println("JSON=" + serializedObj);
        // yeah, I know, StringBufferInputStream is @deprecated, but I had no
        // other choice here...
        // InputStream is = new StringBufferInputStream(JSonFormSerializer.OBJECT + '=' + serializedObj);
        // InputStream is = new ByteArrayInputStream((JSonFormSerializer.OBJECT + '=' +
        // serializedObj).getBytes("UTF-8"));
        InputStream is = new ByteArrayInputStream(serializedObj.getBytes("UTF-8"));
        WebRequest request = new PutMethodWebRequest(uRL, is, JSON_HEADER_VALUE);
        request.setParameter(JSonFormSerializer.OBJECT, serializedObj);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    protected WebResponse doPUTAtURL(String uRL, List<? extends ILBase> objectsToSerialize) throws IOException
    {
        WebConversation conversation = new WebConversation();
        addAuthentication(conversation);
        String serializedObj = (String) Hd3dJsonSerializer.serialize(objectsToSerialize);
        // yeah, I know, StringBufferInputStream is @deprecated, but I had no
        // other choice here...
        // InputStream is = new StringBufferInputStream(JSonFormSerializer.OBJECT + '=' + serializedObj);
        // InputStream is = new ByteArrayInputStream((JSonFormSerializer.OBJECT + '=' +
        // serializedObj).getBytes("UTF-8"));
        InputStream is = new ByteArrayInputStream(serializedObj.getBytes("UTF-8"));
        WebRequest request = new PutMethodWebRequest(uRL, is, JSON_HEADER_VALUE);
        request.setParameter(JSonFormSerializer.OBJECT, serializedObj);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    // class StringReaderInputStream extends InputStream
    // {
    //
    // private final StringReader reader;
    //
    // public StringReaderInputStream(String s)
    // {
    // reader = new StringReader(s);
    // }
    //
    // public int read() throws IOException
    // {
    // return reader.read();
    // }
    // }

    /**
     * Sends a DELETE request to a specific URL.
     * 
     * @param uRL
     *            the target URL of the DELETE request.
     */
    protected WebResponse doDELETEAtURL(String uRL) throws IOException
    {
        WebConversation conversation = new WebConversation();
        addAuthentication(conversation);
        HeadMethodWebRequest request = new HeadMethodWebRequest(uRL);
        request.setMethod("DELETE");

        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    protected static ILProject getProjectA() throws IOException
    {
        return getProject("ProjectA");
    }

    protected ILProject getProjectB() throws IOException
    {
        return getProject("ProjectB");
    }

    protected ILProject getProjectC() throws IOException
    {
        return getProject("ProjectC");
    }

    protected String getLikeConstraint(String column, String value) throws IOException
    {
        String s = "?constraint="
                + URLEncoder.encode("[{\"type\":\"like\",\"column\":\"" + column + "\",\"value\":\"" + value + "\"}]",
                        "UTF-8");
        return s;
    }

    protected String getPaginationConstraint(int firstResult, int quantity)
    {

        return "?pagination={\"first\":" + firstResult + ",\"quantity\":" + quantity + "}";
    }

    protected String getGroupUrl(ILResourceGroup group)
    {

        assertNotNull(group);
        return SERVICE + RESOURCE_GROUP + group.getId().toString();
    }

    protected String getActivityUrl(ILActivity activity)
    {

        assertNotNull(activity);
        return SERVICE + ActivityServiceTest.ACTIVITY + activity.getId().toString();
    }

    protected static String getProjectUrl(ILProject project)
    {
        assertNotNull(project);
        return SERVICE + ProjectServiceTest.PROJECT + project.getId().toString();
    }

    protected String getPlanningUrlForTaskGroup(ILPlanning planning)
    {
        assertNotNull(planning);
        return SERVICE.substring(0, SERVICE.length() - 1) + '/' + PROJECTS + "/" + planning.getProject() + '/'
                + PLANNINGS + "/" + planning.getId().toString();
    }

    /**
     * Get URL where Person resource is available.
     * 
     * @param person
     *            Person which should
     * @return URL corresponding to person
     */
    protected String getPersonUrl(ILPerson person)
    {
        assertNotNull(person);
        return SERVICE + ProjectServiceTest.PERSON + person.getId().toString();
    }

    // CONVENIENCE METHODS
    public ILTag getOrCreateTag(String name) throws IOException
    {
        String constraint = "?tags=true&constraint="
                + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + name + "\"}]", "UTF-8");
        Collection<ILTag> tags = doGetObject(SERVICE + '/' + TAGS + constraint);

        if (tags != null && !tags.isEmpty())// tag found
        {
            return tags.iterator().next();
        }
        else
        // tag not found => create it
        {
            ILTag tag = LTag.getNewInstance(name, null);
            // WebResponse response =
            doPostAtURL(SERVICE + '/' + TAGS, (E) tag);
            tags = doGetObject(SERVICE + '/' + TAGS + constraint);
            assertEquals(1, tags.size());
            return tags.iterator().next();
        }
    }

    public Collection<ILProject> getAllProjects()
    {
        return doGetObject(SERVICE + '/' + PROJECTS);
    }

    public ILProject getProject(int index)
    {
        Collection<ILProject> projects = getAllProjects();
        if (projects == null)
            return null;

        return ((ILProject[]) projects.toArray())[index];
    }

    public Collection<ILSequence> getAllSequences(ILProject project)
    {
        return doGetObject(getProjectUrl(project) + "/" + SEQUENCES);
    }

    public ILSequence getSequence(ILProject project, int index)
    {
        Collection<ILSequence> sequences = getAllSequences(project);
        if (sequences == null)
            return null;

        return ((ILSequence[]) sequences.toArray())[index];
    }

    public Collection<ILShot> getAllShots(ILProject project, ILSequence sequence)
    {
        String url = getProjectUrl(project) + "/" + SEQUENCES + "/" + sequence.getId() + '/' + SHOTS;
        return doGetObject(url);
    }

    public ILShot getShot(ILProject project, ILSequence sequence, int index)
    {
        Collection<ILShot> shots = getAllShots(project, sequence);
        if (shots == null)
            return null;
        return ((ILShot[]) shots.toArray())[index];
    }

    public boolean assertEqualsDate(Date d1, Date d2)
    {
        SimpleDateFormat format = new SimpleDateFormat(DateFormat.DATE_TIME_STRING);
        return format.format(d1).toString().equals(format.format(d2).toString());
    }
}
