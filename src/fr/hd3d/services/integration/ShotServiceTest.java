package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.SEQUENCES;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import fr.hd3d.exception.Hd3dPersistenceException;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILShot;
import fr.hd3d.model.lightweight.impl.LSequence;


/**
 * @author Try LAM
 */
public class ShotServiceTest extends AbstractServiceIntegrationTest<ILShot>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=SequenceShots");
    }

    /**
     * Test Shot retrieved through web services (GET
     * /hd3dServices/project/{projectId}/sequences/{sequenceID}/shots/{shotId} ).
     * 
     * @throws IOException
     * @throws Hd3dPersistenceException
     */
    @Test
    public void testShotGET() throws IOException
    {
        // get all the shots of Pilote sequence of project A
        Collection<ILShot> shots = getShots();
        assertEquals(2, shots.size());
    }

    public Collection<ILShot> getShots() throws IOException
    {
        // get all the shots of Pilote sequence of project A
        String url = getProjectAPiloteSequenceUrl() + "/shots";
        return doGetObject(url);
    }

    /**
     * Test Shot MODIFICATION through web services (PUT
     * /hd3dServices/project/{projectId}/sequences/{sequenceID}/shots/{shotId} ).
     * 
     * @throws IOException
     */
    @Test
    public void testShotPUT() throws IOException
    {
        // get all the shots of Pilote sequence of project A
        String url = getProjectAPiloteSequenceUrl() + "/shots";
        Collection<ILShot> shots = doGetObject(url);
        assertEquals(2, shots.size());
        // updated one of them
        for (ILShot s : shots)
        {
            if (s.getLabel().equals("shot 3 label"))
            {
                s.setLabel("shot 3 label UPDATED");
                s.setDescription("UPDATED");
                url += "/" + s.getId();
                doPUTAtURL(url, s);
                // read it back
                ILShot updated = doGetObject(url);
                assertEquals("shot 3 label UPDATED", updated.getLabel());
                assertEquals("UPDATED", updated.getDescription());
            }
        }

        // /* Take the first Shot object */
        // ILShot shot = shots.iterator().next();
        //
        // /* keep a copy */
        // final ILShot copy = new LShot(shot);
        //
        // /*
        // * update fields
        // */
        // shot.setLabel(shot.getLabel() + " UPDATED");
        // shot.setDescription(shot.getDescription() + "UPDATED");
        // // update the sequence with the first Sequence different from the current one
        // Collection<ILSequence> sequences = getAllSequences(getProjectA());
        // for (ILSequence seq : sequences)
        // {
        // if (!copy.getSequence().equals(seq.getId()))
        // {
        // shot.setSequence(seq.getId());
        // }
        // }

    }

    /**
     * Test Category CREATION through web services (POST /hd3dServices/project/{projectId}/sequences/{sequenceId}).
     * 
     * @throws IOException
     */
    // @Test
    // public void testCategoryPOST() throws IOException {
    // // get the project A
    // ILProject projectA = getProjectA();
    // String url = getProjectUrl(projectA) + Hd3dApplication.CATEGORIES;
    // // retrieve the main sequences (with "shotituent" as parent) for the project A
    // Collection<LSequence> sequences = doGetObject(url);
    // assertEquals(2, sequences.size());
    // for (LSequence sequence : sequences) {
    // // retrieve the Pilote main sequence
    // if ("Pilote".equals(sequence.getName())) {
    // // create a new sub-sequence (with "Pilote" as parent)
    // ILSequence subCategory = LSequence.getNewInstance(null, null, "Sous Categorie", null, projectA.getId());
    // String url1 = getProjectUrl(projectA) + Hd3dApplication.CATEGORIES + "/" + sequence.getId();
    // WebResponse response = doPostAtURL(url1, subCategory);
    // assertEquals("Creation ok", response.getText());
    // // retrieve the project A's newly created sequence and check
    // sequences = doGetObject(url);
    // for (LSequence newCategorie : sequences) {
    // if ("Pilote".equals(newCategorie.getName())) {
    // String url2 = getProjectUrl(projectA) + Hd3dApplication.CATEGORIES + "/" + newCategorie.getId();
    // Collection<LSequence> newCategories = doGetObject(url2);
    // // Pilote sequence must have one more new sub sequence
    // assertEquals(3, newCategories.size());
    // }
    // }
    // }
    // }
    // }
    //
    // /**
    // * Test Category DELETION through web services (DELETE /hd3dServices/project/{projectId}/sequences/{sequenceId}).
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoryDELETE() throws IOException {
    //
    // // get the "Decor" sequence of the project A
    // ILSequence sequence = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
    // // delete it
    // String url = getProjectUrl(getProjectA()) + "/" + Hd3dApplication.CATEGORIES + "/" + sequence.getId();
    // doDELETEAtURL(url);
    // // read it back again
    // ILSequence sequence1 = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
    //
    // assertEquals(sequence, sequence1);
    // }
    //
    // /**
    // * Test like condition on Category name
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoryLike() throws IOException {
    //
    // String constraint = getLikeConstraint("name", "Perso%");
    // Collection<LPerson> sequences = doGetObject(getProjectUrl(getProjectA()) + Hd3dApplication.CATEGORIES
    // + constraint);
    // assertEquals(1, sequences.size());
    // }
    // convenience method
    private String getProjectAPiloteSequenceUrl() throws IOException
    {
        String ret = null;

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + SEQUENCES;
        Collection<LSequence> sequences = doGetObject(url);
        // retrieve the main sequences (with "sequence" as parent) for the
        // project A
        assertEquals(2, sequences.size());
        for (LSequence sequence : sequences)
        {

            if ("Pilote".equals(sequence.getName()))
            {
                assertEquals(projectA.getId(), sequence.getProject());
                // get the subsequences
                url = url + "/" + sequence.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }
}
