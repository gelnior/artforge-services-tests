package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.CLASSDYNMETADATATYPES;
import static fr.hd3d.common.client.ServicesURI.DYNMETADATATYPES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILClassDynMetaDataType;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILDynMetaDataType;
import fr.hd3d.model.lightweight.ILDynMetaDataValue;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LClassDynMetaDataType;


/**
 * Tests web service ClassDynMetaDataTypes.
 * 
 * @author Try LAM
 */
public class ClassDynMetaDataTypesServiceTest extends AbstractServiceIntegrationTest<ILClassDynMetaDataType>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=DynMetaData");
    }

    /**
     * Test ClassDynMetaDataTypes CREATION through web services (POST /hd3dServices/classdynmetadatatypes).
     * 
     * @throws IOException
     */
    @Test
    public void testClassDynMetaDataTypesPOST() throws IOException
    {
        /* find out the DynMetaDataType */
        ILDynMetaDataType type = getDynMetaDataTypeByName("boolean");

        /* create a ClassDynMetaDataType bound to the previous DynMetaDataType */
        ILClassDynMetaDataType classDyn = LClassDynMetaDataType.getNewInstance("one more name 1", type.getId(),
                "label='const 1 label'", "Constituent", null);
        String url = SERVICE + '/' + CLASSDYNMETADATATYPES;
        WebResponse response = doPostAtURL(url, classDyn);
        assertEquals(CREATION_OK, response.getText());

        /* read back the constituent and check newly created DynMetaDataValues */
        ILConstituent constituent = getProjectAPersonnageConstituent("const 1 label");
        Collection<ILDynMetaDataValue> dynValues = constituent.getDynMetaDataValues();
        assertTrue(dynValues != null && !dynValues.isEmpty());
        for (ILDynMetaDataValue d : dynValues)
        {
            System.out.println(d);
        }
    }

    /**
     * Test ClassDynMetaDataTypes MULITPLE CREATION through web services (POST /hd3dServices/classdynmetadatatypes).
     * 
     * @throws IOException
     */
    @Test
    public void testBulkClassDynMetaDataTypesPOST() throws IOException
    {

        ILDynMetaDataType type = getDynMetaDataTypeByName("boolean");

        List<ILClassDynMetaDataType> list = new ArrayList<ILClassDynMetaDataType>();

        // create 2 ClassDynMetaDataTypes for "Constituent" entity
        ILClassDynMetaDataType classDyn = LClassDynMetaDataType.getNewInstance("one more name 1", type.getId(),
                "label='const 1 label'", "Constituent", null);

        ILClassDynMetaDataType classDyn1 = LClassDynMetaDataType.getNewInstance("one more name 2", type.getId(),
                "label='const 2 label'", "Constituent", null);

        // create 1 ClassDynMetaDataType for "Shot" entity, with an existing name (unique name testing)
        ILClassDynMetaDataType classDyn2 = LClassDynMetaDataType.getNewInstance("one more name 2", type.getId(),
                "label='shot 2 label'", "Shot", null);

        list.add(classDyn);
        list.add(classDyn1);
        list.add(classDyn2);

        String url = SERVICE + '/' + CLASSDYNMETADATATYPES;
        WebResponse response = doPostAtURL(url + "?mode=bulk", list);
        assertEquals(CREATION_OK, response.getText());

        // At this point, all the Constituents with label='const 1 label' OR label='const 2 label'
        // will have a dynMetaDataValue created based on dynMetaDataType "hasFur"
        // ILConstituent const1 = getProjectAPersonnageConstituent("const 1 label");
        // int initCounter = 0;
        //
        // for (ILDynMetaDataValue dyn : const1.getDynMetaDataValues())
        // {
        // dyn.setValue("TRUE");
        // initCounter++;
        // }
        // String urlConst = getProjectAPersonnageCategoryUrl() + "/constituents";
        // urlConst += "/" + const1.getId();
        // doPUTConstituentAtURL(urlConst, const1);
        // // read it back
        // ILConstituent updated = doGetObject(urlConst);
        // int updatedCounter = 0;
        // for (ILDynMetaDataValue dyn : updated.getDynMetaDataValues())
        // {
        // if (dyn.getValue().equals("TRUE"))
        // updatedCounter++;
        // }
        // assertEquals(initCounter, updatedCounter);
    }

    /**
     * Test ClassDynMetaDataTypes UPDATE through web services (PUT
     * /hd3dServices/classdynmetadatatypes/{classdynmetadatatypeId}).
     * 
     * @throws IOException
     */
    // @Test
    // public void testClassDynMetaDataTypesPUT() throws IOException
    // {
    //
    // ILConstituent const1 = getProjectAPersonnageConstituent("const 1 label");
    // const1.setLabel("const 1 label UPDATED");
    // String url = getProjectAPersonnageCategoryUrl() + "/constituents/" + const1.getId();
    // doPUTAtURL(url, const1);
    // // read back
    // ILConstituent const1Updated = doGetObject(url);
    // assertEquals("const 1 label UPDATED", const1Updated.getLabel());
    // }
    /**
     * Test ClassDynMetaDataTypes BULK UPDATE through web services (PUT /hd3dServices/classdynmetadatatypes/).
     * 
     * @throws IOException
     */
    // @Test
    // public void testClassDynMetaDataTypesPUTBulk() throws IOException
    // {
    //
    // ILConstituent const1 = getProjectAPersonnageConstituent("const 1 label");
    // const1.setLabel("const 1 label UPDATED");
    // ILConstituent const2 = getProjectAPersonnageConstituent("const 2 label");
    // const2.setLabel("const 2 label UPDATED");
    // List<ILConstituent> constituents = new ArrayList<ILConstituent>();
    // constituents.add(const1);
    // constituents.add(const2);
    // String url = getProjectAPersonnageCategoryUrl() + "/constituents/";
    // doPUTAtURL(url, constituents);
    // // read back
    // ILConstituent const1Updated = doGetObject(url + const1.getId());
    // assertEquals("const 1 label UPDATED", const1Updated.getLabel());
    // ILConstituent const2Updated = doGetObject(url + const2.getId());
    // assertEquals("const 2 label UPDATED", const2Updated.getLabel());
    // }
    // private String getProjectAPersonnageCategoryUrl() throws IOException
    // {
    // String ret = null;
    //
    // ILProject projectA = getProjectA();
    // String url = getProjectUrl(projectA) + CATEGORIES;
    // Collection<LCategory> categories = doGetObject(url);
    // // retrieve the main categories (with "constituent" as parent) for the
    // // project A
    // assertEquals(2, categories.size());
    // for (LCategory category : categories)
    // {
    // if ("Personnage".equals(category.getName()))
    // {
    // assertEquals(projectA.getId(), category.getProject());
    // // get the subcategories
    // url = url + "/" + category.getId();
    // ret = url;
    // break;
    // }
    // }
    // return ret;
    // }
    // convenience method
    private String getProjectAPersonnageCategoryUrl() throws IOException
    {
        String ret = null;

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {
            if ("Personnage".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
                // get the subcategories
                url = url + "/" + category.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }

    // convenience method
    private ILCategory getProjectAPersonnageCategory() throws IOException
    {
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {

            if ("Personnage".equals(category.getName()))
            {
                return category;
            }
        }
        return null;
    }

    // convenience method
    private ILConstituent getProjectAPersonnageConstituent(String label) throws IOException
    {
        String url = getProjectAPersonnageCategoryUrl() + "/constituents?dyn=true";
        Collection<ILConstituent> constituents = doGetObject(url);
        for (ILConstituent c : constituents)
        {
            if (c.getLabel().equals(label))
            {
                return c;// actually, it may have several ones, but just return the first here
            }
        }
        return null;
    }

    public static ILDynMetaDataType getDynMetaDataTypeByName(String name)
    {
        String url = SERVICE + '/' + DYNMETADATATYPES;
        Collection<ILDynMetaDataType> types = doGetObject(url);
        for (ILDynMetaDataType t : types)
        {
            if (name.equals(t.getName()))
            {
                return t;
            }
        }
        return null;
    }

    public static ILClassDynMetaDataType getClassDynMetaDataTypeByName(String name)
    {
        String url = SERVICE + '/' + CLASSDYNMETADATATYPES;
        Collection<ILClassDynMetaDataType> types = doGetObject(url);
        for (ILClassDynMetaDataType t : types)
        {
            if (name.equals(t.getName()))
            {
                return t;
            }
        }
        return null;
    }

}
