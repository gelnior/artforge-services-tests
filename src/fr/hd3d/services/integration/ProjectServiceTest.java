/**
 * 
 */
package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;


/**
 * @author Thomas ESKENAZI
 */
public class ProjectServiceTest extends AbstractServiceIntegrationTest<ILProject>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=Tags");
    }

    LProject project;
    LProject updatedProject;

    public ILProject getTestInstance1()
    {
        project = new LProject();
        project.setColor("#123456");
        project.setName("ProjTest");
        project.setStatus(EProjectStatus.CLOSED.toString());
        project.setHook("ProjTesthook");
        return project;
    }

    public ILProject getTestInstance2()
    {
        updatedProject = new LProject();
        updatedProject.setColor("#654321");
        updatedProject.setName("ProjTest2");
        updatedProject.setStatus(EProjectStatus.OPEN.toString());
        updatedProject.setHook("ProjTest2hook");
        return updatedProject;
    }

    public WebResponse createProject(ILProject project) throws IOException
    {
        return doPostAtURL(SERVICE + PROJECT, project);
    }

    public WebResponse deleteProject(ILProject project) throws IOException
    {
        return doDELETEAtURL(getProjectUrl(project));
    }

    public ILProject updateProject(ILProject project, ILProject update) throws IOException
    {
        project.setColor(update.getColor());
        project.setName(update.getName());
        project.setStatus(update.getStatus());
        project.setHook(update.getHook());
        WebResponse resp = doPUTAtURL(getProjectUrl(project), project);

        try
        {
            return (ILProject) Hd3dJsonSerializer.unserialize(resp.getText());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void testProjectsGET() throws IOException
    {
        Collection<LProject> projects = doGetObject(SERVICE + PROJECT);
        assertEquals(3, projects.size());
        for (LProject project : projects)
        {
            if ("ProjectA".equals(project.getName()))
            {
                assertEquals("#FF0000", project.getColor());
            }
            else if ("ProjectB".equals(project.getName()))
            {
                assertEquals("#0000FF", project.getColor());
            }
            else if ("ProjectC".equals(project.getName()))
            {
                assertEquals("#00FF00", project.getColor());
            }
            else if (!project.getName().startsWith("Project"))
            {
                fail("unexpected project :" + project.getName());
            }
        }
    }

    @Test
    public void testProjectGET() throws IOException
    {
        ILProject projectB = getProjectB();
        projectB = doGetObject(getProjectUrl(projectB));
        checkProjectB(projectB);
    }

    @Test
    public void testProjectsPOST() throws IOException
    {
        ILProject project = getTestInstance1();
        // create the project
        WebResponse response = createProject(project);
        assertEquals(CREATION_OK, response.getText());
        // create the project again: must not be possible
        response = createProject(project);
        assertEquals(400, response.getResponseCode());
        /* create a project with given uuid */
        ILProject project_2 = getTestInstance2();
        final String uuid = "myUUID";
        project_2.setInternalUUID(uuid);
        response = doPostAtURL(SERVICE + PROJECT + "?sync=true", project_2);
        assertEquals(CREATION_OK, response.getText());
        ILProject createdProject_2 = getProject(project_2.getName(), "&sync=true");
        assertEquals(uuid, createdProject_2.getInternalUUID());
    }

    @Test
    public void testProjectADELETE() throws IOException
    {
        ILProject projectA = getProjectA();
        deleteProject(projectA);
        ILProject deletedProject = getProjectA();
        assertNull(deletedProject);
    }

    @Test
    public void testProjectDELETE() throws IOException
    {
        // create the project
        WebResponse response = createProject(getTestInstance1());
        assertEquals(CREATION_OK, response.getText());
        // read it back
        ILProject project = getProject(getTestInstance1().getName());
        assertEquals(getTestInstance1().getColor(), project.getColor());
        assertEquals(getTestInstance1().getStatus(), project.getStatus());
        // delete it
        deleteProject(project);
        // read it back again
        ILProject deletedProject = getProject(getTestInstance1().getName());
        assertNull(deletedProject);
    }

    @Test
    public void testProjectPUT() throws IOException
    {
        ILProject projectC = getProjectC();
        projectC.setColor("#654321");
        projectC.setName("ProjTest2");
        projectC.setStatus(EProjectStatus.OPEN.toString());
        doPUTAtURL(getProjectUrl(projectC), projectC);
        ILProject projectCupdated = doGetObject(getProjectUrl(projectC));
        assertEquals("#654321", projectCupdated.getColor());
        assertEquals("ProjTest2", projectCupdated.getName());
        assertEquals(EProjectStatus.OPEN.toString(), projectCupdated.getStatus());
    }

    @Test
    public void testProjectPUT2() throws IOException
    {
        // create the projects
        WebResponse response = createProject(getTestInstance1());
        assertEquals(CREATION_OK, response.getText());

        // read it back
        ILProject project = getProject(getTestInstance1().getName());
        // update it and check
        ILProject updateProject = updateProject(project, getTestInstance2());
        assertEquals(getTestInstance2().getColor(), updateProject.getColor());
        assertEquals(getTestInstance2().getName(), updateProject.getName());
        assertEquals(getTestInstance2().getStatus(), updateProject.getStatus());

        /* update with an existing name, must not be possible */
        response = createProject(getTestInstance1());
        assertEquals(CREATION_OK, response.getText());
        ILProject project1 = getProject(getTestInstance1().getName());
        project1.setName(project.getName());
        response = doPUTAtURL(getProjectUrl(project1), project1);
        assertEquals(400, response.getResponseCode());

    }

    @Test
    public void testProjectLike() throws IOException
    {
        String constraint = getLikeConstraint("name", "%B");
        Collection<ILProject> projects = doGetObject(SERVICE + PROJECT + constraint);
        assertEquals(1, projects.size());
        ILProject project = projects.iterator().next();
        checkProjectB(project);
        constraint = getLikeConstraint("color", "#0000%");
        projects = doGetObject(SERVICE + PROJECT + constraint);
        assertEquals(1, projects.size());
        project = projects.iterator().next();
        checkProjectB(project);
    }

    @Test
    public void testProjectLike2() throws IOException
    {
        String constraint = getLikeConstraint("name", "%B");
        Collection<ILProject> projects = doGetObject(SERVICE + PROJECT + constraint);
        assertEquals(1, projects.size());
        ILProject project = projects.iterator().next();
        checkProjectB(project);

        constraint = getLikeConstraint("color", "#0000%");
        projects = doGetObject(SERVICE + PROJECT + constraint);
        assertEquals(1, projects.size());
        project = projects.iterator().next();
        checkProjectB(project);
    }

    /**
     * @param project
     */
    private void checkProjectB(ILProject project)
    {

        assertEquals("#0000FF", project.getColor());
        assertEquals(1, project.getResourceGroupNames().size());
        // assertEquals("Guillaume Chatelet", project.getResourceGroupNames().get(0));
        assertEquals("ProjectB", project.getName());
        assertEquals(EProjectStatus.OPEN.toString(), project.getStatus());
        // Note: not reliable to check the version since any operation on
        // project in InitializeDbResource will increment the version
        // assertEquals(new Long(1), project.getVersion());
    }

    // @Test
    // public void testProjectTags() throws IOException
    // {
    // // init
    // doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=Tags");
    //
    // ILProject projectA = getProjectA();
    // final int projectAtagsNbs = projectA.getTags().size();
    // assertEquals(2, projectAtagsNbs);
    // // add a tag
    // List<LTagParentIdType> tagparents = new ArrayList<LTagParentIdType>();
    // tagparents.add(new LTagParentIdType(projectA.getId(), projectA.getClass().getCanonicalName()));
    // ILTag tag = getOrCreateTag("MY TAG");
    // projectA.addTag(tag.getId());
    // doPUTAtURL(getProjectUrl(projectA), projectA);
    // // read back
    // projectA = getProjectA();
    // assertEquals(projectAtagsNbs + 1, projectA.getTags().size());
    // }

    @Test
    public void testProjectTags2() throws IOException
    {
        // create the project
        WebResponse response = createProject(getTestInstance1());
        assertEquals(CREATION_OK, response.getText());

        // read it back
        ILProject project = getProject(getTestInstance1().getName());
        final int projectTagsNbs = project.getTags().size();

        // add a tag
        TagsServiceTest tagsServiceTest = new TagsServiceTest();
        tagsServiceTest.addTag(project, TagsServiceTest.TYPE_PROJECT, "MY TAG");
        doPUTAtURL(getProjectUrl(project), project);
        // read back
        project = getProject(getTestInstance1().getName());
        assertEquals(projectTagsNbs + 1, project.getTags().size());
    }
}
