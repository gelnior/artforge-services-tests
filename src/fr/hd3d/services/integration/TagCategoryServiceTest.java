package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.TAG_CATEGORIES;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.hd3d.model.lightweight.ILTagCategory;


/**
 * @author Try LAM
 */
public class TagCategoryServiceTest extends AbstractServiceIntegrationTest<ILTagCategory>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=TagCategories");
    }

    /**
     * Test Categories retrieved through web services (GET /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    @Test
    public void testTagCategoryGET() throws IOException
    {
        List<ILTagCategory> tagCategory = getTagCategory("MAIN TAG CATEGORY 1");
        assertNotNull(tagCategory);
        assertNotNull(tagCategory.get(0));
    }

    /**
     * Test Category MODIFICATION through web services (PUT /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    @Test
    public void testTagCategoryPUT() throws IOException
    {
        List<ILTagCategory> tagCategory = getTagCategory("MAIN TAG CATEGORY 1");

        ILTagCategory tagCat = tagCategory.get(0);

        // change TagCatefory Name
        final String newName = "MAIN TAG CATEGORY 1 UPDATED";
        tagCat.setName(newName);

        List<Long> tags = tagCat.getBoundTags();
        List<Long> newTags = new ArrayList<Long>(1);
        for (Long tagId : tags)
        {
            newTags.add(tagId);
            break;// just take one tag
        }

        tagCat.setBoundTags(newTags);

        String url = SERVICE + '/' + TAG_CATEGORIES + "/" + tagCat.getId();
        doPUTAtURL(url, tagCat);
        ILTagCategory updated = doGetObject(url);
        assertEquals(newName, updated.getName());
    }

    /**
     * Test Category CREATION through web services (POST /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    // @Test
    // public void testCategoryPOST() throws IOException
    // {
    // // get the project A
    // ILProject projectA = getProjectA();
    // String url = getProjectUrl(projectA) + CATEGORIES;
    // // retrieve the main categories (with "constituent" as parent) for the
    // // project A
    // Collection<LCategory> categories = doGetObject(url);
    // assertEquals(2, categories.size());
    // for (LCategory category : categories)
    // {
    // // retrieve the Personnage main category
    // if ("Personnage".equals(category.getName()))
    // {
    // // create a new sub-category (with "Personnage" as parent)
    // ILCategory subCategory = LCategory.getNewInstance("Sous Categorie", null, projectA.getId());
    //
    // String url1 = getProjectUrl(projectA) + CATEGORIES + "/" + category.getId() + "/"
    // + CHILDREN;
    // WebResponse response = doPostAtURL(url1, subCategory);
    // assertEquals(CREATION_OK, response.getText());
    // // retrieve the project A's newly created category and check
    // categories = doGetObject(url);
    // for (LCategory newCategorie : categories)
    // {
    // if ("Personnage".equals(newCategorie.getName()))
    // {
    // String url2 = getProjectUrl(projectA) + CATEGORIES + "/" + newCategorie.getId()
    // + "/" + CHILDREN;
    // Collection<LCategory> newCategories = doGetObject(url2);
    // // Personnage category must have one more new sub
    // // category
    // assertEquals(3, newCategories.size());
    // }
    // }
    // }
    // }
    // }
    //
    // /**
    // * Test Category DELETION through web services (DELETE /hd3dServices/project/{projectId}/categories/{categoryId}).
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoryDELETE() throws IOException
    // {
    //
    // // get the "Decor" category of the project A
    // ILCategory category = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
    // // delete it
    // String url = getProjectUrl(getProjectA()) + "/" + CATEGORIES + "/" + category.getId() + "/"
    // + CHILDREN;
    // doDELETEAtURL(url);
    // // read it back again
    // ILCategory category1 = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
    //
    // assertEquals(category, category1);
    // }
    //
    // /**
    // * Test like condition on Category name
    // *
    // * @throws IOException
    // */
    // @Test
    // public void testCategoryLike() throws IOException
    // {
    //
    // String constraint = getLikeConstraint("name", "Perso%");
    // Collection<LCategory> categories = doGetObject(getProjectUrl(getProjectA()) + CATEGORIES
    // + constraint);
    // assertEquals(1, categories.size());
    // assertTrue(categories.iterator().next().getName().startsWith("Perso"));
    // }
    public List<ILTagCategory> getTagCategory(String name)
    {
        String url;
        try
        {
            url = SERVICE
                    + '/'
                    + TAG_CATEGORIES
                    + "?constraint="
                    + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"" + "name" + "\",\"value\":\"" + name + "\"}]",
                            "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return null;
        }
        return doGetObject(url);
    }
}
