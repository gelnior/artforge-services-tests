package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.PERSONS;
import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUPS;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.impl.LPerson;


/**
 * Tests web service Person in Project : list (GET) and singleton (CRUD)
 * 
 * @author Guillaume Chatelet
 */
public class ProjectPersonServiceTest extends AbstractServiceIntegrationTest<Object>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    /**
     * Test Persons retrieved through web services (GET /hd3dServices/project/{projectId}/persons/).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonsGET() throws IOException
    {

        Collection<LPerson> persons = doGetObject(getProjectUrl(getProjectA()) + "/" + PERSON);
        assertEquals(3, persons.size());
        for (LPerson person : persons)
        {
            if ("gch".equals(person.getLogin()))
            {
                assertEquals("Guillaume", person.getFirstName());
                assertEquals("Chatelet", person.getLastName());
            }
            else if ("eth".equals(person.getLogin()))
            {
                assertEquals("Thomas", person.getFirstName());
                assertEquals("Eskénazi", person.getLastName());
            }
            else if ("acs".equals(person.getLogin()))
            {
                assertEquals("Arnaud", person.getFirstName());
                assertEquals("Chassagne", person.getLastName());
            }
            else
            {
                fail("unexpected person :" + person.getName());
            }
        }
    }

    /**
     * Test Persons retrieved through web services (GET /hd3dServices/project/{projectId}/persons/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonGET() throws IOException
    {

        ILPerson person = this.getPersonInProject("eth", this.getProjectUrl(getProjectA()));
        assertEquals("eth", person.getLogin());
        assertEquals("Thomas", person.getFirstName());
        assertEquals("Eskénazi", person.getLastName());
    }

    /**
     * Test Person creation through web services (POST /hd3dServices/project/{projectId}/persons/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonsPOST() throws IOException
    {
        ILProject projectA = getProjectA();
        ILPerson person = LPerson.getNewInstance("jdoe", "Jane", "Doe", null, null, null, 0L);

        // create person
        String personUrl = SERVICE + '/' + PERSONS;
        WebResponse response = doPostAtURL(personUrl, person);
        assertEquals(CREATION_OK, response.getText());
        ILPerson createdPerson = getPerson("jdoe");

        // attach to a resource group
        final ILResourceGroup group = (ILResourceGroup) getResourceGroup("Test_group_00");
        group.getResourceIDs().add(createdPerson.getId());
        String resourceGroupUrl = SERVICE + '/' + RESOURCEGROUPS;
        response = doPUTAtURL(resourceGroupUrl + "/" + group.getId(), group);

        // read it back: is he properly attached to the project ?
        createdPerson = this.getPersonInProject("jdoe", this.getProjectUrl(getProjectA()));
        assertEquals(person.getLogin(), createdPerson.getLogin());
        assertEquals(person.getFirstName(), createdPerson.getFirstName());
        assertEquals(person.getLastName(), createdPerson.getLastName());
    }

    /**
     * Test Person deletion through web services (DELETE /hd3dServices/project/{projectId}/persons/{personId}).
     * 
     * @throws IOException
     */
    // @Test
    // public void testPersonDELETE() throws IOException
    // {
    //
    // // WebResponse response = doDELETEAtURL(getProjectUrl(getProjectA()) + "/" + PERSON + "eth");
    // // TODO log
    //
    // // TODO Comprendre pourquoi Tomcat renvoie des accents non UTF-8
    //
    // // assertTrue(response.getResponseMessage().contentEquals(
    // // "Méthode Non Autorisée")
    // // || response.getResponseMessage().contentEquals(
    // // "Method Not Allowed"));
    // }
    /**
     * Test Person modification through web services (PUT /hd3dServices/project/{projectId}/person/{personId}).
     * 
     * @throws IOException
     */
    @Test
    public void testPersonPUT() throws IOException
    {

        ILProject projectA = getProjectA();
        ILPerson person = this.getPersonInProject("eth", this.getProjectUrl(getProjectA()));
        person.setFirstName("Bob");
        person.setLastName("Maurane");
        doPUTAtURL(getProjectUrl(projectA) + "/" + PERSON + person.getId(), person);

        ILPerson createdPerson = this.getPersonInProject("eth", this.getProjectUrl(getProjectA()));

        assertEquals("Bob", createdPerson.getFirstName());
        assertEquals("Maurane", createdPerson.getLastName());

    }

    /**
     * Test like condition on login, FirstName and LastName attributes.
     * 
     * @throws IOException
     */
    @Test
    public void testPersonLike() throws IOException
    {

        String constraint = getLikeConstraint("login", "%h");
        Collection<LPerson> persons = doGetObject(getProjectUrl(getProjectA()) + "/" + PERSON + constraint);
        assertEquals(2, persons.size());
        for (LPerson person : persons)
        {
            if ("gch".equals(person.getLogin()))
            {
                assertEquals("Guillaume", person.getFirstName());
                assertEquals("Chatelet", person.getLastName());
            }
            else if ("eth".equals(person.getLogin()))
            {
                assertEquals("Thomas", person.getFirstName());
                assertEquals("Eskénazi", person.getLastName());
            }
            else
            {
                fail("unexpected person :" + person.getName());
            }
        }
        // Testint lastName column
        constraint = getLikeConstraint("lastName", "%t");
        persons = doGetObject(getProjectUrl(getProjectA()) + "/" + PERSON + constraint);
        assertEquals(1, persons.size());
        ILPerson person = persons.iterator().next();
        assertEquals("gch", person.getLogin());
        assertEquals("Guillaume", person.getFirstName());
        assertEquals("Chatelet", person.getLastName());
        // Testint firstName column
        constraint = getLikeConstraint("firstName", "%d");
        persons = doGetObject(getProjectUrl(getProjectA()) + "/" + PERSON + constraint);
        assertEquals(1, persons.size());
        person = persons.iterator().next();
        assertEquals("acs", person.getLogin());
        assertEquals("Arnaud", person.getFirstName());
        assertEquals("Chassagne", person.getLastName());

    }
}
