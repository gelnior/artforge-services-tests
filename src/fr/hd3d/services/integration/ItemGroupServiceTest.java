package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.ITEMGROUPS;
import static fr.hd3d.common.client.ServicesURI.SHEETS;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.meterware.httpunit.PutMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.Const;
import fr.hd3d.model.lightweight.ILConstituent;
import fr.hd3d.model.lightweight.ILItem;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;
import fr.hd3d.model.lightweight.impl.LItemGroup;
import fr.hd3d.model.lightweight.impl.LSheet;
import fr.hd3d.services.resources.ItemGroupResource;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.services.translator.json.JSonFormSerializer;


/**
 * @author Try LAM
 */
public class ItemGroupServiceTest extends AbstractServiceIntegrationTest<ILItem>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=DynMetaData");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=Sheets");
    }

    /**
     * Test ItemGroup retrieved through web services (GET
     * /hd3dServices/project/{projectId}/sheets/{sheetId}/itemGroups/{itemGroupId}?objectId={boundClassObjectId}).
     * 
     * @throws IOException
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testItemGroupGETwithObjectId() throws IOException
    {
        // get the constituent (bound class) filtered attributes by ItemGroup's items
        ILConstituent constituent = getConstituent();
        String url = getItemGroupUrl() + "?objectId=" + constituent.getId();
        List response = doGetObject(url);
        assertEquals(3, response.size());
    }

    /**
     * Test ICosntituent partial update via ItemGroup through web services (PUT
     * /hd3dServices/project/{projectId}/sheets/{sheetId}/itemGroups/{itemGroupId}?objectId={boundClassObjectId}).
     * 
     * @throws IOException
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testItemGroupPUTwithObjectId() throws IOException
    {
        // get the constituent (bound class) filtered attributes by ItemGroup's items
        ILConstituent constituent = getConstituent();
        String url = getItemGroupUrl() + "?objectId=" + constituent.getId();
        List<Map<String, Object>> updated = new ArrayList<Map<String, Object>>();
        {
            // update the "label" attribute
            Map<String, Object> updatedLabelMap = new HashMap<String, Object>();
            updatedLabelMap.put(ItemGroupResource.NAME, "Label");
            updatedLabelMap.put(ItemGroupResource.VALUE, "TOTO label");
            // updatedLabelMap.put("controlContent", "TOTO controlContent");
            // updatedLabelMap.put("metaType", "Short_Text");
            updated.add(updatedLabelMap);
        }
        {// update the "difficulty" attribute
            Map<String, Object> updatedDifficultyMap = new HashMap<String, Object>();
            updatedDifficultyMap.put(ItemGroupResource.NAME, "Complexity");
            updatedDifficultyMap.put(ItemGroupResource.VALUE, 9999L);
            // updatedDifficultyMap.put("controlContent", "TOTO controlContent");
            // updatedDifficultyMap.put("metaType", "Boolean");
            updated.add(updatedDifficultyMap);
        }
        // update it
        WebResponse resp = doPUTMapAtURL(url, updated);
        assertEquals("OK", resp.getResponseMessage());
        // test response back
        int passed = 0;
        List<Map<String, Object>> response = (List<Map<String, Object>>) doGetObject(url);
        for (Map<String, Object> m : response)
        {
            if ("Label".equals((String) m.get(ItemGroupResource.NAME)))
            {
                assertEquals("TOTO label", m.get(ItemGroupResource.VALUE));
                passed++;
            }
            if ("Complexity".equals((String) m.get(ItemGroupResource.NAME)))
            {
                assertEquals(9999L, m.get(ItemGroupResource.VALUE));
                passed++;
            }
        }
        assertEquals(2, passed);
    }

    // convenience methods
    public ILConstituent getConstituent() throws IOException
    {
        // get all the constituents of Personage category of project A
        String url = getProjectAPersonnageCategoryUrl() + "/constituents";
        Collection<ILConstituent> constituents = doGetObject(url);
        for (ILConstituent c : constituents)
        {
            if (c.getLabel().equals("const 1 label"))
            {
                url += "/" + c.getId();
                return doGetObject(url);
            }
        }
        return null;
    }

    public String getProjectAPersonnageCategoryUrl() throws IOException
    {
        String ret = null;
        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {
            if ("Personnage".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
                // get the sub categories
                url = url + "/" + category.getId();
                ret = url;
                break;
            }
        }
        return ret;
    }

    public String getItemGroupUrl() throws IOException
    {
        String url = SERVICE + '/' + SHEETS;
        Collection<LSheet> sheets = doGetObject(url);
        Long mySheet1Id = Const.ID_NULL;
        for (LSheet s : sheets)
        {
            if ("My Sheet 1".equals(s.getName()))
            {
                mySheet1Id = s.getId();
                break;
            }
        }
        // get the itemGroups of this sheet
        url += "/" + mySheet1Id + '/' + ITEMGROUPS;
        List<LItemGroup> itemGroups = doGetObject(url);
        Long itemGroupId = Const.ID_NULL;
        for (LItemGroup g : itemGroups)
        {
            if ("Portlet 1".equals(g.getName()))
            {
                itemGroupId = g.getId();
                break;
            }
        }
        // get the item group
        url += "/" + itemGroupId;
        return url;
    }

    private WebResponse doPUTMapAtURL(String url, List<Map<String, Object>> objectToSerialize) throws IOException
    {
        WebConversation conversation = new WebConversation();
        addAuthentication(conversation);
        String serializedObj = (String) Hd3dJsonSerializer.serialize(objectToSerialize);
        // yeah, I know, StringBufferInputStream is @deprecated, but I had no
        // other choice here...
        InputStream is = new ByteArrayInputStream(serializedObj.getBytes("UTF-8"));
        // InputStream is = new StringBufferInputStream(JSonFormSerializer.OBJECT + '=' + serializedObj);
        WebRequest request = new PutMethodWebRequest(url, is, JSON_HEADER_VALUE);
        request.setParameter(JSonFormSerializer.OBJECT, serializedObj);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }
}
