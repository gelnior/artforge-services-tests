package fr.hd3d.services.integration;

import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.CHILDREN;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LCategory;


/**
 * @author Try LAM
 */
public class CategoryServiceTest extends AbstractServiceIntegrationTest<ILCategory>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=CategoriesConstituents");
    }

    /**
     * Test Categories retrieved through web services (GET /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoryGET() throws IOException
    {

        ILProject projectA = getProjectA();
        String url = getProjectUrl(projectA) + '/' + CATEGORIES;
        Collection<LCategory> categories = doGetObject(url);
        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {
            if ("Personnage".equals(category.getName()))
            {
                assertEquals(projectA.getId(), category.getProject());
                // get the subcategories
                url = url + "/" + category.getId() + "/children";
                Collection<LCategory> subCategories = doGetObject(url);
                // Personnage is supposed to have 2 sub-categories
                assertEquals(2, subCategories.size());
            }
            // TODO check other categories
        }
    }

    /**
     * Test Category MODIFICATION through web services (PUT /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoryPUT() throws IOException
    {

        ILProject projectA = getProjectA();
        // ILProject projectB = getProjectB();
        ILCategory category = this.getCategoryInProject("Personnage", getProjectUrl(projectA));

        category.setName("UPDATED Personnage");
        // category.setProject(projectB.getId());
        String url = getProjectUrl(projectA) + '/' + CATEGORIES + "/" + category.getId();
        doPUTAtURL(url, category);
        ILCategory updatedCategory = this.getCategoryInProject("UPDATED Personnage", getProjectUrl(projectA));
        assertEquals("UPDATED Personnage", updatedCategory.getName());
    }

    /**
     * Test Category CREATION through web services (POST /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoryPOST() throws IOException
    {
        ILProject projectA = getProjectA();
        String categoriesUrl = getProjectUrl(projectA) + '/' + CATEGORIES;

        ILCategory category = getProjectAPersonnageCategory();

        if (category != null)
        {
            // create a new sub-category (with "Personnage" as parent)
            createSubCategory(projectA, category);

            // retrieve the project A's newly created category and check
            category = getProjectAPersonnageCategory();

            Collection<LCategory> categories = doGetObject(categoriesUrl);
            for (LCategory newCategorie : categories)
            {
                if ("Personnage".equals(newCategorie.getName()))
                {
                    String url2 = getProjectUrl(projectA) + '/' + CATEGORIES + "/" + newCategorie.getId() + "/"
                            + CHILDREN;
                    Collection<LCategory> newCategories = doGetObject(url2);
                    // Personnage category must have one more new sub
                    // category
                    assertEquals(3, newCategories.size());
                }
            }
        }

        createSimilarCategory(projectA, category);
    }

    private ILCategory getProjectAPersonnageCategory() throws IOException
    {
        ILCategory ret = null;

        ILProject projectA = getProjectA();

        String categoriesUrl = getProjectUrl(projectA) + '/' + CATEGORIES;

        // retrieve the main categories (with "constituent" as parent) for the
        // project A
        Collection<LCategory> categories = doGetObject(categoriesUrl);
        assertEquals(2, categories.size());
        for (LCategory category : categories)
        {
            // retrieve the Personnage main category
            if ("Personnage".equals(category.getName()))
            {
                ret = category;
            }
        }

        return ret;
    }

    private void createSubCategory(ILProject project, ILCategory category) throws IOException
    {
        // create a new sub-category (with "Personnage" as parent)
        ILCategory subCategory = LCategory.getNewInstance("Sous Categorie", null, project.getId());

        String url1 = getProjectUrl(project) + '/' + CATEGORIES + "/" + category.getId() + "/" + CHILDREN;
        WebResponse response = doPostAtURL(url1, subCategory);
        assertEquals(CREATION_OK, response.getText());
    }

    private void createSimilarCategory(ILProject project, ILCategory category) throws IOException
    {
        ILCategory similarCategory = LCategory.getNewInstance(category.getName(), category.getParent(), category
                .getProject());
        WebResponse response = doPostAtURL(getProjectUrl(project) + '/' + CATEGORIES, similarCategory);
        assertEquals(400, response.getResponseCode());
    }

    /**
     * Test Category DELETION through web services (DELETE /hd3dServices/project/{projectId}/categories/{categoryId}).
     * 
     * @throws IOException
     */
    @Test
    public void testCategoryDELETE() throws IOException
    {

        // get the "Decor" category of the project A
        ILCategory category = getCategoryInProject("Decor", getProjectUrl(getProjectA()));
        // delete it
        String url = getProjectUrl(getProjectA()) + "/" + CATEGORIES + "/" + category.getId() + "/" + CHILDREN;
        doDELETEAtURL(url);
        // read it back again
        ILCategory category1 = getCategoryInProject("Decor", getProjectUrl(getProjectA()));

        assertEquals(category, category1);
    }

    /**
     * Test like condition on Category name
     * 
     * @throws IOException
     */
    @Test
    public void testCategoryLike() throws IOException
    {

        String constraint = getLikeConstraint("name", "Perso%");
        Collection<LCategory> categories = doGetObject(getProjectUrl(getProjectA()) + '/' + CATEGORIES + constraint);
        assertEquals(1, categories.size());
        assertTrue(categories.iterator().next().getName().startsWith("Perso"));
    }

}
