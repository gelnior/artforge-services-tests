package fr.hd3d.services.integration;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.model.lightweight.impl.LTask;


/**
 * Web service tests of Person / Task : list (GET) and singleton (CRUD)
 * 
 * @author Guillaume Chatelet
 */
public class PersonTaskServiceTest extends AbstractServiceIntegrationTest<ILTask>
{
    protected void initDB() throws IOException
    {
        doTextGetAtURL(SERVICE + TEST_INIT_BD + "?cmd=ProjectsTasksActivities");
    }

    private static final String ANOTHER_SUPER_TASK = "Another super task";
    private static final String ONE_SUPER_TASK = "One super task";

    /**
     * Test Tasks retrieved through web services (GET /hd3dServices/persons/{personId}/tasks/).
     * 
     * @throws IOException
     */
    @Test
    public void testGetTasks() throws Exception
    {

        // Get project and persons for task attributes validation.
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final ILPerson acs = this.getPerson("acs");
        final ILPerson eth = this.getPerson("eth");

        // Get tasks
        final Collection<ILTask> tasks = doGetObject(SERVICE + "/persons/" + gch.getId() + "/tasks");
        assertNotNull(tasks);
        assertEquals(1, tasks.size());

        for (ILTask task : tasks)
        {

            // Validate project associated to the task
            assertEquals(projectA.getName(), task.getProjectName());
            assertEquals(projectA.getId(), task.getProjectID());
            assertEquals(projectA.getColor(), task.getProjectColor());

            // Validate task creator
            if (ONE_SUPER_TASK.equals(task.getName()))
            {
                assertEquals(acs.getName(), task.getCreatorName());
                assertEquals(acs.getId(), task.getCreatorID());
            }
            else if (ANOTHER_SUPER_TASK.equals(task.getName()))
            {
                assertEquals(eth.getName(), task.getCreatorName());
                assertEquals(eth.getId(), task.getCreatorID());
            }

            // Validate task associated to the person
            assertEquals(gch.getName(), task.getWorkerName());
            assertEquals(gch.getId(), task.getWorkerID());

            // Validate other attributes
            assertEquals((Byte) (byte) 0, task.getCompletion());
            assertNull(task.getCompletionDate());
            assertEquals(ETaskStatus.OK.toString(), task.getStatus());
        }
    }

    /**
     * Test Task retrieved through web services (GET /hd3dServices/persons/{personId}/tasks/{taskId}).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskGET() throws IOException
    {

        final ILProject projectA = getProjectA();
        final ILTask task = this.getTaskInProject("One super task", this.getProjectUrl(projectA));
        final ILPerson gch = this.getPerson("gch");

        final ILTask taskUrl = this.doGetObject(this.getPersonUrl(gch) + "/tasks/" + task.getId());

        // Validate task associated to the person
        assertEquals(gch.getName(), taskUrl.getWorkerName());
        assertEquals(gch.getId(), taskUrl.getWorkerID());

        // Validate other attributes
        assertEquals((Byte) (byte) 0, taskUrl.getCompletion());
        assertNull(taskUrl.getCompletionDate());
        assertEquals(ETaskStatus.OK.toString(), taskUrl.getStatus());
    }

    /**
     * Validate the fact that other person tasks cannot be accessed through web services (GET
     * /hd3dServices/persons/{personId}/tasks/{taskId}).
     * 
     * @throws IOException
     */
    @Test
    public void testWrongPersonTaskGET() throws IOException
    {

        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final ILTask otherTask = this.getTaskInProject("Test_task_00", this.getProjectUrl(projectA));
        final ILTask otherTaskUrl = this.doGetObject(this.getPersonUrl(gch) + "/tasks/" + otherTask.getId());

        assertNull(otherTaskUrl);
    }

    /**
     * Test Task creation through web services (POST /hd3dServices/persons/{personId}/tasks/{taskId}/).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskPOST() throws IOException
    {

        LTask task = new LTask();
        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String url = this.getPersonUrl(gch) + "/tasks/";

        // Set task attributes.
        task.setName("Test_task_POST");
        task.setCompletion((Byte) (byte) 0);
        task.setCreator(gch);
        task.setCreatorID(gch.getId());
        task.setProject(projectA);
        task.setProjectID(projectA.getId());
        task.setStatus(ETaskStatus.OK.toString());

        // Validate POST
        WebResponse response = doPostAtURL(url, task);
        assertEquals(CREATION_OK, response.getText());
    }

    /**
     * Test Group deletion through web services (DELETE /hd3dServices/persons/{personId}/tasks/{taskId}/).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskDELETE() throws IOException
    {

        final ILProject projectA = this.getProjectA();
        final ILPerson gch = this.getPerson("gch");
        final String url = this.getPersonUrl(gch) + "/tasks/";
        final String projectUrl = this.getProjectUrl(projectA);

        final ILTask task = this.getTaskInProject("Test_Task_00", projectUrl);

        // Delete task.
        doDELETEAtURL(url + task.getId());

        // Ensure that group does not exist anymore.
        assertNull(doGetObject(url + task.getId()));
    }

    /**
     * Test task update through web services (PUT /hd3dServices/persons/{personId}/tasks/{taskId}/).
     * 
     * @throws IOException
     */
    @Test
    public void testTaskPUT() throws IOException
    {

        final ILProject projectA = this.getProjectA();
        final String projectUrl = this.getProjectUrl(projectA);
        final ILPerson eth = this.getPerson("eth");

        // Retrieve test task
        final ILTask task = this.getTaskInProject("Test_Task_00", projectUrl);
        final String url = this.getPersonUrl(eth) + "/tasks/" + task.getId();

        // Update test task
        task.setName("Test_task_UPDATE");
        doPUTAtURL(url, task);

        final ILTask taskUpdated = this.doGetObject(url);
        assertEquals("Test_task_UPDATE", taskUpdated.getName());
    }

    /**
     * Test whether a task update through web services work if Project ID or Person ID is not valid. (PUT
     * /hd3dServices/persons/{personId}/tasks/{taskId}/).
     * 
     * @throws IOException
     */
    @Test
    public void testWrongTaskPUT() throws IOException
    {

        final ILProject projectA = this.getProjectA();
        final String projectUrl = this.getProjectUrl(projectA);
        final ILPerson eth = this.getPerson("eth");

        // Retrieve test task
        final ILTask task = this.getTaskInProject("Test_Task_00", projectUrl);
        final String url = this.getPersonUrl(eth) + "/tasks/" + task.getId();

        // Update test task
        task.setName("Test_task_UPDATE");
        task.setProjectID(99999L);
        doPUTAtURL(url, task);

        final ILTask taskUpdated = this.doGetObject(url);
        assertNotNull(taskUpdated);
        // assertEquals(new Long(99999), task.getProjectID());

        // TODO : Tests ARCHI-141
    }

    /**
     * Test Like constraint through web services (GET /hd3dServices/groups).
     * 
     * @throws Exception
     */
    @Test
    public void testTasksLike() throws Exception
    {

        final ILProject projectA = getProjectA();
        final ILPerson gch = this.getPerson("gch");

        // Get tasks with constraint.
        String constraint = getLikeConstraint("name", "One%");
        final Collection<ILTask> tasks = doGetObject(this.getPersonUrl(gch) + "/tasks/" + constraint);

        // Validate tasks retrieved.
        assertEquals(1, tasks.size());
        ILTask task = tasks.iterator().next();

        // Validate Task Project
        assertEquals(projectA.getName(), task.getProjectName());
        assertEquals(projectA.getId(), task.getProjectID());
        assertEquals(projectA.getColor(), task.getProjectColor());

        // Validate task attribute
        assertEquals(ONE_SUPER_TASK, task.getName());
        assertEquals((Byte) (byte) 0, task.getCompletion());
        assertNull(task.getCompletionDate());
        assertEquals(ETaskStatus.OK.toString(), task.getStatus());
    }

}
