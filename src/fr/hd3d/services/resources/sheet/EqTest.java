package fr.hd3d.services.resources.sheet;

import org.junit.Assert;
import org.junit.Test;

import fr.hd3d.exception.Hd3dException;


/**
 * 
 * @author try.lam
 */
public class EqTest
{
    @Test
    public void testEquality()
    {
        try
        {
            IOneOperandTypeHandler eqString = new Eq.Eq_String();
            Assert.assertTrue(eqString.match(null, null, null));
            Assert.assertTrue(eqString.match("toto", "toto", null));
            Assert.assertFalse(eqString.match(null, "toto", null));
            Assert.assertFalse(eqString.match("toto", null, null));
            Assert.assertFalse(eqString.match("toto", "tata", null));

            IOneOperandTypeHandler eqLong = new Eq.Eq_Long();
            Assert.assertTrue(eqLong.match(null, null, null));
            Assert.assertTrue(eqLong.match(1L, 1L, null));
            Assert.assertFalse(eqLong.match(null, 1L, null));
            Assert.assertFalse(eqLong.match(1L, null, null));

        }
        catch (Hd3dException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
