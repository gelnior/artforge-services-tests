/**
 * 
 */
package fr.hd3d.services;

import static fr.hd3d.common.client.ServicesURI.ACTIVITIES;
import static fr.hd3d.common.client.ServicesURI.CATEGORIES;
import static fr.hd3d.common.client.ServicesURI.CLEANUP;
import static fr.hd3d.common.client.ServicesURI.ERASE_DB;
import static fr.hd3d.common.client.ServicesURI.INIT_DB;
import static fr.hd3d.common.client.ServicesURI.PERSONS;
import static fr.hd3d.common.client.ServicesURI.PROJECTS;
import static fr.hd3d.common.client.ServicesURI.RESOURCEGROUPS;
import static fr.hd3d.common.client.ServicesURI.SEQUENCES;
import static fr.hd3d.common.client.ServicesURI.TASKS;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Properties;

import junit.framework.TestCase;

import com.meterware.httpunit.ClientProperties;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.exception.Hd3dRuntimeException;
import fr.hd3d.model.lightweight.ILCategory;
import fr.hd3d.model.lightweight.ILPerson;
import fr.hd3d.model.lightweight.ILPersonDay;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.ILResourceGroup;
import fr.hd3d.model.lightweight.ILSequence;
import fr.hd3d.model.lightweight.ILTask;
import fr.hd3d.services.translator.json.Hd3dJsonSerializer;
import fr.hd3d.utils.Bench;
import fr.hd3d.utils.Conf;


/**
 * @author Thomas Eskénazi
 * 
 */
public abstract class AbstractServiceTest extends TestCase
{
    public final static String CONF_FILE = "test.properties";
    public final static Properties props = new Properties();
    public static String ROOT_SERVER = "http://localhost:8080/";
    protected static String ROOT_SERVICE = ROOT_SERVER + "Hd3dServices/";
    protected static String SERVICE = ROOT_SERVER + "Hd3dServices/v1/";

    protected static final String TEXT_HEADER_VALUE = "text/plain";
    protected static final String ACCEPT_HEADER = "Accept";
    protected static final String JSON_HEADER_VALUE = "application/json; charset=UTF-8";

    protected static final String TEST_INIT_BD = INIT_DB;
    protected static final String TEST_ERASE_DB = ERASE_DB;
    protected static final String TEST_CLEANUP = CLEANUP;
    // protected static final String PROJECT = StringUtils.substringAfter('/' + PROJECTS, "/") + '/';
    protected static final String PROJECT = PROJECTS + '/';
    protected static final String PERSON = PERSONS + '/';
    protected static final String RESOURCE_GROUP = RESOURCEGROUPS + '/';
    protected static final String TASK = TASKS + '/';
    protected static final String ACTIVITY = ACTIVITIES + '/';
    protected static final String DAYS_AND_ACTIVITIES = ServicesURI.DAYS_AND_ACTIVITIES + '/';
    protected static final String TASK_TYPE = ServicesURI.TASK_TYPES + '/';

    protected static final Hd3dJsonSerializer SERIALIZER = new Hd3dJsonSerializer();

    // Authentication
    protected static String root_user;
    protected static String root_pass;

    protected Bench testDuration;

    static
    {
        HttpUnitOptions.setDefaultCharacterSet("UTF-8");
        HttpUnitOptions.setDefaultContentType(JSON_HEADER_VALUE);
        ClientProperties.getDefaultProperties().setAcceptGzip(true);// NOTE: EOFException still remains in spite of this
        // override default server and port
        InputStream stream = null;
        stream = readProperties(CONF_FILE);
        if (stream == null)
            stream = readProperties("/" + CONF_FILE);

        if (stream == null)
            throw new Hd3dRuntimeException(CONF_FILE + " not found");

        try
        {
            props.load(stream);
        }
        catch (IOException exception)
        {
            // TODO Log
            exception.printStackTrace();
        }
        finally
        {
            try
            {
                if (stream != null)
                    stream.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        String servicesServer = props.getProperty("hd3d.servicesServer");
        String rootService = props.getProperty("hd3d.rootService");
        String service = props.getProperty("hd3d.service");
        if (servicesServer != null && !"".equals(servicesServer))
        {
            ROOT_SERVER = servicesServer;
            ROOT_SERVICE = ROOT_SERVER + rootService;
            SERVICE = ROOT_SERVER + service;
        }
        System.out.println("ROOT_SERVER=" + ROOT_SERVER);
        System.out.println("ROOT_SERVICE=" + ROOT_SERVICE);
        System.out.println("SERVICE=" + SERVICE);

        root_user = props.getProperty("root.username");
        root_pass = props.getProperty("root.password");
    }

    public static InputStream readProperties(String path)
    {
        // String stripped = path.startsWith("/") ? path.substring(1) : path;
        InputStream stream = null;
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader != null)
            stream = classLoader.getResourceAsStream(path);
        if (stream == null)
            stream = Conf.class.getResourceAsStream(path);
        if (stream == null)
            stream = Conf.class.getClassLoader().getResourceAsStream(path);
        return stream;
    }

    protected abstract void initDB() throws IOException;

    @Override
    protected void setUp() throws Exception
    {
        System.out.println(">>>>>>>>>>>>>>>>>> TEST INIT START <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        Bench b = new Bench();
        b.b();
        doTextGetAtURL(SERVICE + TEST_ERASE_DB);
        doTextGetAtURL(SERVICE + TEST_CLEANUP);
        initDB();
        super.setUp();
        System.out.println("TOOK:" + b.e() + " ms");
        System.out.println("----------------- TEST INIT END -------------------------------------------------");
        // start "real" test counter
        testDuration = new Bench();
        testDuration.b();
    }

    public WebResponse doTextGetAtURL(String uRL) throws IOException
    {
        return doGetAtURL(uRL, TEXT_HEADER_VALUE);
    }

    protected static WebResponse doGetAtURL(String uRL, String headerValue) throws IOException
    {
        WebConversation conversation = new WebConversation();
        addAuthentication(conversation);
        WebRequest request = new GetMethodWebRequest(uRL);
        request.setHeaderField(ACCEPT_HEADER, headerValue);
        WebResponse response = conversation.getResource(request);
        assertNotNull("Response NULL", response);
        return response;
    }

    @Override
    protected void tearDown() throws Exception
    {
        // "real" test duration
        System.out.println("TEST DURATION = " + testDuration.e() + " ms");
        //
        System.out.println("----------------- TEAR DOWN START -----------------------------------------------");
        Bench b = new Bench();
        b.b();
        doTextGetAtURL(SERVICE + TEST_ERASE_DB);
        doTextGetAtURL(SERVICE + TEST_CLEANUP);
        super.tearDown();
        System.out.println("TOOK:" + b.e() + " ms");
        System.out.println(">>>>>>>>>>>>>>>>>> TEAR DOWN END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    /**
     * @param conversation
     */
    @SuppressWarnings("deprecation")
    protected static void addAuthentication(WebConversation conversation)
    {
        // conversation.putCookie("hd3d_A34_fdt_cookie", "eth:knVVIOAIWd4OTSDpz7BIqx3/3OI");
        // conversation.addCookie("hd3d_A34_fdt_cookie", "eth:knVVIOAIWd4OTSDpz7BIqx3/3OI");
        conversation.setAuthorization(root_user, root_pass);
    }

    /**
     * Templatizing this function allow clients to autocast the result thus<BR>
     * <CODE>Collection<ILPerson> list = doGetObjectAtURL(...);</CODE><BR>
     * will not complain about missing cast.
     */
    @SuppressWarnings("unchecked")
    protected static final <E> E doGetObject(String url)
    {
        try
        {
            return (E) Hd3dJsonSerializer.unserialize(doJsonGetAtURL(url).getText());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Templatizing this function allow clients to autocast the result thus<BR>
     * <CODE>Collection<ILPerson> list = doGetObjectAtURL(...);</CODE><BR>
     * will not complain about missing cast.
     */
    @SuppressWarnings("unchecked")
    protected final <E> E doGetObject(WebResponse response) throws IOException
    {
        return (E) Hd3dJsonSerializer.unserialize(response.getText());
    }

    protected static WebResponse doJsonGetAtURL(String uRL) throws IOException
    {
        return doGetAtURL(uRL, JSON_HEADER_VALUE);
    }

    protected static ILProject getProject(String projectName) throws IOException
    {
        String constraint = "?tags=true&constraint="
                + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + projectName + "\"}]",
                        "UTF-8");
        final Collection<ILProject> projects = doGetObject(SERVICE + PROJECT + constraint);
        if (projects == null || projects.size() == 0)
            return null;
        else
            return projects.iterator().next();
    }

    protected ILProject getProject(String projectName, String extraQuery) throws IOException
    {
        String constraint = "?tags=true&constraint="
                + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + projectName + "\"}]",
                        "UTF-8");
        final Collection<ILProject> projects = doGetObject(SERVICE + PROJECT + constraint + extraQuery);
        if (projects == null || projects.size() == 0)
            return null;
        else
            return projects.iterator().next();
    }

    /**
     * Retrieve Group from its name via web service with EQUAL constraint.
     * 
     * @param groupName
     *            Group name
     * @return The group whose name is <i>groupName</i>
     * @throws IOException
     */
    protected ILResourceGroup getResourceGroup(String groupName) throws IOException
    {
        try
        {
            String constraint = "?constraint="
                    + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + groupName + "\"}]",
                            "UTF-8");

            final Collection<ILResourceGroup> groups = doGetObject(SERVICE + RESOURCE_GROUP + constraint);

            assertNotNull(groups);
            // assertTrue(groups.size() == 1);
            return groups.iterator().next();
        }
        catch (Exception e)
        {
            return null;
        }

    }

    /**
     * Retrieve Task from its name via web service with EQUAL constraint.
     * 
     * @param taskName
     *            Task name
     * @return The task whose name is <i>taskName</i>
     * @throws IOException
     */
    protected ILTask getTaskInProject(String taskName, String projectUrl) throws IOException
    {

        String constraint = "?constraint="
                + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + taskName + "\"}]", "UTF-8");

        final Collection<ILTask> tasks = doGetObject(projectUrl + "/" + TASK + constraint);

        assertNotNull(tasks);
        assertTrue(tasks.size() == 1);
        return tasks.iterator().next();

    }

    /**
     * Retrieve Person from an URL with login filter
     * 
     * @param login
     *            Person login
     * @return
     * @throws IOException
     */
    protected ILPerson getPerson(String login) throws IOException
    {
        // Set Constraint
        String constraint = "?constraint="
                + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"login\",\"value\":\"" + login + "\"}]", "UTF-8");
        // Retrieve Person
        final Collection<ILPerson> persons = doGetObject(SERVICE + PERSON + constraint);
        if (persons == null || persons.isEmpty())
            return null;
        else
        {
            assertTrue(persons.size() == 1);
            return persons.iterator().next();
        }
    }

    /**
     * Retrieve Person in a specific project from an URL with login filter
     * 
     * @param login
     *            Person login
     * @return
     * @throws IOException
     */
    protected ILPerson getPersonInProject(String login, String projectUrl) throws IOException
    {
        // Set URL with constraint
        String constraint = "?constraint="
                + URLEncoder.encode("[{\"type\":\"eq\",\"column\":\"login\",\"value\":\"" + login + "\"}]", "UTF-8");
        String personUrl = projectUrl + "/" + PERSON + constraint;
        // Retrieve Person
        final Collection<ILPerson> persons = doGetObject(personUrl);
        // Test if there is only one result
        assertTrue(persons.size() == 1);
        // Return result
        return persons.iterator().next();
    }

    protected ILPersonDay getDay(Long personId) throws IOException
    {
        // Note: date format must be "YYYY-MM-DD hh:mm:ss"
        String constraint = "?constraint=[{\"type\":\"eq\",\"column\":\"date\",\"value\":\"1970-01-01 01:00:00\"}]";
        final Collection<ILPersonDay> days = doGetObject(SERVICE + PERSON + personId + "/" + DAYS_AND_ACTIVITIES
                + constraint);
        assertTrue(days.size() > 0);
        return days.iterator().next();
    }

    /**
     * get the category defined by the given name belonging to the given project
     */
    protected ILCategory getCategoryInProject(String categoryName, String projectUrl) throws IOException
    {

        // Set URL with constraint
        String constraint = "[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + categoryName + "\"}]";
        String categoryUrl = projectUrl + "/" + CATEGORIES + "?constraint=" + URLEncoder.encode(constraint, "UTF-8");

        // Retrieve Category
        final Collection<ILCategory> categories = doGetObject(categoryUrl);

        // Note: DO NOT assume there is always a result returned.
        // Sometime, it may be used to check that an element has really been
        // deleted
        // Test if there is only one result
        // assertTrue(categories.size() == 1);

        if (categories.isEmpty())
        {
            return null;
        }

        // Return result
        return categories.iterator().next();
    }

    /**
     * get the sequence defined by the given name belonging to the given project
     */
    protected ILSequence getSequenceInProject(String sequenceName, String projectUrl) throws IOException
    {

        // Set URL with constraint
        String constraint = "[{\"type\":\"eq\",\"column\":\"name\",\"value\":\"" + sequenceName + "\"}]";
        String sequenceUrl = projectUrl + "/" + SEQUENCES + "?constraint=" + URLEncoder.encode(constraint, "UTF-8");

        // Retrieve Category
        final Collection<ILSequence> sequences = doGetObject(sequenceUrl);

        // Note: DO NOT assume there is always a result returned.
        // Sometime, it may be used to check that an element has really been
        // deleted
        // Test if there is only one result
        // assertTrue(categories.size() == 1);

        if (sequences.isEmpty())
        {
            return null;
        }

        // Return result
        return sequences.iterator().next();
    }
}
