package fr.hd3d.services.translator.json.tests;

import java.io.IOException;

import fr.hd3d.exception.Hd3dException;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public interface ITranslatorTest
{

    public void testToLightweight() throws IOException, Hd3dException;

    public void testFromLightweight() throws IOException, Hd3dException;

    public void testUpdateFromLightweight() throws IOException, Hd3dException;
}
