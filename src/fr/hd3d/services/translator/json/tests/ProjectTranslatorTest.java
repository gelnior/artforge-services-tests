/**
 * 
 */
package fr.hd3d.services.translator.json.tests;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashSet;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.model.lightweight.ILProject;
import fr.hd3d.model.lightweight.impl.LProject;
import fr.hd3d.model.persistence.IProject;
import fr.hd3d.model.persistence.IResourceGroup;
import fr.hd3d.model.persistence.impl.hibernate.ProjectH;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.model.translator.Translators;


/**
 * @author Thomas Eskenazi
 * 
 */
public class ProjectTranslatorTest extends AbstractServiceTranslatorTest<ILProject, IProject> implements
        ITranslatorTest
{

    @Override
    public IBaseTranslator<ILProject, IProject> getLocalTranslator()
    {
        return Translators.project;
    }

    /**
     * @param lightProject
     * @param baseProject
     */
    protected void checkLightAndBaseAreEquals(ILProject lightProject, IProject baseProject)
    {

        assertEquals(lightProject.getColor(), baseProject.getColor());
        assertEquals(lightProject.getName(), baseProject.getName());
        assertEquals(lightProject.getStatus(), baseProject.getStatus().toString());
        assertEquals(lightProject.getId(), baseProject.getId());
        assertEquals(lightProject.getResourceGroupIDs(), getIDListfromSet(baseProject.getResourceGroups()));
        assertEquals(lightProject.getVersion(), baseProject.getVersion());
    }

    LinkedHashSet<Long> getStaffIDs()
    {

        // final ILPerson eth = this.getPerson("eth");
        // final LinkedHashSet<Long> staffIDs = new LinkedHashSet<Long>();
        // staffIDs.add(eth.getId());
        // return staffIDs;
        return new LinkedHashSet<Long>();
    }

    private LinkedHashSet<IResourceGroup> getStaff()
    {

        // TODO get some reals IPerson.
        return new LinkedHashSet<IResourceGroup>();
    }

    public void testUpdateFromLightweight() throws IOException
    {

    // Get Light object.
    // /final ILProject lightProject = this.getLocalLightObject();

    // Get Persisted object.
    // final ITranslator<ILProject, IProject> translator = this.getLocalTranslator();

    // // Update Persisted object with light object data.
    // try {
    // ILProject projectA = this.getProject("ProjectA");
    // IProject project = translator.fromLightweight(projectA);
    // translator.updateFromLightweight(lightProject, project);
    //
    // // Validate the update
    // final IProject projectUpdated = translator.fromLightweight(this
    // .getProject("projectA"));
    // checkLightAndBaseAreEquals(lightProject, projectUpdated);
    // } catch (Exception e) {
    // fail("Translation failed");
    // }

    }

    @Override
    protected ILProject getLocalLightObject() throws IOException
    {

        ILProject lightProject = new LProject();
        lightProject.setColor("#123456");
        lightProject.setId(965L);
        lightProject.setName("UnitTesting7589");
        lightProject.setResourceGroupIDs(getStaffIDs());
        lightProject.setStatus(EProjectStatus.CLOSED.toString());
        lightProject.setVersion(null);

        return lightProject;
    }

    protected IProject getLocalBaseObject()
    {

        return new ProjectH(123L, null, "Ublug Poject", new Date(), new Date(), "#654321", EProjectStatus.CLOSED,
                getStaff(), "");
    }

    protected void initDB() throws IOException
    {
    // TODO Auto-generated method stub

    }
}
