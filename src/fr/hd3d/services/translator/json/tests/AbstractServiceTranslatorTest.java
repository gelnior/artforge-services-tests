package fr.hd3d.services.translator.json.tests;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import fr.hd3d.exception.Hd3dException;
import fr.hd3d.exception.Hd3dTranslationException;
import fr.hd3d.model.lightweight.ILBase;
import fr.hd3d.model.persistence.IBase;
import fr.hd3d.model.translator.IBaseTranslator;
import fr.hd3d.services.AbstractServiceTest;


/**
 * @author Guillaume CHATELET
 * @author Thomas ESKENAZI
 * @author Try LAM
 */
public abstract class AbstractServiceTranslatorTest<L extends ILBase, P extends IBase> extends AbstractServiceTest
{

    protected abstract L getLocalLightObject() throws IOException;

    public Long getLocalObjectID() throws IOException
    {

        return getLocalLightObject().getId();
    }

    public java.sql.Timestamp getLocalObjectVersion() throws IOException
    {

        return getLocalLightObject().getVersion();
    }

    protected abstract IBaseTranslator<L, P> getLocalTranslator();

    protected abstract void checkLightAndBaseAreEquals(L lightObject, P baseObject);

    @SuppressWarnings("unchecked")
    protected Set<Long> getIDListfromSet(Set<?> baseObjects)
    {

        LinkedHashSet<Long> result = new LinkedHashSet<Long>(baseObjects.size());
        for (Iterator<IBase> iterator = (Iterator<IBase>) baseObjects.iterator(); iterator.hasNext();)
        {
            IBase baseObject = iterator.next();
            result.add(baseObject.getId());
        }
        return result;

    }

    @Test
    public void testFromLightweight() throws IOException, Hd3dException
    {

    // L lightObject = getLocalLightObject();
    // P baseObject = getLocalTranslator().fromLightweight(lightObject, null);
    // checkLightAndBaseAreEquals(lightObject, baseObject);
    }

    @Test
    public void testToLightweight() throws IOException, Hd3dTranslationException
    {
    //
    // // Causes error because by passes application'way usual of working
    // // P baseObject = getLocalBaseObject();
    // // L lightObject = getLocalTranslator().toLightweight(baseObject, null);
    // // checkLightAndBaseAreEquals(lightObject, baseObject);
    //
    }

    protected abstract P getLocalBaseObject();

}
