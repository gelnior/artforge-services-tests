package fr.hd3d.services.translator.json.tests;

import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.Test;

import fr.hd3d.services.translator.json.JsonWrapper;

/**
 * Tests for fr.hd3d.services.translator.json.JsonWrapper.
 * 
 * @author Frank Rousseau
 */
public final class JsonWrapperTest extends TestCase {

    /**
     * Test Tests singleton
     */
    @Test
    public final void testIsSingleObject() {

        final JsonWrapper wrapper01 = new JsonWrapper("test");
        assertEquals(true, wrapper01.isSingleton());
        assertEquals(1, wrapper01.getNbRecords());
    }

    /**
     * Tests lists
     */
    @Test
    public final void testLists() {

        final ArrayList<String> list = new ArrayList<String>();
        list.add("test 01");
        list.add("test 02");

        // Null case
        final JsonWrapper wrapper01 = new JsonWrapper(null);
        assertEquals(0, wrapper01.getNbRecords());
        assertEquals(false, wrapper01.isSingleton());

        // Automatic number of records settings
        final JsonWrapper wrapper02 = new JsonWrapper(list);
        assertEquals(false, wrapper02.isSingleton());
        assertEquals(2, wrapper02.getNbRecords());

        // Defined number of records settings
        final JsonWrapper wrapper03 = new JsonWrapper(list, 3);
        assertEquals(false, wrapper03.isSingleton());
        assertEquals(3, wrapper03.getNbRecords());

        // Empty case
        final JsonWrapper wrapper04 = new JsonWrapper();
        assertEquals(false, wrapper04.isSingleton());
        assertEquals(false, wrapper04.isSingleton());
    }

}
