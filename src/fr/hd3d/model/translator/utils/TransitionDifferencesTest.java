package fr.hd3d.model.translator.utils;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests on the id list transition manager.
 * 
 * @author frank.rousseau
 */
public class TransitionDifferencesTest {
    @Test
    public void testNull() {

        testEmptyAnswer(TransitionDifferences.findTransition(null, null));
        testEmptyAnswer(TransitionDifferences.findTransition(
                new HashSet<Long>(), null));
        testEmptyAnswer(TransitionDifferences.findTransition(null,
                new HashSet<Long>()));
    }

    @Test
    public void testEquality() {

        testEmptyAnswer(TransitionDifferences.findTransition(buildSet(),
                buildSet()));
        Assert.assertFalse(0 == TransitionDifferences.findTransition(
                buildSet(), buildSet(5)).toAdd.size());
    }

    @Test
    public void testDifference() {

        testEmptyAnswer(TransitionDifferences.findTransition(buildSet(0),
                buildSet(0)));
        checkDifferences(TransitionDifferences.findTransition(buildSet(5),
                buildSet()), buildSet(), buildSet(5));
        checkDifferences(TransitionDifferences.findTransition(buildSet(5, 1),
                buildSet(1)), buildSet(), buildSet(5));
        checkDifferences(TransitionDifferences.findTransition(buildSet(5, 1),
                buildSet(1, 3)), buildSet(3), buildSet(5));
        checkDifferences(TransitionDifferences.findTransition(buildSet(1),
                buildSet(1, 3)), buildSet(3), buildSet());
    }

    private void checkDifferences(TransitionDifferences findTransition,
            Set<Long> toAdd, Set<Long> toRemove) {

        Assert.assertEquals(toAdd, findTransition.toAdd);
        Assert.assertEquals(toRemove, findTransition.toRemove);
    }

    private void testEmptyAnswer(final TransitionDifferences transition) {

        Assert.assertEquals(0, transition.toAdd.size());
        Assert.assertEquals(0, transition.toRemove.size());
    }

    private Set<Long> buildSet(int... array) {

        final HashSet<Long> set = new HashSet<Long>();
        for (Integer i : array) {
            set.add(new Long(i));
        }
        return set;
    }
}
