package fr.hd3d.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for class StringUtils (tools to validate string).
 * 
 * @author Guillaume Chatelet
 */
public class StringUtilsTest {

    /**
     * Test classic string validation.
     */
    @Test
    public void EmptyStringTest() {

        Assert.assertTrue(StringUtils.isValid("a"));
        Assert.assertFalse(StringUtils.isValid(null));
        Assert.assertFalse(StringUtils.isValid(""));
        Assert.assertFalse(StringUtils.isValid("\t  "));
    }

    /**
     * Test HTML color validation.<br />
     * Valid format is : '#HHHHHH' where H is an hexadecimal number.
     */
    @Test
    public void ColorStringTest() {

        Assert.assertTrue(StringUtils.isValidColor("#123FFF"));
        Assert.assertFalse(StringUtils.isValidColor("#FFaFFFF"));
        Assert.assertFalse(StringUtils.isValidColor("#zFFFFFF"));
        Assert.assertFalse(StringUtils.isValid(null));

    }
}
